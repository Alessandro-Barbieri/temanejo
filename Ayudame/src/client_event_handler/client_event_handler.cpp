#include "client_event_handler/client_event_handler.hpp"
#include "ayudame_lib.hpp"
#include "ayudame_log.h"

Client_event_handler::Client_event_handler(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
    : m_manager(manager),
      m_id(0),
      m_ayu(ayu),
      m_client(client)
{
    //LOG(INFO)<< "Client_event_handler()";

    //getting the process id
    uint64_t pid = getpid();
    set_proc_id(&m_id, pid);
    set_client_id(&m_id, m_client);

    //LOG(DEBUG)<< "m_id:" << m_id << "pid:" <<pid ;
    //LOG(DEBUG)<< "m_id:" << m_id << "m_client:" <<m_client ;
}

Client_event_handler::~Client_event_handler()
{
    //LOG(DEBUG)<< "Client_event_handler::~Client_event_handler()";
}

void Client_event_handler::event(const ayu_event_t event,
                                 ayu_event_data_t _data)
{
    if (event == AYU_FINISH) {
        m_manager.shutdown();
        return;
    }
    _data.common.timestamp = m_ayu.get_timestamp();
    _data.common.client_id = m_id;
    std::shared_ptr <Intern_event> data(Intern_event::create_intern_event(event, _data));
    m_ayu.event(data);
}

void Client_event_handler::msg_send_out(std::shared_ptr <Intern_event> request)
{
    request->print(HUMAN);

}

void Client_event_handler::add_task(const ayu_id_t task_id,
                                    const ayu_id_t scope_id,
                                    const ayu_label_t task_label)
{
    std::shared_ptr <Intern_event> data = Intern_event::create_intern_event_task(m_id, AYU_ADDTASK, m_ayu.get_timestamp(), task_id, scope_id, task_label);
    m_ayu.event(data);
}

void Client_event_handler::add_dependency(const ayu_id_t dependency_id,
                                          const ayu_id_t from_id,
                                          const ayu_id_t to_id,
                                          const ayu_label_t dependency_label)
{
    std::shared_ptr <Intern_event> data = Intern_event::create_intern_event_dependency(m_id, AYU_ADDDEPENDENCY, m_ayu.get_timestamp(), dependency_id, from_id, m_id, to_id, dependency_label);
    m_ayu.event(data);
}

void Client_event_handler::add_dependency_matched(const ayu_id_t dependency_id,
                                                  const ayu_client_id_t from_client_id,
                                                  const ayu_id_t from_id,
                                                  const ayu_client_id_t to_client_id,
                                                  const ayu_id_t to_id,
                                                  const ayu_label_t dependency_label)
{
    std::shared_ptr <Intern_event> data = Intern_event::create_intern_event_dependency(from_client_id, AYU_ADDDEPENDENCY, m_ayu.get_timestamp(), dependency_id, from_id, to_client_id, to_id, dependency_label);
    m_ayu.event(data);
}

void Client_event_handler::set_property_matched(const ayu_client_id_t client_id,
                                                const ayu_id_t property_owner_id,
                                                const ayu_label_t key,
                                                const ayu_label_t value)
{
    std::shared_ptr <Intern_event> data =
                                          Intern_event::create_intern_event_property(client_id, AYU_SETPROPERTY, m_ayu.get_timestamp(), property_owner_id, key, value);
    m_ayu.event(data);
}

void Client_event_handler::set_property(const ayu_id_t property_owner_id,
                                        const ayu_label_t key,
                                        const ayu_label_t value)
{
    std::shared_ptr <Intern_event> data =
                                          Intern_event::create_intern_event_property(m_id, AYU_SETPROPERTY, m_ayu.get_timestamp(), property_owner_id, key, value);
    m_ayu.event(data);
}

void Client_event_handler::add_userdata(const ayu_label_t _data)
{
    std::shared_ptr <Intern_event> data = Intern_event::create_intern_event_userdata(m_id, AYU_USERDATA, m_ayu.get_timestamp(), _data);

    m_ayu.event(data);
}

void Client_event_handler::finish()
{
    m_manager.shutdown();
}



void Client_event_handler::register_get_callback(int (*f)(), tca_callback_t type)
{
    m_get_wd_id_function = f;
    //LOG(ERROR) << "HUHU" << tca_callback_str[type] << " " << m_get_wd_id_function() ;

}
