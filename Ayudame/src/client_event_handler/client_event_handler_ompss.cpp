#include "client_event_handler/client_event_handler_ompss.hpp"

Client_event_handler_ompss::Client_event_handler_ompss(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
    : req_continue(nullptr),
      req_break(nullptr),
      req_step(nullptr),
      req_block_task(nullptr),
      req_unblock_task(nullptr),
      Client_event_handler(manager, ayu, client)
{
    LOG(DEBUG) << "Client_event_handler_ompss::Client_event_handler_ompss()";

}
Client_event_handler_ompss::~Client_event_handler_ompss()
{
    LOG(DEBUG) << "Client_event_handler_ompss::~Client_event_handler_ompss()";
}

void Client_event_handler_ompss::msg_send_out(std::shared_ptr <Intern_event> event)
{

    if (event->get_client_id() != m_id) {
        LOG(ERROR) << "event.get_client_id() != m_id";
        return;
    }
    // This is just doing a print out
    Client_event_handler::msg_send_out(event);

    //TODO call instrumentation
    std::shared_ptr <Intern_request> request = Intern_event::castToRequest(event);
    switch (request->get_request()) {
    case AYU_STEP:
        if (request->get_key() == "step_client") {
            int step_counter = atoi(request->get_value().c_str());
            if (req_step != nullptr) {
                for (int i = 0; i < step_counter; i++) {
                    if (TCA_OK != req_step()) {
                        LOG(ERROR) << "user code: error requesting req_step";
                    }
                }
            } else {
                LOG(ERROR) << "req_step==nullptr";
            }

        }
        break;
    case AYU_BREAK:
        if (request->get_key() == "break") {
          if (req_break != nullptr) {
            if (TCA_OK != req_break()) {
                LOG(ERROR) << "user code: error requesting req_break";
            }
          } else {
              LOG(ERROR) << "req_break==nullptr";
          }
        }
        break;
    case AYU_CONTINUE:
        if (request->get_key() == "continue") {
          if (req_continue != nullptr) {
            if (TCA_OK != req_continue()) {
                LOG(ERROR) << "user code: error requesting req_continue";
            }
          } else {
              LOG(ERROR) << "req_continue==nullptr";
          }
        }
        break;
    case AYU_BLOCK_TASK:
        if (request->get_key() == "block_task") {
            int task_id = atoi(request->get_value().c_str());
            if (req_block_task != nullptr) {
                if (TCA_OK != req_block_task(task_id)) {
                    LOG(ERROR) << "user code: error requesting req_block_task";
                }
            } else {
                LOG(ERROR) << "req_block_task==nullptr";
            }
        }
        break;
    case AYU_UNBLOCK_TASK:
        if (request->get_key() == "unblock_task") {
            int task_id = atoi(request->get_value().c_str());
            if (req_unblock_task != nullptr) {
                if (TCA_OK != req_unblock_task(task_id)) {
                    LOG(ERROR) << "user code: error requesting req_unblock_task";
                }
            } else {
                LOG(ERROR) << "req_unblock_task==nullptr";
            }
        }
        break;
    default:
        LOG(ERROR) << "UNKNOWN REQUEST";
        break;
    }

}

void Client_event_handler_ompss::shutdown()
{
}

void Client_event_handler_ompss::tca_initialize(tca_function_lookup_t lookup,
                                                const char *runtime_version,
                                                unsigned int tca_version)
{

    if (lookup) {

        LOG(DEBUG) << "tool initialize: got lookup =" << (void *) (lookup);
        LOG(DEBUG) << "tool initialize:     rt_version = " << runtime_version;
        LOG(DEBUG) << "tool initialize:     tca version = " << tca_version;

        LOG(DEBUG) << "tool initialize: looking up 'tca_request_step'";
        req_step = (tca_request_step) lookup("tca_request_step");
        LOG(DEBUG) << "tool initialize:        got " << (void *) req_step;

        LOG(DEBUG) << "tool initialize: looking up 'tca_request_continue'";
        req_continue = (tca_request_continue) lookup("tca_request_continue");
        LOG(DEBUG) << "tool initialize:        got " << (void *) req_continue;

        LOG(DEBUG) << "tool initialize: looking up 'tca_request_break'";
        req_break = (tca_request_break) lookup("tca_request_break");
        LOG(DEBUG) << "tool initialize:        got " << (void *) req_break;

        LOG(DEBUG) << "tool initialize: looking up 'tca_request_block_task'";
        req_block_task = (tca_request_block_task) lookup("tca_request_block_task");
        LOG(DEBUG) << "tool initialize:        got " << (void *) req_block_task;

        LOG(DEBUG) << "tool initialize: looking up 'tca_request_unblock_task'";
        req_unblock_task = (tca_request_unblock_task) lookup("tca_request_unblock_task");
        LOG(DEBUG) << "tool initialize:        got " << (void *) req_unblock_task;

        LOG(DEBUG) << "tool initialize: done initialize.";

    } else {
        LOG(ERROR) << "tool initialize: FAILED.";

    }

}

