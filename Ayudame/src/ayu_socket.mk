SWIG ?= swig

if ENABLE_PYTHON_MODULE
ayu_socket_PYTHON = $(srcdir)/ayu_socket.py __init__.py
ayu_socketdir = $(pkgpythondir)
pyexec_LTLIBRARIES = _ayu_socket.la
endif

#EXTRA_DIST += ayu_socket_wrap.cxx
EXTRA_DIST += ayu_socket.i ayu_socket.py

MAINTAINERCLEANFILES += $(srcdir)/ayu_socket_wrap.cxx $(srcdir)/ayu_socket.py

__init__.py: 
	touch __init__.py

_ayu_socket_la_SOURCES = \
	ayu_socket_wrap.cxx \
	socket.cpp \
	ayu_socket.cpp \
	intern_event/intern_event.cpp \
	intern_event/intern_printable.cpp \
	intern_event/intern_event_task.cpp \
	intern_event/intern_event_property.cpp \
	intern_event/intern_event_userdata.cpp \
	intern_event/intern_event_dependency.cpp \
	intern_event/intern_request.cpp
#_ayu_socket_la_LIBADD = libayudame.la
_ayu_socket_la_LDFLAGS  = -module -shared -version-info $(api_version)
_ayu_socket_la_CXXFLAGS = --std=c++11 
_ayu_socket_la_CPPFLAGS = -I$(api_includedir) $(PYTHON_INCLUDE) -I$(libevent_includedir)
_ayu_socket_la_LIBADD = $(srcdir)/../libevent/libevent.la
_ayu_socket_la_LIBADD += $(srcdir)/../libevent/libevent_pthreads.la


$(srcdir)/ayu_socket_wrap.cxx: $(srcdir)/ayu_socket.i
	$(SWIG) -c++ -python -I$(api_includedir) $(srcdir)/ayu_socket.i

$(srcdir)/ayu_socket.py: $(srcdir)/ayu_socket.i
	$(MAKE) $(AM_MAKEFLAGS) ayu_socket_wrap.cxx

#ayu_socket_wrap.o: ayu_socket_wrap.cxx
#	$(CXX) $(_ayu_socket_la_CXXFLAGS)  -fPIC -c ayu_socket_wrap.cxx