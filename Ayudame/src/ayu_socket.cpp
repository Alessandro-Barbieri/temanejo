#include "ayu_socket.hpp"
#include "ayudame_log.h"

Ayu_Socket::Ayu_Socket()
    : m_hostename_to_fd( { }),
      m_proc_to_fd( { }),
      m_host_uniq( { }),
      m_master_id(-1),
      m_my_hostname(""),
      m_temanjo(false)
{
}

Ayu_Socket::~Ayu_Socket()
{
}

int Ayu_Socket::socket_write(std::shared_ptr <Intern_event> data)
{
    const size_t buf_size = data->get_message_size();
    char buf[buf_size];
    data->pack(buf);

    if (m_socket_type == CLIENT) {


        //add_event_to_write_buffer(data, 0);
        ssize_t sent_bytes = send(m_sfd, buf, buf_size, 0);
        if (sent_bytes < 0 ) {
            LOG(ERROR) << "Client failed sending message" << sent_bytes << ":" << buf_size;
            return -1;
        }

    } else if (m_socket_type == SERVER) {


        uint64_t destination = get_master_id(data->get_client_id());
        int send_fd = 0;
        struct bufferevent *bev = nullptr;

        //find correct file descriptor
        if (destination != get_master_id(m_master_id)) {
            destination = calculate_host(destination);
            send_fd = get_fd_for_hostname(destination);
            bev = get_bev_for_hostname(destination);

        } else {
            ayu_client_id_t proc_id = get_proc_id(data->get_client_id());
            if (proc_id == get_proc_id(m_master_id)) {
                LOG(ERROR) << "this is a local event and should not be handled by the socket";
            } else {
                send_fd = get_fd_for_proc(proc_id);
                bev = get_bev_for_proc(proc_id);
            }
        }

        if (send_fd == -1) {
            //send request to ALL file descriptor
            for (auto it = m_hostename_to_fd.begin();
                it != m_hostename_to_fd.end(); ++it) {
                send_fd = it->second.first;
                bev = it->second.second;

                int sent_bytes = evbuffer_w(bev, buf, buf_size);
                if (sent_bytes != 0) {
                    LOG(ERROR) << "Server failed sending message" ;
                    return -1;
                }

            }
        } else {
            //send request to file descriptor
            int sent_bytes = evbuffer_w(bev, buf, buf_size);
            if (sent_bytes != 0 ) {
                LOG(ERROR) << "Server failed sending message" ;
                return -1;
            }

        }

    }

    return 0;
}

uint64_t Ayu_Socket::calculate_host(uint64_t destination)
{

    uint64_t k_index = destination - MAX_REMOTE_CHANNELS_PER_NODE + 1; // ??
    uint64_t k_index_to = (MAX_REMOTE_CHANNELS_PER_NODE + k_index - 2) / MAX_REMOTE_CHANNELS_PER_NODE;

    if (get_master_id(m_master_id) == k_index_to) {
        return destination;
    } else {
        return calculate_host(k_index_to);
    }
}

void Ayu_Socket::set_host_uniq(std::vector <std::string> host_uniq)
{
    m_host_uniq = host_uniq;
}

const std::shared_ptr <Intern_event> Ayu_Socket::socket_read()
{
    if (m_socket_type == CLIENT) {

        //todo should this be done here ?

        int pending = 0;
        ioctl(m_sfd, FIONREAD, &pending);

        if (pending > 0) {
            LOG(INFO) << "message " <<pending<<":"<<m_sfd;

            ssize_t count;
            char buff[ayu_sizeof_value];
            ayu_count_t message_size = 0;
            count = recv(m_sfd, buff, ayu_sizeof_value, MSG_PEEK);
            //count = recv(m_sfd, buff, ayu_sizeof_value, MSG_DONTWAIT);
            if (count == sizeof(ayu_sizeof_value)) {
                std::char_traits <char>::copy((char*) &message_size, &buff[0], ayu_sizeof_value);
                char message_buffer[message_size];
                count = read(m_sfd, message_buffer, sizeof message_buffer);
                std::shared_ptr <Intern_event> data_result(Intern_event::create_intern_event(message_buffer));
                /* what the shit ?
                 if(nullptr != data_result) {
                 ayu_count_t tmp_count  = 0;
                 while(tmp_count != message_size) {
                 count =  recv (m_sfd, message_buffer, (sizeof(message_buffer) - tmp_count), 0);
                 LOG(WARNING)<< "" <<tmp_count<<" " << count;
                 if(count > 0) {
                 tmp_count+= count;
                 }
                 }
                 }
                 */
                if (data_result == nullptr) {
                    LOG(ERROR) << "data_result== nullptr";
                }

                //data_result->print(HUMAN);
                return data_result;

            } else if (count == -1) {
                /* If errno == EAGAIN, that means we have read all
                 data. So go back to the main loop. */
                if (errno != EAGAIN) {
                    LOG(ERROR) << "read";
                    return nullptr;
                }
            } else if (count == 0) {
                /* End of file. The remote has closed the
                 connection. */
                //LOG(INFO)<<  "End of file. The remote has closed the connection";
                return nullptr;
            }
        }
        return nullptr;

    } else if (m_socket_type == SERVER) {
        //LOG(WARNING) << "socket_read:"<< get_read_buffer_size();
        if (get_read_buffer_size() != 0) {
            std::lock_guard <std::mutex> local_mutex(m_mutex_event);
            std::shared_ptr <Intern_event> b = *m_read_buffer.begin();
            m_read_buffer.erase(m_read_buffer.begin());
            return b;
        }
    }
    return nullptr;
}

bool Ayu_Socket::add_event_to_read_buffer(const char* message_buffer, int infd, struct bufferevent* bev)
{
    //LOG(WARNING) << "add_event_to_read_buffer" << message_buffer << infd << get_read_buffer_size();
    ayu_event_t event;
    ayu_count_t msg_index = 3 * ayu_sizeof_value;
    std::char_traits <char>::copy((char*) &event, &message_buffer[msg_index], ayu_sizeof_value);
    if (event == AYU_EVENT_NULL) {
        msg_index -= ayu_sizeof_value;
        ayu_client_id_t client_id;
        std::char_traits <char>::copy((char*) &client_id, &message_buffer[msg_index], ayu_sizeof_value);
        ayu_client_id_t proc_id = get_proc_id(client_id);
        //LOG(ERROR)<<"##########register_fd_for_proc: " << client_id << "  client: " <<  proc_id << "  fd: " << infd ;
        m_proc_to_fd.insert(std::make_pair(proc_id, std::make_pair(infd, bev)));
        return true;
    } else {
        std::shared_ptr <Intern_event> data_result(Intern_event::create_intern_event(message_buffer));
        if (nullptr != data_result) {
            //data_result->print(HUMAN);
            std::lock_guard <std::mutex> local_mutex(m_mutex_event);
            m_read_buffer.push_back(data_result);
            return true;
        } else {
            return false;
        }
    }
}

void Ayu_Socket::register_fd_for_hostname(std::string hostname, int fd, struct bufferevent* bev)
{
    //todo if hostname ==my_hostname
    if (hostname != "127.0.0.1") { //TODO shoud use my_hostname
        LOG(DEBUG) << "register_fd_for_hostname: " << hostname << "  fd: " << fd;
        m_hostename_to_fd.insert(std::make_pair(hostname, std::make_pair(fd, bev)));
    }

    //TODO bypass functionality
    if (m_temanjo) {
        LOG(DEBUG) << "register_fd_for_hostname: " << hostname << "  fd: " << fd;
        m_hostename_to_fd.insert(std::make_pair(hostname, std::make_pair(fd, bev)));
    }

}

int Ayu_Socket::get_fd_for_hostname(uint64_t k_index_to)
{

    if (k_index_to >= m_host_uniq.size()) {
        return -1;
    }
    std::string hostname = m_host_uniq.at(k_index_to);
    auto it = m_hostename_to_fd.find(hostname);
    if (it != m_hostename_to_fd.end()) {
        return it->second.first;
    }
    return -1;
}

int Ayu_Socket::get_fd_for_proc(uint64_t procc)
{

    auto it = m_proc_to_fd.find(procc);
    if (it != m_proc_to_fd.end()) {
        return it->second.first;
    }
    return -1;
}

bufferevent* Ayu_Socket::get_bev_for_hostname(uint64_t k_index_to)
{

    if (k_index_to >= m_host_uniq.size()) {
        return nullptr;
    }
    std::string hostname = m_host_uniq.at(k_index_to);
    auto it = m_hostename_to_fd.find(hostname);
    if (it != m_hostename_to_fd.end()) {
        return it->second.second;
    }
    return nullptr;
}

bufferevent* Ayu_Socket::get_bev_for_proc(uint64_t procc)
{

    auto it = m_proc_to_fd.find(procc);
    if (it != m_proc_to_fd.end()) {
        return it->second.second;
    }
    return nullptr;
}

void Ayu_Socket::delete_fd(int fd)
{
    for (auto it = m_hostename_to_fd.begin(); it != m_hostename_to_fd.end();
        ++it) {
        if (it->second.first == fd) {
            m_hostename_to_fd.erase(it);
            return;
        }
    }
    for (auto it = m_proc_to_fd.begin(); it != m_proc_to_fd.end(); ++it) {
        if (it->second.first == fd) {
            m_proc_to_fd.erase(it);
            return;
        }
    }

    LOG(ERROR) << "delete_fd error nothing was delete THIS SHOULD NOT  HAPPEN";

}

void Ayu_Socket::shutdown_socket()
{
    if (m_socket_type == SERVER) {
        while (m_open_sockets.load() > 0) {
            //LOG(DEBUG)<<"Socket::shutdown_socket() SERVER####" << m_open_sockets.load();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    //LOG(DEBUG)<<" Ayu_Socket::shutdown_socket()";
    while (get_read_buffer_size() != 0) {
        //LOG(DEBUG)<<"Buffer is not Empty";
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        //LOG(DEBUG)<<"Buffer is not Empty end";
    }
    Socket::shutdown_socket();

    if (get_read_buffer_size() != 0) {
        LOG(ERROR) << "BUFFER NOT emtpty";
    }
}

bool Ayu_Socket::is_shutdownable()
{
    if (get_read_buffer_size() == 0 && Socket::is_shutdownable()) {
        return true;
    }
    return false;

}

int Ayu_Socket::get_read_buffer_size()
{
    std::lock_guard <std::mutex> local_mutex(m_mutex_event);
    return m_read_buffer.size();
}

