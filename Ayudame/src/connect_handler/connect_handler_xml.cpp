#include "connect_handler/connect_handler_xml.hpp"
#include "ayudame_log.h"

Connect_handler_xml::Connect_handler_xml(Connect_manager& manager,
                                         Ayudame& ayu,
                                         std::string xml_filename)
    : Connect_handler(manager, ayu),
      m_filename(xml_filename),
      m_xml_file()
{
    try {
        m_xml_file.open(m_filename);
    } catch (std::ofstream::failure e) {
        LOG(ERROR) << e.what();
    }
}

Connect_handler_xml::~Connect_handler_xml()
{

}

void Connect_handler_xml::shutdown(){
    try {
        if (m_xml_file.is_open()) {
            m_xml_file.close();
        }
    } catch (std::ofstream::failure e) {
        LOG(ERROR) << e.what();
    }
}

void Connect_handler_xml::msg_send_out(std::shared_ptr<Intern_event> data)
{
  data->print(XML, m_xml_file);
}




const std::shared_ptr<Intern_event> Connect_handler_xml::msg_read(){
    LOG(ERROR) << "NO implementation availible for xml";
    return nullptr;
}
