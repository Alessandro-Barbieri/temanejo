#include "connect_handler/connect_handler_temanejo_old.hpp"
#include "ayudame_log.h"


Connect_handler_temanejo_old::Connect_handler_temanejo_old
(Connect_manager& manager, Ayudame& ayu, unsigned port)
    : Connect_handler(manager, ayu),
      function_names({}),
      m_server_socket()
{
    LOG(INFO)<< "INIT Socket temanejo old AYU_PORT:" << port;
    m_server_socket.set_server_data(port);
    m_server_socket.init();
}

void Connect_handler_temanejo_old::msg_send_out(std::shared_ptr<Intern_event> data) {

    LOG(INFO)<< "Sending temanejo_old event " << data->get_event();

    switch (data->get_event()) {
    case AYU_ADDTASK:
        add_task((Intern_event_task*) data.get());
        break;
    case AYU_EVENT_NULL:
    case AYU_INIT:
    case AYU_FINISH:
    case AYU_START:
    case AYU_END:
    case AYU_ADDDEPENDENCY:
        add_dependency((Intern_event_dependency*) data.get());
        break;
    case AYU_SETPROPERTY:
        //set_property(data);
        //break;
    case AYU_BARRIER:
    case AYU_ATTACH:
    case AYU_DETACH:
    default:
        break;
    }

}




const std::shared_ptr<Intern_event> Connect_handler_temanejo_old::msg_read(){
    LOG(ERROR) << "NO implementation availible for tem old";
    return nullptr;
}

/**
 * add task
 */
void Connect_handler_temanejo_old::add_task(Intern_event_task *data) {

    std::string s = data->get_task_label();
    std::string::size_type pos = s.find(' ');

    if (pos != std::string::npos) {
        s= s.substr(0, pos);
    }


    auto it = function_names.find(s);

    if (it ==  function_names.end()) {

        int function_id = function_names.size() + 1;


        pack_and_send( (ayu_id_t) data->get_client_id(),                //buf0
                       (ayu_id_t) function_id,              //buf1
                       (ayu_id_t) O_AYU_REGISTERFUNCTION,                             //buf2
                      0,                          //buf3 will be calculated(String length)
                      function_id,              //buf4  priority or function id
                      0,                                                  //buf5
                      0,                                                  //buf6
                      (ayu_id_t) data->get_timestamp(),                              //buf7
                      (ayu_label_t) s.c_str());           //string
        //printf("##client_id=%lu function_id=%lu event_id=%d function_name=%s \n",  data->get_client_id(),function_id,O_AYU_REGISTERFUNCTION,s.c_str());

        function_names.insert(std::make_pair(s,function_id));

        it = function_names.find(s);


        if (it !=  function_names.end()) {
            LOG(ERROR) << "Failed putting function name into list";

        }

    }


    std::string a ("");
    pack_and_send((ayu_id_t) data->get_client_id(),                //buf0
                  (ayu_id_t) data->get_task_id(),                              //buf1
                  (ayu_id_t) new_events_to_old.find(data->get_event())->second,  //buf2
                  (ayu_id_t) it->second,                                         //buf3 function id
                  0,                                                  //buf4  priority
                  0,                                                  //buf5
                  0,                                                  //buf6
                  (ayu_id_t) data->get_timestamp(),                              //buf7
                  /*(ayu_label_t) data->get_task_label().c_str()*/a.c_str());                                  //string

    //printf("##client_id=%lu task_id=%lu event_id=%d function_id=%lu \n",  data->get_client_id(),data->get_task_id(),new_events_to_old.find(data->get_event())->second,it->second);
}

/**
 * add dependency
 */
void Connect_handler_temanejo_old::add_dependency(Intern_event_dependency *data) {
    std::string a("");

    pack_and_send((ayu_id_t) data->get_client_id(),                        //buf0
                  (ayu_id_t) data->get_to_id(),                          //buf1
                  (ayu_id_t) new_events_to_old.find(data->get_event())->second,  //buf2
                  (ayu_id_t) data->get_from_id(),                        //buf3
                  0,                                                  //buf4  priority
                  0,                                                  //buf5
                  0,                                                  //buf6
                  (ayu_id_t) data->get_timestamp(),                              //buf7
                  /*(ayu_label_t) data->get_dependency_label().c_str()*/a.c_str());                                              //string
    //printf("##client_id=%lu to_id=%lu event_id=%d from_id_id=%lu  label=%s \n",  data->get_client_id(),data->get_to_id(),new_events_to_old.find(data->get_event())->second,data->get_from_id(),data->get_dependency_label().c_str());

}

/**
 * pack and send the buffer
 */
void Connect_handler_temanejo_old::pack_and_send(ayu_id_t buf0,
        ayu_id_t buf1,
        ayu_id_t buf2,
        ayu_id_t buf3,
        ayu_id_t buf4,
        ayu_id_t buf5,
        ayu_id_t buf6,
        ayu_id_t ts,
        const ayu_label_t string) {
    /*

    //printf("##buf0=%lu buf1=%lu buf2=%lu buf3=%lu buf4=%lu buf5=%lu buf6=%lu  string=%s \n", buf0,  buf1, buf2, buf3, buf4, buf5, buf6, string);


    char string_buf[AYU_string_buf_size];   // string buffer
    int64_t msg_buffer[AYU_buf_size];

    msg_buffer[0] = htobe64((uint64_t) buf0); //runtime ID
    msg_buffer[1] = htobe64((uint64_t) buf1);
    msg_buffer[2] = htobe64((uint64_t) buf2); //eventID
    msg_buffer[3] = htobe64((uint64_t) buf3);
    msg_buffer[4] = htobe64((uint64_t) buf4);
    msg_buffer[5] = htobe64((uint64_t) buf5);
    msg_buffer[6] = htobe64((uint64_t) buf6);
    msg_buffer[7] = htobe64((uint64_t) ts);

    size_t string_len;
    string_len = strlen((char const *) string);

    if (string_len != 0) {

        msg_buffer[3] = htobe64(string_len);

        memset(string_buf, '\0', AYU_string_buf_size);
        strcpy(string_buf, (char const *) string);

        char full_buf[sizeof(msg_buffer) + sizeof(string_buf)];
        memcpy(full_buf, msg_buffer, sizeof(msg_buffer));
        memcpy(full_buf + sizeof(msg_buffer), string_buf, string_len);

        int i = m_server_socket.Socket::socket_write( full_buf,
                                       sizeof(msg_buffer) + string_len);

        if (i < 0) {
            LOG(ERROR)<< "Failed sending message";
        }
    } else {
        int i = m_server_socket.Socket::socket_write((const char*) msg_buffer, sizeof(msg_buffer));
        if (i < 0) {
            LOG(ERROR) << "Failed sending message";
        }
    }
    */
}

