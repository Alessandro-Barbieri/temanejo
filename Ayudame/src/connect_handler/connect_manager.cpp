#include "connect_handler/connect_manager.hpp"
#include "ayudame_lib.hpp"
#include "ayudame_log.h"


Connect_manager::Connect_manager(Ayudame& ayu)
        : m_ayu(ayu),
m_connect_handlers({}),
m_connect_handlers_status({}) {
    LOG(DEBUG)<< "Connect_manager::Connect_manager()";
}

Connect_manager::~Connect_manager() {
    LOG(DEBUG)<< "Connect_manager::~Connect_manager()";
}

void Connect_manager::shutdown() {
    LOG(DEBUG)<< "Connect_manager::shutdown()";

    for (auto& connect_handler : m_connect_handlers) {
        connect_handler.second->shutdown();
    }
    //m_connect_handlers.clear();


}



void Connect_manager::msg_send_out(std::shared_ptr<Intern_event> data) {
    for (auto& connect_handler : m_connect_handlers) {
        std::map<ayu_connect_t, bool>::iterator it = m_connect_handlers_status.find(connect_handler.first);
        if (it != m_connect_handlers_status.end() && it->second == true) {
            //LOG(DEBUG) << "#Connect_manager::msg_send_out start";
            //TODO pass pointer instead of reference
            connect_handler.second->msg_send_out(data); // 'second' because m_connect_handlers is a map.
            //LOG(DEBUG) << "#Connect_manager::msg_send_out end";
        }
    }
}

void Connect_manager::init_connect(const ayu_connect_t connect) {
    if (m_connect_handlers.find(connect) == m_connect_handlers.end()) {
        std::shared_ptr <Connect_handler> connect_handler = create_connect_handler(connect);
        m_connect_handlers.insert(std::make_pair(connect, connect_handler));

        //activated
        m_connect_handlers_status.insert(std::make_pair(connect, true));
    }
}


const std::shared_ptr<Intern_event>  Connect_manager::msg_read() {

    auto connect_handler = m_connect_handlers.find(AYU_CONNECT_SOCKET);
    if (connect_handler != m_connect_handlers.end()) {
        return connect_handler->second->msg_read();
    }

    connect_handler = m_connect_handlers.find(AYU_CONNECT_TEMANEJO);
    if (connect_handler != m_connect_handlers.end()) {
        return connect_handler->second->msg_read();
    }

    return nullptr;

}


std::shared_ptr <Connect_handler> Connect_manager::create_connect_handler(const ayu_connect_t connect) {
    std::string dot_filename;
    std::string xml_filename;
    std::string host_string;
    std::string port_string;
    unsigned port;

    switch (connect) {
    case AYU_CONNECT_GENERIC:
        //return std::shared_ptr<Connect_handler>(new Connect_handler_generic(*this));
    case AYU_CONNECT_STDOUT_HUMAN:
        return std::shared_ptr <Connect_handler_stdout_human>
               (new Connect_handler_stdout_human(*this, m_ayu));
    case AYU_CONNECT_STDOUT_RAW:
        return std::shared_ptr <Connect_handler_stdout_raw>
               (new Connect_handler_stdout_raw(*this, m_ayu));
        // case AYU_CONNECT_TEST:
        //     return std::shared_ptr <Connect_handler_test>
        //           (new Connect_handler_test(*this, m_ayu));
    case AYU_CONNECT_DOT:
        dot_filename =
            m_ayu.get_config().get_string("connect.dot_filename");
        return std::shared_ptr <Connect_handler_dot>
               (new Connect_handler_dot(*this, m_ayu, dot_filename));
    case AYU_CONNECT_XML:
        xml_filename =
            m_ayu.get_config().get_string("connect.xml_filename");
        return std::shared_ptr <Connect_handler_xml>
               (new Connect_handler_xml(*this, m_ayu, xml_filename));
    case AYU_CONNECT_TEMANEJO1:
        port_string = m_ayu.get_config().get_string("connect.ayu_port");
        port = std::stoul(port_string);
        return std::shared_ptr <Connect_handler_temanejo_old>
               (new Connect_handler_temanejo_old(*this, m_ayu, port));
    case AYU_CONNECT_TEMANEJO:
        host_string = m_ayu.get_config().get_string("connect.ayu_host");
        port_string = m_ayu.get_config().get_string("connect.ayu_port");
        port = std::stoul(port_string);
        return std::shared_ptr <Connect_handler_temanejo>
               (new Connect_handler_temanejo(*this, m_ayu, port, host_string));
    case AYU_CONNECT_SOCKET:
        return std::shared_ptr <Connect_handler_socket>
               (new Connect_handler_socket(*this, m_ayu));
    default:
        LOG(ERROR)<< "default case reached in create_connect_handler";
        break;
    }

    return std::shared_ptr <Connect_handler>
           (new Connect_handler_stdout_raw(*this, m_ayu));
}


Connect_handler* Connect_manager::get_connect(const ayu_connect_t connect) {

    std::map <ayu_connect_t, std::shared_ptr <Connect_handler>> ::iterator it = m_connect_handlers.find(connect);
    if (it != m_connect_handlers.end()) {
        return it->second.get();
    } else {
        return nullptr;
    }



}



void Connect_manager::activate_connect(const ayu_connect_t connect) {
    std::map<ayu_connect_t, bool>::iterator it = m_connect_handlers_status.find(connect);
    if (it != m_connect_handlers_status.end()) {
        it->second = true;
    }

}

void Connect_manager::deactivate_connect(const ayu_connect_t connect) {
    std::map<ayu_connect_t, bool>::iterator it = m_connect_handlers_status.find(connect);
    if (it != m_connect_handlers_status.end()) {
        it->second = false;
    }

}

void Connect_manager::deactivate_all_connect() {
    for (auto& connect_handler_status : m_connect_handlers_status) {
        connect_handler_status.second =false;
    }
}
