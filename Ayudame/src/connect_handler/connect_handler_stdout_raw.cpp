#include "connect_handler/connect_handler_stdout_raw.hpp"


void Connect_handler_stdout_raw::msg_send_out(std::shared_ptr<Intern_event> data)
{
    data->print(RAW);
}




const std::shared_ptr<Intern_event> Connect_handler_stdout_raw::msg_read(){
    LOG(ERROR) << "NO implementation availible for raw";
    return nullptr;
}
