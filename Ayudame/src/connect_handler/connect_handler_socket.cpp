#include "connect_handler/connect_handler_socket.hpp"
#include "ayudame_log.h"
#include "ayudame_lib.hpp"


void Connect_handler_socket::msg_send_out(std::shared_ptr<Intern_event> data)
{

        //LOG(DEBUG) << "Connect_handler_socket::msg_send_out";

        int i = m_client_socket.socket_write(data);

        //LOG(DEBUG) << "#Connect_handler_socket::msg_send_out event  " << i  << "  "  << ayu_event_str[data->get_event()]<< " DONE";


}

const std::shared_ptr<Intern_event> Connect_handler_socket::msg_read()
{
        return m_client_socket.socket_read();
}



int Connect_handler_socket::init_client(int port, std::string host)
{

        m_client_socket.set_client_data(port,host);
        return m_client_socket.init();

}



Connect_handler_socket::~Connect_handler_socket()
{
        LOG(DEBUG)<< "Connect_handler_socket::~Connect_handler_socket()";
}

void Connect_handler_socket::shutdown(){
    LOG(DEBUG)<< "Connect_handler_socket::shutdown()";

    while(m_ayu.get_buffer_size() != 0) {
        LOG(DEBUG)<<"Buffer is not Empty";
        std::this_thread::sleep_for (std::chrono::milliseconds(10));
    }

    m_client_socket.shutdown_socket();
}

