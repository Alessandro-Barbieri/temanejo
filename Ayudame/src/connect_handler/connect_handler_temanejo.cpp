#include "connect_handler/connect_handler_temanejo.hpp"
#include "ayudame_log.h"

Connect_handler_temanejo::Connect_handler_temanejo
    (Connect_manager& manager, Ayudame& ayu, unsigned port, std::string host)
        : Connect_handler(manager, ayu), m_client_socket()
{
    LOG(INFO)<< "INIT Socket temanejo AYU_PORT:" << port;

    // These data types are required to be 8 byte wide
    //CHECK_EQ(sizeof(ayu_id_t), ayu_sizeof_value);
    //CHECK_EQ(sizeof(ayu_count_t), ayu_sizeof_value);
    //CHECK_EQ(sizeof(ayu_int_t), ayu_sizeof_value);
    //CHECK_EQ(sizeof(ayu_float_t), ayu_sizeof_value);

    m_client_socket.set_client_data(port, host);
    m_client_socket.init();
}



void Connect_handler_temanejo::shutdown(){

    LOG(DEBUG)<< "Connect_handler_temanejo::shutdown()";

    m_client_socket.shutdown_socket();

}


void Connect_handler_temanejo::msg_send_out(std::shared_ptr<Intern_event> data)
{
    //LOG(INFO)<< "Sending temanejo event " << ayu_event_str[data->get_event()];

    int socket_write_successful = m_client_socket.socket_write(data);
    if (socket_write_successful != 0)
    {
        LOG(ERROR) << "socket_write was not successful!";
    }
}


const std::shared_ptr<Intern_event> Connect_handler_temanejo::msg_read(){
    return m_client_socket.socket_read();
}
