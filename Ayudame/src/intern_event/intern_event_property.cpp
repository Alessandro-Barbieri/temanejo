#include "intern_event/intern_event_property.hpp"
#include "ayudame_log.h"

Intern_event_property::Intern_event_property(ayu_client_id_t _client_id,
                      ayu_event_t _event,
                      ayu_count_t _timestamp,
                      ayu_id_t _property_owner_id,
                      std::string _key,
                      std::string _value)
  : Intern_event(_client_id, _event, _timestamp),
    m_property_owner_id(_property_owner_id),
    m_key(_key),
    m_value(_value)
{
}

Intern_event_property::Intern_event_property(ayu_event_data_t data)
  :  Intern_event_property(data.common.client_id,
                           data.common.event,
                           data.common.timestamp,
                           data.set_property.property_owner_id,
                           std::string(data.set_property.key),
                           std::string(data.set_property.value))
{
}

Intern_event_property::Intern_event_property(const char *message)
  :  m_property_owner_id(0),
     m_key(""),
     m_value("")
{

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    init(message, &msg_index, &msg_size, &label_size);

    // add_property:   property_owner_id
    std::char_traits<char>::copy((char*) &m_property_owner_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    if (label_size != 0) {

        std::size_t msg_size_part_one;
        std::char_traits<char>::copy((char*) &msg_size_part_one, &message[msg_index], ayu_sizeof_value);
        msg_index += ayu_sizeof_value;

        // add_property:   key
        char tmp[msg_size_part_one];
        std::char_traits<char>::copy(tmp, &message[msg_index], msg_size_part_one);
        tmp[msg_size_part_one] = '\0'; // append null-character
        msg_index += msg_size_part_one;
        m_key=std::string(tmp);

        std::size_t msg_size_part_two;
        msg_size_part_two = label_size-msg_size_part_one-ayu_sizeof_value;

        // add_property:   value
        char tmp2[msg_size_part_two];
        std::char_traits<char>::copy(tmp2, &message[msg_index], msg_size_part_two);
        tmp2[msg_size_part_two] = '\0'; // append null-character
        msg_index += msg_size_part_two;
        m_value=std::string(tmp2);
    }

    //LOG(INFO) << "UNPACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    //<< " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    //<< " property_owner_id:" << m_property_owner_id << " key:" << m_key
    //<< " value:" << m_value;
}




void Intern_event_property::pack(char* message) {

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    msg_size =  get_message_size();
    label_size = ayu_sizeof_value + m_key.size() + m_value.size();

    Intern_event::pack(message, &msg_index, &msg_size, &label_size);

    std::char_traits<char>::copy(&message[msg_index],(char*) &m_property_owner_id, ayu_sizeof_value);
    msg_index += ayu_sizeof_value;


    if (label_size != 0) {

        std::size_t key_size = m_key.size();
        std::char_traits<char>::copy(&message[msg_index],(char*) &key_size, ayu_sizeof_value);
        msg_index += ayu_sizeof_value;

        std::char_traits<char>::copy(&message[msg_index], m_key.c_str(), m_key.size());
        msg_index +=  m_key.size();

        std::char_traits<char>::copy(&message[msg_index], m_value.c_str(), m_value.size());
        msg_index += m_value.size();
    }

    //LOG(INFO) << "PACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    //<< " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    //<< " property_owner_id:" << m_property_owner_id << " key:" << m_key
    //<< " value:" << m_value;

}

void Intern_event_property::print(ayu_printformat_t printformat,
                                        std::ostream &os) {

    Intern_event::print(printformat, os);
    if (RAW == printformat) {
        os << m_property_owner_id << " " << m_key << " " << m_value << "\n";
    } else if(HUMAN == printformat) {
        os << "PropertyOwnerID=" << m_property_owner_id << " Key=" << m_key << " Value="  << m_value << "\n";
    } else if(DOT == printformat) {
        // TODO
    } else if(XML == printformat) {
            os << xml_seperator << m_property_owner_id <<  xml_seperator << m_key << xml_seperator << m_value << "\n";
        }
}




ayu_count_t Intern_event_property::get_message_size() const {
    ayu_count_t message_size = 0;
    message_size += Intern_event::get_message_size();
    message_size += ayu_sizeof_value;
    message_size += ayu_sizeof_value;
    message_size += m_key.size(); // m_key
    message_size += m_value.size() ; // m_value


    return message_size;
}
