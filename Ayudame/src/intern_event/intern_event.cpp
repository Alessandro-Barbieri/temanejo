#include "intern_event/intern_event.hpp"
#include "intern_event/intern_event_task.hpp"
#include "intern_event/intern_event_dependency.hpp"
#include "intern_event/intern_event_property.hpp"
#include "intern_event/intern_event_userdata.hpp"
#include "intern_event/intern_request.hpp"
#include "ayudame_log.h"

void Intern_event::init(const char *message, ayu_count_t *msg_index , ayu_count_t *msg_size ,ayu_count_t *label_size) {
    *msg_index=0;
    *msg_size=0;
    *label_size=0;

    // header: message size in bytes
    std::char_traits<char>::copy((char*) msg_size, &message[*msg_index], ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

    // header: label size in bytes
    std::char_traits<char>::copy((char*) label_size, &message[*msg_index], ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

    // common: client_id
    std::char_traits<char>::copy((char*) &m_client_id, &message[*msg_index], ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

    // common: event
    std::char_traits<char>::copy((char*) &m_event,  &message[*msg_index], ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

    // common: timestamp
    std::char_traits<char>::copy((char*) &m_timestamp, &message[*msg_index], ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

}


void Intern_event::pack(char* message) {
    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    msg_size =  get_message_size();
    label_size = 0; // intern event has no lable

    pack(message, &msg_index, &msg_size, &label_size);
}

void Intern_event::pack(char* message, ayu_count_t *msg_index , ayu_count_t *msg_size ,ayu_count_t *label_size) {

    *msg_index = 0;


    //VLOG(3) << "PACK_2  common";

    // copy message_size to message
    std::char_traits<char>::copy(&message[*msg_index],
                                 (char*) msg_size,
                                 ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

    // copy label_size to message
    std::char_traits<char>::copy(&message[*msg_index],
                                 (char*) label_size,
                                 ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

    // common data
    std::char_traits<char>::copy(&message[*msg_index],
                                 (char*) &m_client_id,
                                 ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[*msg_index],
                                 (char*) &m_event,
                                 ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[*msg_index],
                                 (char*) &m_timestamp,
                                 ayu_sizeof_value);
    *msg_index += ayu_sizeof_value;

}

void Intern_event::print(ayu_printformat_t printformat,
                         std::ostream &os) {
    if (RAW == printformat) {
        os << m_client_id << " " << m_event << " " << m_timestamp << "\t";
    } else if (HUMAN == printformat) {
        os << "ClientID="<< m_client_id << " Event=" << m_event << " Timestamp=" << m_timestamp << "\t";
    } else if(DOT == printformat) {

    }else if(XML == printformat){
        os <<  m_event << xml_seperator << m_client_id << xml_seperator <<  m_timestamp;
    }
}


ayu_count_t Intern_event::get_message_size() const {
    ayu_count_t message_size = 0;
    message_size += 2 * ayu_sizeof_value; // m_msg_size, m_label_size
    message_size += 3 * ayu_sizeof_value; // m_client_id, m_event, m_timestamp


    return message_size;
}






std::shared_ptr <Intern_event> Intern_event::create_intern_event(const ayu_event_t event, ayu_event_data_t data) {
    data.common.event = event;


    if(AYU_ADDTASK == event) {
        return std::shared_ptr <Intern_event>(new Intern_event_task(data));
    } else if (AYU_ADDDEPENDENCY == event) {
        return std::shared_ptr <Intern_event>(new Intern_event_dependency(data));
    } else if(AYU_SETPROPERTY == event) {
        return std::shared_ptr <Intern_event>(new Intern_event_property(data));
    } else if (AYU_USERDATA  == event) {
        return  std::shared_ptr <Intern_event>(new Intern_event_userdata(data));
    }
    LOG(ERROR)<< "NO Intern_event was created "<< event;
    return nullptr;
}

std::shared_ptr <Intern_event> Intern_event::create_intern_event(const char *message_buffer) {

    ayu_event_t event;
    ayu_count_t msg_index = 3 * ayu_sizeof_value;
    std::char_traits<char>::copy((char*) &event,  &message_buffer[msg_index], ayu_sizeof_value);

    if (event == AYU_ADDTASK) {
        return std::shared_ptr <Intern_event>(new Intern_event_task(message_buffer));
    } else if(event == AYU_ADDDEPENDENCY) {
        return std::shared_ptr <Intern_event>(new Intern_event_dependency(message_buffer));
    } else if(event == AYU_SETPROPERTY) {
        return std::shared_ptr <Intern_event>(new Intern_event_property(message_buffer));
    } else if(event == AYU_USERDATA) {
        return std::shared_ptr <Intern_event>(new Intern_event_userdata(message_buffer));
    } else if(event == AYU_REQUEST) {
        return std::shared_ptr <Intern_event>(new Intern_request(message_buffer));
    }
    LOG(ERROR)<< "NO Intern_event was created:"<< event;

    return nullptr;
}


std::shared_ptr <Intern_event> Intern_event::create_intern_event_task(ayu_client_id_t client_id,
        ayu_event_t event,
        ayu_count_t timestamp,
        ayu_id_t task_id,
        ayu_id_t scope_id,
        ayu_label_t task_label) {
    if ( event == AYU_ADDTASK ) {
        return std::shared_ptr <Intern_event> (new Intern_event_task(client_id, AYU_ADDTASK, timestamp, task_id, client_id, scope_id, task_label));
    } else {
        LOG(ERROR) << "internal event creation failed!";
        return nullptr;
    }
}

std::shared_ptr <Intern_event> Intern_event::create_intern_event_dependency(ayu_client_id_t client_id,
        ayu_event_t event,
        ayu_count_t timestamp,
        ayu_id_t dependency_id,
        ayu_id_t from_id,
        ayu_client_id_t to_client_id,
        ayu_id_t to_id,
        ayu_label_t dependency_label) {
    if ( event == AYU_ADDDEPENDENCY ) {
        return std::shared_ptr <Intern_event> (new Intern_event_dependency(client_id, AYU_ADDDEPENDENCY, timestamp, dependency_id, from_id, to_client_id, to_id, dependency_label));
    } else {
        LOG(ERROR) << "internal event creation failed!";
        return nullptr;
    }
}

std::shared_ptr <Intern_event> Intern_event::create_intern_event_property(ayu_client_id_t client_id,
        ayu_event_t event,
        ayu_count_t timestamp,
        ayu_id_t property_owner_id,
        ayu_label_t key,
        ayu_label_t value) {
    if ( event == AYU_SETPROPERTY ) {
        return std::shared_ptr <Intern_event>(new Intern_event_property(client_id, AYU_SETPROPERTY, timestamp, property_owner_id, key, value));
    } else {
        LOG(ERROR) << "internal event creation failed!";
        return nullptr;
    }
}

std::shared_ptr <Intern_event> Intern_event::create_intern_request(ayu_client_id_t client_id,
        ayu_event_t event,
        ayu_count_t timestamp,
        ayu_request_t request,
        ayu_label_t key,
        ayu_label_t value) {
    if ( event == AYU_REQUEST ) {
        return std::shared_ptr <Intern_event>(new Intern_request(client_id, timestamp, request, key, value));
    } else {
        LOG(ERROR) << "internal event creation failed!";
        return nullptr;
    }
}



std::shared_ptr <Intern_event> Intern_event::create_intern_event_userdata(ayu_client_id_t client_id,
        ayu_event_t event,
        ayu_count_t timestamp,
        ayu_label_t data) {
    if ( event == AYU_USERDATA ) {
        return  std::shared_ptr <Intern_event>(new Intern_event_userdata(client_id, AYU_USERDATA, timestamp, data));
    } else {
        LOG(ERROR) << "internal event creation failed!";
        return nullptr;
    }
}

std::shared_ptr <Intern_event_task> Intern_event::castToEventTask(std::shared_ptr <Intern_event> event) {
    return std::static_pointer_cast<Intern_event_task>(event);
}


std::shared_ptr <Intern_event_dependency> Intern_event::castToEventDependency(std::shared_ptr <Intern_event> event) {
    return std::static_pointer_cast<Intern_event_dependency>(event);
}


std::shared_ptr <Intern_event_property> Intern_event::castToEventProperty(std::shared_ptr <Intern_event> event) {
    return std::static_pointer_cast<Intern_event_property>(event);
}


std::shared_ptr <Intern_event_userdata> Intern_event::castToEventUserdata(std::shared_ptr <Intern_event> event) {
    return std::static_pointer_cast<Intern_event_userdata>(event);
}

std::shared_ptr <Intern_request> Intern_event::castToRequest(std::shared_ptr <Intern_event> event) {
    return std::static_pointer_cast<Intern_request>(event);
}

/*
    std::shared_ptr <Intern_event> Intern_event::castRequestToEvent(std::shared_ptr <Intern_request> request) {
        return std::static_pointer_cast<Intern_event>(request);
}
*/
