
%module ayu_socket
%{


#include <stdint.h>



#include "ayudame_types.h"
#include "socket.hpp"
#include "ayu_socket.hpp"
#include "intern_event/intern_printable.hpp"
#include "intern_event/intern_event.hpp"
#include "intern_event/intern_event_dependency.hpp"
#include "intern_event/intern_event_task.hpp"
#include "intern_event/intern_event_property.hpp"
#include "intern_event/intern_event_userdata.hpp"
#include "intern_event/intern_request.hpp"
%}



%include "stdint.i"

%include <std_shared_ptr.i>
%include "std_string.i"


%shared_ptr(Intern_printable);
%shared_ptr(Intern_event);
%shared_ptr(Intern_event_task);
%shared_ptr(Intern_event_dependency);
%shared_ptr(Intern_event_property);
%shared_ptr(Intern_event_userdata);
%shared_ptr(Intern_request);




%include "ayudame_types.h"
%include "socket.hpp"
%include "ayu_socket.hpp"
%include "intern_event/intern_printable.hpp"
%include "intern_event/intern_event.hpp"
%include "intern_event/intern_event_task.hpp"
%include "intern_event/intern_event_dependency.hpp"
%include "intern_event/intern_event_property.hpp"
%include "intern_event/intern_event_userdata.hpp"
%include "intern_event/intern_request.hpp"


