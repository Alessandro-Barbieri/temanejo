#include "socket.hpp"
#include "ayudame_log.h"

#include <arpa/inet.h>

#define EVLOOP_ONCE             0x01
#define EVLOOP_NONBLOCK         0x02
#define EVLOOP_NO_EXIT_ON_EMPTY 0x04

Socket::Socket()
    : m_sfd(0),
      m_port(0),
      m_host(""),
      m_open_sockets(0),
      m_mutex_event(),
      m_socket_type(NONE)
{
}

Socket::Socket(const Socket& _s)
    : m_sfd(_s.m_sfd),
      m_port(_s.m_port),
      m_host(_s.m_host),
      m_open_sockets(_s.m_open_sockets.load()),
      m_mutex_event(),
      m_socket_type(_s.m_socket_type)
{
}

Socket& Socket::operator=(const Socket& _s)
{
    m_sfd = _s.m_sfd;
    m_port = _s.m_port;
    m_host = _s.m_host;
    m_open_sockets.store(_s.m_open_sockets.load());
    //m_mutex_event = _s.m_mutex_event;
    m_socket_type = _s.m_socket_type;
    return *this;
}

Socket::~Socket()
{
    LOG(DEBUG) << "Socket::~Socket()";
}

void Socket::set_server_data(int port)
{
    if (m_socket_type == NONE) {
        m_port = port;
        m_socket_type = SERVER;
    } else {
        LOG(ERROR) << "Can't set_server_data, because data was set before";
    }

}

void Socket::set_client_data(int port, std::string host)
{
    if (m_socket_type == NONE) {
        m_port = port;
        m_host = host;
        m_socket_type = CLIENT;
    } else {
        LOG(ERROR) << "Can't set_client_data, because data was set before";
    }

}

int Socket::init()
{
    if (m_socket_type == SERVER) {
        return init_server_socket();
    } else if (m_socket_type == CLIENT) {
        return init_client_socket();
    } else {
        LOG(ERROR) << "Can't init Socket, because data was NOT set before";
        return -1;
    }
    return -1;
}

void Socket::shutdown_socket()
{

    if (m_socket_type == SERVER) {

        LOG(DEBUG) << "Socket::shutdown_socket() SERVER";

        if (m_thread == nullptr) {
            return;
        }

        while (m_open_sockets.load() > 0) {
            //LOG(DEBUG)<<"Socket::shutdown_socket() SERVER####" << m_open_sockets.load();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        //
        struct timeval ten_sec;

        ten_sec.tv_sec = 0;
        ten_sec.tv_usec = 100;

        event_base_loopexit(base, &ten_sec);
        //event_base_dispatch(base);
        //event_base_loopbreak(base);
        //event_base_free(base);

        //m_active_thread.store(false);
        while (m_active_thread.load() == true) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        m_thread->join();
        m_thread = nullptr;

        LOG(DEBUG) << "Socket::shutdown_socket() SERVER DONE";

    } else if (m_socket_type == CLIENT) {
        if (m_sfd == 0) {
            return;
        }

        //ioctl returns number of bytes in file descriptor
        //int pending = 1;
        //while (pending != 0) {
        //    ioctl(m_sfd, FIONWRITE, &pending);
        //   LOG(DEBUG)<<"pending"<<pending;
        //   std::this_thread::sleep_for(std::chrono::milliseconds(100));
        //}
        //shutdown(m_sfd, SHUT_RDWR); //maybe not needed

        close(m_sfd);
        LOG(DEBUG) << "Socket::shutdown_socket() CLIENT DONE";
    }

}

bool Socket::is_shutdownable()
{
    if (m_open_sockets.load() > 0) {
        return false;
    }
    return true;
}

void Socket::force_shutdown()
{
    if (m_socket_type == SERVER) {

        event_base_loopexit(base, NULL);
    }

    /*
     LOG(WARNING) << "force_shutdown";
     for (int i = 0; i < MAX_CHANNELS_PER_NODE; i++) {
     if (events[i].data.fd != 0) {
     shutdown(events[i].data.fd, SHUT_RDWR);
     close(events[i].data.fd);
     }
     }
     */
}

/**
 * server is listening on a socket
 */
int Socket::init_client_socket()
{
//LOG(ERROR) << chost<< " " << m_host << " " << gethostbyname(chost);
    while (1) {
        struct sockaddr_in m_server_addr; // address structs for server socket
        struct hostent *server = gethostbyname(m_host.c_str());

        // initialise variables --------------------
        memset(&m_server_addr, 0, sizeof(struct sockaddr_in));
        // -----------------------------------------
        // establish socket ------------------------
        // TODO: Do we need IPv6 support?
        //m_sfd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
        m_sfd = socket(AF_INET, SOCK_STREAM, 0);
        if (m_sfd < 0) {
            LOG(ERROR) << "Socket creation failed";
            continue;
        }
        // -----------------------------------------
        // connect socket ------------------------
        m_server_addr.sin_port = htons(m_port);
        m_server_addr.sin_family = AF_INET;

        bcopy((char *) server->h_addr, (char *)&m_server_addr.sin_addr.s_addr, server->h_length);

        if (connect(m_sfd, (struct sockaddr *) &m_server_addr, sizeof(m_server_addr)) < 0) {
            LOG(WARNING) << "Connect socket to " << m_host << ":" << m_port << " failed.";
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            continue;
        }
        LOG(INFO) << "Connect socket to " << m_host << ":" << m_port;
        break;
    }
    return 0;

}

//______________________________________________________________________________________________________________
//______________________________________________________________________________________________________________
//___________________________________________SERVER_____________________________________________________________
//______________________________________________________________________________________________________________
//______________________________________________________________________________________________________________
/**
 * This function will be called by libevent when the client socket is
 * ready for reading.
 */
void Socket::read_cb(struct bufferevent *bev, void *ctx)
{
    //LOG(ERROR) << "evbuffer_get_length(bufferevent_get_input(bev) " << evbuffer_get_length(bufferevent_get_input(bev));
    while (evbuffer_get_length(bufferevent_get_input(bev)) > sizeof(ayu_sizeof_value)) {
        //LOG(ERROR) << "++++evbuffer_get_length(bufferevent_get_input(bev) " << evbuffer_get_length(bufferevent_get_input(bev));
        Socket *socket;
        socket = reinterpret_cast <Socket*>(ctx);
        char buff[ayu_sizeof_value];
        memset(buff, 0, ayu_sizeof_value);
        int count = evbuffer_copyout(bufferevent_get_input(bev), buff, ayu_sizeof_value);
        if (count == sizeof(ayu_sizeof_value)) {
            size_t message_size = 0;
            std::char_traits <char>::copy((char*) &message_size, &buff[0], ayu_sizeof_value);
            char message_buffer[message_size];
            memset(message_buffer, 0, message_size);
            count = evbuffer_copyout(bufferevent_get_input(bev), message_buffer, message_size);
            if (count == message_size) {
                bool added_to_buffer = socket->add_event_to_read_buffer(message_buffer, bufferevent_getfd(bev), bev);
                if (added_to_buffer) {
                    if (evbuffer_drain(bufferevent_get_input(bev), message_size) == -1) {
                        LOG(ERROR) << "evbuffer_drain FALED";
                    }
                } else {
                    LOG(ERROR) << "BUFFER ADD FALED";
                }
            } else {
                //LOG(ERROR) << "count != message_size" << count <<":"<< message_size;
                return;
            }
        } else {
            //LOG(ERROR) << "count != sizeof(ayu_sizeof_value)" << count <<":"<< sizeof(ayu_sizeof_value)<<":"<< ayu_sizeof_value;
            return;
        }
    }
}

/**
 *
 * @param bev
 * @param ctx
 */
void Socket::write_cb(struct bufferevent *bev, void *ctx)
{
    //LOG(ERROR) << "write_cb ";
    return;
}

int Socket::evbuffer_w(struct bufferevent *bev, const char* buf, const size_t buf_size)
{
    //LOG(ERROR) << "evbuffer_w";
    int r = bufferevent_write(bev, buf, buf_size);
    bufferevent_flush(bev, EV_WRITE, BEV_FLUSH);
    return r;
}

void Socket::event_cb(struct bufferevent *bev, short events, void *ctx)
{

    Socket *socket;
    socket = reinterpret_cast <Socket*>(ctx);
    if (events & BEV_EVENT_ERROR) {
        LOG(ERROR) << "Error from bufferevent";
    }
    if (events & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
        bufferevent_free(bev);
        socket->m_open_sockets--;
        socket->delete_fd(bufferevent_getfd(bev));
    }
}

/**
 *
 * @param listener
 * @param fd
 * @param address
 * @param socklen
 * @param ctx
 */
void Socket::accept_conn_cb(struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *address, int socklen,
                            void *ctx)
{
    //LOG(ERROR) << "accept_conn_cb ";
    Socket *socket;
    socket = reinterpret_cast <Socket*>(ctx);

    /* We got a new connection! Set up a bufferevent for it. */
    struct event_base *base = evconnlistener_get_base(listener);
    struct bufferevent *bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE | BEV_OPT_THREADSAFE);
    bufferevent_setcb(bev, &Socket::read_cb, &Socket::write_cb, &Socket::event_cb, socket);
    bufferevent_enable(bev, EV_READ | EV_WRITE);

    char str[INET_ADDRSTRLEN];

    if (address->sa_family == AF_INET)
    {
        sockaddr_in *sin = reinterpret_cast <sockaddr_in*>(address);
        inet_ntop(AF_INET, &(sin->sin_addr.s_addr), str, INET_ADDRSTRLEN);

        socket->m_open_sockets++;
        socket->register_fd_for_hostname(std::string(str), fd, bev);
    }
    LOG(INFO) << "Accepted connection from " << str << ":" << fd << ":" << bufferevent_getfd(bev);
}

/**
 *
 * @param listener
 * @param ctx
 */

void Socket::accept_error_cb(struct evconnlistener *listener, void *ctx)
{

    LOG(ERROR) << "accept_error_cb ";

    Socket *socket;
    socket = reinterpret_cast <Socket*>(ctx);

    struct event_base *base = evconnlistener_get_base(listener);
    int err = EVUTIL_SOCKET_ERROR();
    event_base_loopexit(base, NULL);

    socket->m_open_sockets--;
    socket->delete_fd(evconnlistener_get_fd(listener));

    fprintf(stderr, "Got an error %d (%s) on the listener. "
            "Shutting down.\n", err, evutil_socket_error_to_string(err));
}

/**
 * server is listening on a socket
 */
int Socket::init_server_socket()
{
    LOG(INFO) << "hostname " << m_port;
    struct sockaddr_in sin;
    struct evconnlistener *listener;

    if (m_port <= 0 || m_port > 65535) {
        LOG(ERROR) << "Invalid port";
        return -1;
    }
    LOG(INFO) << " base" << m_port;

    evthread_use_pthreads(); // configure threads

    base = event_base_new(); // initialize the base

    if (!base) {
        LOG(ERROR) << "Couldn't open event base";
        return -1;
    }

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(0);
    sin.sin_port = htons(m_port);

    listener = evconnlistener_new_bind(base, &Socket::accept_conn_cb, this, LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, -1, (struct sockaddr*) &sin, sizeof(sin));
    if (!listener) {
        perror("Couldn't create listener");
        return -1;
    }

    evconnlistener_set_error_cb(listener, &Socket::accept_error_cb);

//event_base_loop(base, EVLOOP_NO_EXIT_ON_EMPTY | EVLOOP_NONBLOCK);
//event_base_loop(base, EVLOOP_NO_EXIT_ON_EMPTY);

    m_active_thread.store(true);
    m_thread = std::make_shared <std::thread>(&Socket::server_thread, this);
    if (m_thread == nullptr) {
        LOG(ERROR) << "m_thread== nullptr";
        return -1;
    }

//event_base_dispatch(base);

    //LOG(ERROR) << " endDDDDD" << m_port;
    return 0;
}

void Socket::server_thread()
{
    //LOG(ERROR) << " server_loop START";

    event_base_dispatch(base);
    //LOG(ERROR) << " server_loop END";
    event_base_free(base);
    m_active_thread.store(false);
}

