# this will evaluate the argument --with-ompss
# and do a minimal check that the header files are usuable
#
# outputs: 
#    conditional:   ENABLE_OMPSS_PLUGIN
#    ac_subst:      OMPSS_INCLUDE_PATH
AC_DEFUN([AC_AYUDAME_OMPSS], [
  AC_CONFIG_FILES([instrumentations/OmpSs/src/Makefile])
  # the following checks for --with-ompss=<path>
  # and sets ENABLE_OMPSS_PLUGIN and OMPSS_INCLUDE_PATH
  AC_MSG_CHECKING(for --with-ompss argument)
  AC_ARG_WITH([ompss],
  	[AS_HELP_STRING([--with-ompss=<path>], [Path to OmpSs (i.e. Nanos++ RTL) installation directory>])],
  	[with_ompss=$withval],
  	[with_ompss="not set"]
  )
  AC_MSG_RESULT($with_ompss)
  
  # disable plugin pending checks below
  enable_ompss_plugin="no"
  # set variable for instrumenatation/OmpSs/src/Makefile.am
  AC_SUBST(OMPSS_INCLUDE_PATH, $with_ompss/include/nanox-dev)	
  # this will install OmpSs plugin into OmpSs dir 
  # (if ompss_plugin_ins_LT_LIBRARIES also set in OmpSs/src/Makefile.am
  # instead of lib_LT_LIBRARIES)
  # we prefer to install into our dir
  #AC_SUBST(ompss_plugin_insdir, $with_ompss/lib/instrumentation)
  #AC_SUBST(ompss_plugin_ins_dbgdir, $with_ompss/lib/instrumentation-debug)
  
  AS_IF([test "$with_ompss" != "not set"], [
    # test if OmpSs headers usuable; this is a hack
    AC_LANG_PUSH([C++])    # switch to C++
    CPPFLAGS_SAVE=$CPPFLAGS
    echo $CPPFLAGS_SAVE
    CPPFLAGS+=" -Ishould_not_be_here -I$with_ompss/include/nanox-dev -include"new_decl.hpp""
    AC_CHECK_HEADER([plugin.hpp], [enable_ompss_plugin="yes"], [enable_ompss_plugin="no"]) # this does not seem to work with gxx
    CPPFLAGS=$CPPFLAGS_SAVE
    AC_LANG_POP([C++])
  ])
  AM_CONDITIONAL(ENABLE_OMPSS_PLUGIN, test "$enable_ompss_plugin" != "no")	
])
