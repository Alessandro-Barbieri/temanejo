AC_DEFUN([AC_AYUDAME_LIB], [

  # Add the following line if the autoconf complains about
  # "linking libtool libraries using a non-POSIX archiver \
  # requires 'AM_PROG_AR' in 'configure.ac'"
  #m4_ifdef([AM_PROG_AR], [AM_PROG_AR])

  LT_INIT
  AC_CONFIG_HEADERS([config.h])

  #
  # Check for programs ###
  #

  AC_PROG_CXX
  # really necessary?
  #AC_PROG_CC
  AC_PROG_INSTALL
  AC_PROG_LN_S
  AC_PROG_CPP

  #
  # Check for libraries ###
  #

  # FIXME: Replace `main' with a function in `-lpthread':
  AC_CHECK_LIB([pthread], [main])

  #
  # Check for header files ###
  #

  AC_CHECK_HEADERS([stdint.h unistd.h])

  #
  # Check for typedefs, structures, and compiler characteristics ###
  #

  AC_HEADER_STDBOOL
  AC_TYPE_INT64_T
  AC_TYPE_SIZE_T
  AC_TYPE_UINT32_T
  AC_TYPE_UINT64_T
  AC_TYPE_UINT8_T

  #
  # Check for library functions ###
  #

  AC_CHECK_FUNCS([memset])

  #
  # Add configure options --enable-stdout, with a default disabled
  #

  #AC_ARG_ENABLE([stdout],
  #	[AS_HELP_STRING([--enable-stdout@<:@=yes/no@:>@, --disable-stdout],
  #	  [Enable or disable output to stdout. Default is disabled.])],
  #	[ if test "x$enableval" = "yes" ; then
  #	    AC_DEFINE([AYU_WRITETOSTDOUT],[1],[Enable output to stdout])
  #	  else
  #	    AC_DEFINE([AYU_WRITETOSTDOUT],[0],[Disable output to stdout])
  #	  fi ],
  #	[AC_DEFINE([AYU_WRITETOSTDOUT],[0],[Enable output to stdout])])

  AC_ARG_ENABLE([stdout],
   [AS_HELP_STRING([--enable-stdout=yes|no],
  		 [Enable or disable output to stdout. Default is disabled.])],
   [enable_stdout="$enableval"],
   [enable_stdout="no"])

   if test "x$enable_stdout" = "xyes" ; then
     AC_DEFINE([AYU_WRITETOSTDOUT],[1],[Enable or disable output to stdout])
   else
     AC_DEFINE([AYU_WRITETOSTDOUT],[0],[Enable or disable output to stdout])
   fi


  #
  # Add configure options --enable-socket, with a default enabled
  #

  AC_ARG_ENABLE([socket],
    [AS_HELP_STRING([--enable-socket=yes|no],
  		  [Enable or disable connection to socket.
  		   Default is enabled.])],
    [ if test "x$enableval" = "xyes" ; then
        AC_DEFINE([AYU_CONNECTTOSOCKET],[1],
  		[Enable or disable connection to socket])
        AC_CHECK_FUNCS([inet_ntoa socket])
        AC_CHECK_HEADERS([arpa/inet.h netinet/in.h sys/socket.h])
      else
        AC_DEFINE([AYU_CONNECTTOSOCKET],[0],
  		[Enable or disable connection to socket])
      fi
    ],
    [AC_DEFINE([AYU_CONNECTTOSOCKET],[1],
  	     [Enable or disable connection to socket])
     AC_CHECK_FUNCS([inet_ntoa socket])
     AC_CHECK_HEADERS([arpa/inet.h netinet/in.h sys/socket.h])
  	     ])

  #AC_ARG_ENABLE([socket],
  # [AS_HELP_STRING([--enable-socket, --disable-socket],
  #		 [Enable or disable the connections to a socket.
  #		 Default is enabled.])],
  # [enable_socket="$enableval"],
  # [enable_socket="no"])
  #
  # if test "x$enable_socket" = "xyes" ; then
  #   AC_DEFINE([AYU_CONNECTTOSOCKET],[1],[Enable the connections to a socket])
  # else
  #   AC_DEFINE([AYU_CONNECTTOSOCKET],[0],[Disable the connections to a socket])
  # fi

  #
  # Add configure options --enable-verbose, with a default disabled
  #

  AC_ARG_ENABLE([verbose],
    [AS_HELP_STRING([--enable-verbose],
      [Set verbosity level, Default 0])],
    [if   test "x$enableval" = "x0" ; then
       AC_DEFINE([AYU_VERBOSE],[0],[Define verbosity level])
     elif test "x$enableval" = "x1" ; then
       AC_DEFINE([AYU_VERBOSE],[1],[Define verbosity level])
     elif test "x$enableval" = "x2" ; then
       AC_DEFINE([AYU_VERBOSE],[2],[Define verbosity level])
     elif test "x$enableval" = "x3" ; then
       AC_DEFINE([AYU_VERBOSE],[3],[Define verbosity level])
     else
       echo
       echo "ERROR: BAD verbose mode. Allowed values are 0, 1, 2, 3"
       exit -1
     fi
    ],
    [AC_DEFINE([AYU_VERBOSE],[0],[Define verbosity level])])


  #
  # Include different headers according to operating system types
  #

  AC_CANONICAL_BUILD
  AS_CASE([$build_os],
  [linux],[AC_CHECK_HEADERS([endian.h],[
  #include <endian.h>
  ],[],[])],
  [freebsd],[AC_CHECK_HEADERS([sys/endian.h],[
  #include <sys/endian.h>
  ],[],[])],
  [netbsd],[AC_CHECK_HEADERS([sys/endian.h],[
  #include <sys/endian.h>
  ],[],[])],
  [openbsd],[AC_CHECK_HEADERS([sys/endian.h],[
  #include <sys/types.h>
  AC_DEFINE([be64toh(x)],[betoh64(x)],[Adjust be64toh function])
  ],[],[])],
  []
  )


  AC_CONFIG_FILES([Makefile src/Makefile doc/Makefile test/Makefile])

])
