#ifndef TCA_TOOL_H
#define TCA_TOOL_H

#include <stdint.h>
#include <tca_types.h>



typedef void (*tca_interface_fn_t)(void);

typedef tca_interface_fn_t (*tca_function_lookup_t)(const char *entry_point);





extern tca_interface_fn_t lookup(const char *entry_point) __attribute__ ((weak));


/* mandatory
   ask runtime to resume normal task execution and run freely.
   TCA_API tca_success_t tca_request_continue();
*/
typedef tca_success_t (*tca_request_continue)(void);

/* mandatory
   ask runtime to stop executing any tasks.
   TCA_API tca_success_t tca_request_break();
*/
typedef tca_success_t (*tca_request_break)(void);

/* mandatory
   ask runtime to execute next task.
   TCA_API tca_success_t tca_request_step();
*/
typedef tca_success_t (*tca_request_step)(void);

/* mandatory
   ask runtime to shutdown, no tca resource will be valid after call returns. 
   TCA_API tca_success_t tca_request_shutdown();
*/
typedef tca_success_t (*tca_request_shutdown)(void);

/* optional
   ask runtime to block task with id "id", i.e. do not execute task.
   TCA_API tca_success_t tca_request_block_task(tca_task_id_t id);
*/
typedef tca_success_t (*tca_request_block_task)(tca_task_id_t id);

/* optional
   ask runtime to unblock task with id "id", allow execution of task.
   TCA_API tca_success_t tca_request_unblock_task(tca_task_id_t id);
*/
typedef tca_success_t (*tca_request_unblock_task)(tca_task_id_t id);

/* optional
   ask runtime to forceably execute task with id "id" next.
   TCA_API tca_success_t tca_request_run_task(tca_task_id_t id);
*/
typedef tca_success_t (*tca_request_run_task)(tca_task_id_t id);

/* optional
   insert (artificial) dependency between tasks with ids "to" and "from", i.e. 
   do not run "from" before "to" has finished.
   TCA_API tca_success_t tca_request_insert_dependency(
     tca_dependency_id_t from, tca_dependency_id_t to); 
*/
typedef tca_success_t (*tca_request_insert_dependency)(
   tca_dependency_id_t from, tca_dependency_id_t to); 

/* optional
   remove dependency with id "id".
   TCA_API tca_success_t tca_request_remove_dependency(tca_dependency_id_t id);
*/
typedef tca_success_t (*tca_request_remove_dependency)(tca_dependency_id_t id);

/* optional
   break when task with id "id" is reached.
   TCA_API tca_success_t tca_request_break_at_task(tca_task_id_t id);
*/
typedef tca_success_t (*tca_request_break_at_task)(tca_task_id_t id);

/* optional
   remove any break_at_task for task with id "id".
   TCA_API tca_success_t tca_request_unbreak_at_task(tca_task_id_t id);
*/
typedef tca_success_t (*tca_request_unbreak_at_task)(tca_task_id_t id);

/* optional
   break when a task is reached that involves dependency with id "id".
   TCA_API tca_success_t tca_request_break_at_dependency(tca_dependency_id_t id);
*/
typedef tca_success_t (*tca_request_break_at_dependency)(tca_dependency_id_t id);

/* optional
   remove any break_at_dependency for dependency with id "id".
   TCA_API tca_success_t tca_request_unbreak_at_dependency(tca_dependency_id_t id);
*/
typedef tca_success_t (*tca_request_unbreak_at_dependency)(tca_dependency_id_t id);

/* optional
   ask runtime to resume normal task execution on thread "id".
   TCA_API tca_success_t tca_request_continue_thread(tca_thread_id_t id);
*/
typedef tca_success_t (*tca_request_continue_thread)(tca_thread_id_t id);

/* optional
   ask runtime to stop executing any tasks in thread "id".
   TCA_API tca_success_t tca_request_break_thread(tca_threadid_t id);
*/
typedef tca_success_t (*tca_request_break_thread)(tca_thread_id_t id);

/* optional
   ask runtime to execute next task on thread "id".
   TCA_API tca_success_t tca_request_step_thread(tca_thread_id_t id);
*/
typedef tca_success_t (*tca_request_step_thread)(tca_thread_id_t id);


/* optional
   ask runtime to set the priotiry of a task "id".
   TCA_API tca_success_t tca_request_set_priority(tca_task_id_t id, tca_priority_t priority);
*/
typedef tca_success_t (*tca_request_set_priority)(tca_task_id_t id, tca_priority_t priority);

#endif /* TCA_TOOL_H */


