#ifndef TCA_RT_HPP
#define TCA_RT_HPP
#define TCA_VERSION 1

#ifndef TCA_RT_VERBOSE
#define TCA_RT_VERBOSE 0
#endif

#include "tca_types.h"
#include <stdio.h>

/* mandatory */
/* ask runtime to resume normal task execution and run freely */
tca_success_t tca_request_continue();

/* mandatory */
/* ask runtime to stop executing any tasks (mandatory) */
tca_success_t tca_request_break();

/* mandatory */
/* ask runtime to execute next task  (mandatory) */
tca_success_t tca_request_step();

/* mandatory */
/* ask runtime to shutdown, no tca resource will be valid after call returns */
tca_success_t tca_request_shutdown();

/* optional */ // or mandatory to emulate "insert dependency"
/* ask runtime to block task with id "id", i.e. do not execute task */
tca_success_t tca_request_block_task(tca_task_id_t id);

/* optional */
/* ask runtime to unblock task with id "id", allow execution of task */
tca_success_t tca_request_unblock_task(tca_task_id_t id);

/* optional */
/* ask runtime to forceably execute task with id "id" next */
tca_success_t tca_request_run_task(tca_task_id_t id);

/* optional */
/* insert (artificial) dependency between tasks with ids "to" and "from", i.e.
 do not run "from" before "to" has finished */
tca_success_t tca_request_insert_dependency(tca_task_id_t from, tca_task_id_t to);

/* optional */
/* remove dependency with id "id" */
tca_success_t tca_request_remove_dependency(tca_dependency_id_t id);

/* optional */
/* break when task with id "id" is reached */
tca_success_t tca_request_break_at_task(tca_task_id_t id);

/* optional */
/* remove any break_at_task for task with id "id" */
tca_success_t tca_request_unbreak_at_task(tca_task_id_t id);

/* optional */
/* break when a task is reached that involves dependency with id "id" */
tca_success_t tca_request_break_at_dependency(tca_dependency_id_t id);

/* optional */
/* remove any break_at_dependency for dependency with id "id" */
tca_success_t tca_request_unbreak_at_dependency(tca_dependency_id_t id);

/* optional */
/* ask runtime to resume normal task execution on thread "id" */
tca_success_t tca_request_continue_thread(tca_thread_id_t id);

/* optional */
/* ask runtime to stop executing any tasks in thread "id" */
tca_success_t tca_request_break_thread(tca_thread_id_t id);

/* optional */
/* ask runtime to execute next task on thread "id" */
tca_success_t tca_request_step_thread(tca_thread_id_t id);

/* optional */
/*   ask runtime to set the priotiry of a task "id".*/
tca_success_t tca_request_set_priority(tca_task_id_t id, tca_priority_t priority);



tca_interface_fn_t lookup(const char *entry_point);

#endif // TCA_RT_HPP
