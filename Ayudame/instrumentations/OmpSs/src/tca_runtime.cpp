#include "tca_runtime.h"
#include "simple_stepper.hpp"

#include <string.h>



tca_success_t tca_request_continue()
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_continue: got req 'tca_request_continue'\n");
#endif
    stepper::set_progress(TCA_CONTINUE);
    return TCA_OK;

}

tca_success_t tca_request_break()
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_break: got req 'tca_request_break'\n");
#endif
    stepper::set_progress(TCA_BREAK);
    return TCA_OK;

}

tca_success_t tca_request_step()
{
    /*
     //call nanos api
     nanos::myThread->setSteps(1);

     */
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_step: got req 'tca_request_step'\n");
#endif
    stepper::increase_step_counter();
    return TCA_OK;

}

tca_success_t tca_request_shutdown()
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_shutdown: got req 'tca_request_shutdown'\n");
    printf("rt tca_request_shutdown: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_block_task(tca_task_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_block_task: got req 'tca_request_block_task'\n");
    printf("rt tca_request_block_task:     with id %u\n", id);
#endif
    stepper::add_task(id);
    return TCA_OK;

}

tca_success_t tca_request_unblock_task(tca_task_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_unblock_task: got req 'tca_request_unblock_task'\n");
    printf("rt tca_request_unblock_task:     with id %u\n", id);
#endif
    stepper::remove_task(id);
    return TCA_OK;

}

tca_success_t tca_request_run_task(tca_task_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_run_task: got req 'tca_request_run_task'\n");
    printf("rt tca_request_run_task:     with id %u\n", id);
    printf("rt tca_request_run_task: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_insert_dependency(tca_task_id_t from, tca_task_id_t to)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_insert_dependency: got req 'tca_request_insert_dependency'\n");
    printf("rt tca_request_insert_dependency:     with from id %u and to id %u\n", from, to);
    printf("rt tca_request_insert_dependency: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_remove_dependency(tca_dependency_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_remove_dependency: got req 'tca_request_remove_dependency'\n");
    printf("rt tca_request_remove_dependency:     with id %u\n", id);
    printf("rt tca_request_remove_dependency: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_break_at_task(tca_task_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_break_at_task: got req 'tca_request_break_at_task'\n");
    printf("rt tca_request_break_at_task:     with id %u\n", id);
    printf("rt tca_request_break_at_task: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_unbreak_at_task(tca_task_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_unbreak_at_task: got req 'tca_request_unbreak_at_task'\n");
    printf("rt tca_request_unbreak_at_task:     with id %u\n", id);
    printf("rt tca_request_unbreak_at_task: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_break_at_dependency(tca_dependency_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_break_at_dependency: got req 'tca_request_break_at_dependency'\n");
    printf("rt tca_request_break_at_dependency:     with id %u\n", id);
    printf("rt tca_request_break_at_task: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_unbreak_at_dependency(tca_dependency_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_unbreak_at_dependency: got req 'tca_request_unbreak_at_dependency'\n");
    printf("rt tca_request_unbreak_at_dependency:     with id %u\n", id);
    printf("rt tca_request_unbreak_at_dependency: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_continue_thread(tca_thread_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_continue_thread: got req 'tca_request_continue_thread'\n");
    printf("rt tca_request_continue_thread:     with id %u\n", id);
#endif
    stepper::remove_thread(id);
    return TCA_OK;

}

tca_success_t tca_request_break_thread(tca_thread_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_break_thread: got req 'tca_request_break_thread'\n");
    printf("rt tca_request_break_thread:     with id %u\n", id);
#endif
    stepper::add_thread(id);
    return TCA_OK;

}

tca_success_t tca_request_step_thread(tca_thread_id_t id)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_step_thread: got req 'tca_request_step_thread'\n");
    printf("rt tca_request_step_thread:     with id %u\n", id);
    printf("rt tca_request_step_thread: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_success_t tca_request_set_priority(tca_task_id_t id, tca_priority_t priority)
{
#if TCA_RT_VERBOSE > 0
    printf("rt tca_request_set_priority: got req 'tca_request_set_priority'\n");
    printf("rt tca_request_set_priority:     with id %u and priority %u\n", id, priority);
    printf("rt tca_request_set_priority: NOT IMPLEMENTED\n");
#endif
    return TCA_OK;

}

tca_interface_fn_t lookup(const char *entry_point)
{
#if TCA_RT_VERBOSE > 0
    printf("rt my_lookup: got entry_point  = %s\n", entry_point);
#endif

    if (0 == strncmp(entry_point, "tca_request_continue", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_continue;
    }
    else if (0 == strncmp(entry_point, "tca_request_break", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_break;
    }
    else if (0 == strncmp(entry_point, "tca_request_step", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_step;
    }
    else if (0 == strncmp(entry_point, "tca_request_shutdown", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_shutdown;
    }
    else if (0 == strncmp(entry_point, "tca_request_block_task", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_block_task;
    }
    else if (0 == strncmp(entry_point, "tca_request_unblock_task", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_unblock_task;
    }
    else if (0 == strncmp(entry_point, "tca_request_run_task", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_run_task;
    }
    else if (0 == strncmp(entry_point, "tca_request_insert_dependency", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_insert_dependency;
    }
    else if (0 == strncmp(entry_point, "tca_request_remove_dependency", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_remove_dependency;
    }
    else if (0 == strncmp(entry_point, "tca_request_break_at_task", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_break_at_task;
    }
    else if (0 == strncmp(entry_point, "tca_request_unbreak_at_task", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_unbreak_at_task;
    }
    else if (0 == strncmp(entry_point, "tca_request_break_at_dependency", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_break_at_dependency;
    }
    else if (0 == strncmp(entry_point, "tca_request_unbreak_at_dependency", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_unbreak_at_dependency;
    }
    else if (0 == strncmp(entry_point, "tca_request_continue_thread", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_continue_thread;
    }
    else if (0 == strncmp(entry_point, "tca_request_break_thread", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_break_thread;
    }
    else if (0 == strncmp(entry_point, "tca_request_step_thread", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_step_thread;
    }
    else if (0 == strncmp(entry_point, "tca_request_set_priority", _comparelimit)) {
        return (tca_interface_fn_t) &tca_request_set_priority;
    }
    else {
        printf("no lookup definded for %s\n", entry_point);
        return NULL;
    }

    // should not reach here!!
    return NULL;
}

