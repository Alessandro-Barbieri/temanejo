#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include <string.h>
#include "ayu_mpi.h"


int MPI_Finalize( void ) {
    int ierr;
    ierr = MPI_Barrier(MPI_COMM_WORLD);


    counterMPI++;
    mpiElem  elem = { aympi_rank, counterMPI, "MPI_Finalize", -1, -1, 0, NULL}; /*id,fname,partner,tag,comm,request*/

#ifdef AYU_MPI_DEBUG

    fprintf (stderr, "MPI task: MyRank = %d | ID = %d | Name = %s \n",
             elem.rank, elem.id, elem.fname);
#endif



#ifdef AYUDAME

    if(ayu_event) {
        /* take a client ID and declaration of variables */
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t scope_id = 0;
        ayu_id_t task_id = 0;
        ayu_id_t dependency_id;
        ayu_event_data_t data;
        data.common.client_id = myclient_id;



        task_id =  counterMPI;
        MPI_Barrier( MPI_COMM_WORLD );


        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;

        char* buf = (char*) malloc(100);
        memset(buf, 0, 100);
        //sprintf(buf, "AYU_ADDTASK_MPI_Finalize %d", aympi_rank);
        sprintf(buf, "AYU_ADDTASK_MPI_Finalize");

        data.add_task.task_label = buf;
        ayu_event(AYU_ADDTASK, data);

        ayu_wipe_data(&data);

        data.common.client_id = myclient_id;
        dependency_id = 1000 + (task_id-1)*task_id;
        data.add_dependency.dependency_id = dependency_id;
        data.add_dependency.from_id = task_id - 1;
        data.add_dependency.to_id = task_id;
        data.add_dependency.dependency_label= "AYU_ADDDEPENDENCY_MPI_Finalize";
        ayu_event(AYU_ADDDEPENDENCY, data);

        free(buf);


        //sleep(5);
        ayu_wipe_data(&data);
        data.common.client_id = myclient_id;
        ayu_event(AYU_FINISH, data);


    }
#endif

    MPI_Barrier( MPI_COMM_WORLD );

    ierr = PMPI_Finalize();





    return ierr;
}
