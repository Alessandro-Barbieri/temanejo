#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include "ayu_mpi.h"

//needed for get path in initialize
#include <unistd.h>
unsigned int counterMPI = 0;
unsigned int counterCOMM = 0;

int aympi_rank = -1;  // -1 means no MPI

#define MAIN_SCOPE 0

int MPI_Init( int *argc, char ***argv )
{
    int ierr=0;
    int size=0;

#ifdef AYU_MPI_DEBUG
    printf("Aydame MPI debugger\n");
#endif

    ierr = PMPI_Init (argc, argv);
    ierr = PMPI_Barrier(MPI_COMM_WORLD);

    MPI_Comm_rank (MPI_COMM_WORLD, &aympi_rank);        /* get current process id */
    MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */
    ranks=size;


    int i =0;

    mpiElem elem_empty = { -1, 0,"", -1, -1, -1, NULL};
    for (i=0;i<ARRAY_SIZE;i++){
        requestList[i]= elem_empty;
    }

    /*
    i = 0;
    for(i=0;i<=ARRAY_SIZE-1;i++){
             mpiElem elem =requestList[i];
         printf ("-------------------MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | request = %p \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.request);
    }
    */



#ifdef AYU_MPI_DEBUG
    if (aympi_rank == 0)
        printf ( "MPIAYU_INIT=%d \n", size);
#endif

    counterMPI = MAIN_SCOPE+1;
    counterCOMM = 0;

    mpiElem elem = { aympi_rank, counterMPI, "MPI_Init",  -1, -1, 0, NULL}; /*id,fname,rank,partner,tag,comm,request*/
#ifdef AYUDAME

    if(ayu_event)
    {
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_event_data_t data;

        //############################
        data.common.client_id = myclient_id;
        data.add_task.task_id = MAIN_SCOPE;
        data.add_task.scope_id = MAIN_SCOPE;
        data.add_task.task_label = "Main";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        int pid= getpid();
        char pid_buffer[1024];
        sprintf(pid_buffer,"%llu",pid);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= MAIN_SCOPE;
        data.set_property.key="pid";
        data.set_property.value=pid_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        char hostname[128];
        gethostname(hostname, sizeof hostname);
        struct hostent* h;
        h = gethostbyname(hostname);
        char h_name_buffer[1024];
        sprintf(h_name_buffer,"%s", h->h_name);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= MAIN_SCOPE;
        data.set_property.key="hostname";
        data.set_property.value=h_name_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        char buff[1024];
        memset(buff,0,sizeof(buff));
        ssize_t len = readlink("/proc/self/exe", buff, sizeof(buff)-1);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= MAIN_SCOPE;
        data.set_property.key="path+application";
        data.set_property.value=buff;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        //############################



        ayu_id_t scope_id = 0;
        ayu_id_t task_id=0;
        data.common.client_id = myclient_id;

        task_id = counterMPI;


//        MPI_Barrier( MPI_COMM_WORLD );

        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;


        char* buf = (char*) malloc(100);
        memset(buf, 0, 100);
        //printf(buf, "AYU_ADDTASK_MPI_Init %d", aympi_rank);
        sprintf(buf, "AYU_ADDTASK_MPI_Init");

        data.add_task.task_label = buf;
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        free(buf);
    }
#endif

#ifdef AYU_MPI_DEBUG
    printf ("MPI task: MyRank = %d | ID = %d | Name = %s  \n", elem.rank, elem.id, elem.fname);
#endif


    ierr = PMPI_Barrier(MPI_COMM_WORLD);
    return ierr;
}




int MPI_Init_thread(int *argc, char ***argv,
                    int required, int *provided)
{
    int ierr=0;
    int size=0;

#ifdef AYU_MPI_DEBUG
    printf("Aydame MPI debugger\n");
#endif

    ierr = PMPI_Init_thread (argc, argv, required, provided);
    ierr = PMPI_Barrier(MPI_COMM_WORLD);

    MPI_Comm_rank (MPI_COMM_WORLD, &aympi_rank);        /* get current process id */
    MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */


#ifdef AYU_MPI_DEBUG
    if (aympi_rank == 0)
        printf ( "MPIAYU_INIT_THREAD=%d \n", size);
#endif

    counterMPI = 0;
    counterCOMM = 0;

    mpiElem elem = { aympi_rank, counterMPI, "MPI_Init_thread",  -1, -1, 0, NULL}; /*id,fname,rank,partner,tag,comm,request*/

#ifdef AYUDAME

    if(ayu_event)
    {
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t scope_id = 0;
        ayu_id_t task_id=0;
        ayu_event_data_t data;
        data.common.client_id = myclient_id;



        task_id = counterMPI;


//        MPI_Barrier( MPI_COMM_WORLD );

        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;


        char* buf = (char*) malloc(100);
        memset(buf, 0, 100);
        //sprintf(buf, "AYU_ADDTASK_MPI_Init %d", aympi_rank);
        sprintf(buf, "AYU_ADDTASK_MPI_Init_Thread");

        data.add_task.task_label = buf;
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        free(buf);
    }
#endif

#ifdef AYU_MPI_DEBUG
    printf ("MPI task: MyRank = %d | ID = %d | Name = %s  \n", elem.rank, elem.id, elem.fname);
#endif


    ierr = PMPI_Barrier(MPI_COMM_WORLD);
    return ierr;
}
