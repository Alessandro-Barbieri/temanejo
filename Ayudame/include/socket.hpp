/**
 * \file socket.hpp
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef AYUDAME_SOCKET_HPP
#define AYUDAME_SOCKET_HPP

#include "ayudame_types.h"

#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory>
#include <iostream>

#include <fcntl.h>

#include <thread>
#include <atomic>
#include <mutex>

#include <vector>

#include "intern_event/intern_event.hpp"
/* Libevent. */
#include <event2/event.h>
#include <event2/event_struct.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/thread.h>
#include <event2/util.h>


#include <arpa/inet.h>

/**
 * \brief Class representing an socket connection
 * This class can be a server or a client socket
 * The server socket is implemented with an epoll event
 * and has also an own thread reading the server socket and
 * writing events into the buffer.
 *
 * \class Socket
 * \ingroup socket
 */
class Socket
{

public:

    /**
     *
     */
    Socket();

    /**
     *
     */
    Socket(const Socket& _s);

    /**
     *
     */
    Socket& operator=(const Socket& _s);

    /**
     *
     */
    virtual ~Socket();

    //Server will have only a port
    //Client will have host:port
    /**
     * Function sets the server port
     * @param port Port
     */
    void set_server_data(int port);

    /**
     * Function sets the Hostname and Port where the client has to connect to
     * @param port Port
     * @param host Hostname
     */
    void set_client_data(int port, std::string host);

    /**
     * Function initializes the server or client
     * @return -1 if failed
     */
    int init();

    /**
     * Checks if the socket is shutdownable
     * @return true if you can shutdown the socket
     */
    virtual bool is_shutdownable();

    /**
     * Forces the socket to shudown
     */
    virtual void force_shutdown();

    /**
     * This function will shutdown the socket in a safe way.
     * If the socket is a server:
     *  It will wait until are incomming connections are closed
     *  After all connection are closed the internal thread will be joined
     * If the socket is a client:
     *  The close event will be send out
     */
    virtual void shutdown_socket();
    //int get_buffer_size();

protected:
    int m_sfd; // server socket handle
    std::vector <std::shared_ptr <Intern_event>> m_read_buffer;
    int m_port;
    std::string m_host;

    int evbuffer_w(struct bufferevent *bev, const char* message_buffer, const size_t buf_size);
private:
    int socket_write(const char *buf, size_t buf_size, int send_fd);

    static void accept_error_cb(struct evconnlistener *listener, void *ctx);
    static void accept_conn_cb(struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *address, int socklen, void *ctx);
    static void event_cb(struct bufferevent *bev, short events, void *ctx);
    static void write_cb(struct bufferevent *bev, void *ctx);
    static void read_cb(struct bufferevent *bev, void *ctx);

    void loop();

    virtual bool add_event_to_read_buffer(const char * message_buffer, int infd,struct bufferevent* bev)=0;


    virtual void register_fd_for_hostname(std::string hostname, int fd, struct bufferevent* bev)=0;
    virtual int get_fd_for_hostname(uint64_t index)=0;
    virtual struct bufferevent* get_bev_for_hostname(uint64_t index)=0;
    virtual int get_fd_for_proc(uint64_t proc)=0;
    virtual struct bufferevent* get_bev_for_proc(uint64_t proc)=0;
    virtual void delete_fd(int fd)=0;



    void server_thread();


    int init_server_socket();
    int init_client_socket();


    std::shared_ptr <std::thread> m_thread;
    std::atomic <bool> m_active_thread;

    //std::vector<std::shared_ptr <Intern_event>> m_buffer;

    //struct sockaddr_in m_server_addr; // address structs for server socket
    //struct sockaddr_in m_client_addr;// address structs for client socket

    struct event_base *base;
    //struct evconnlistener *listener;



protected:

    typedef enum
    {
        NONE = 0,
        CLIENT = 1,
        SERVER= 3
    }ayu_socket_t;

    std::mutex m_mutex_event;

    ayu_socket_t m_socket_type;
    std::atomic<int> m_open_sockets;

};

#endif // AYUDAME_SOCKET_HPP
