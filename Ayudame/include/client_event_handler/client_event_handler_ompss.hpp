/**
 * \file client_event_handler_ompss.hpp
 * \brief header of the class Client_event_handler_ompss
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */

#ifndef CLIENT_EVENT_HANDLER_OMPSS_HPP_
#define CLIENT_EVENT_HANDLER_OMPSS_HPP_

class Client_event_manager;

#include "ayudame_types.h"

#include "client_event_handler/client_event_handler.hpp"

/**
 * \class Client_event_handler_mpi
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_mpi
 * */
class Client_event_handler_ompss: public Client_event_handler
{

public:

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //  using Client_event_handler::Client_event_handler;
    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_ompss(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client);

    /**
     *
     */
    Client_event_handler_ompss() = delete;

    /**
     *
     */
    virtual ~Client_event_handler_ompss();

    /**
     *
     * @param request
     */
    virtual void msg_send_out(std::shared_ptr <Intern_event> request);

    virtual void shutdown();

private:

    tca_request_continue req_continue;
    tca_request_break req_break;
    tca_request_step req_step;
    tca_request_block_task req_block_task;
    tca_request_unblock_task req_unblock_task;

    virtual void tca_initialize(tca_function_lookup_t lookup,
                                const char *runtime_version,
                                unsigned int tca_version);

};

#endif /* CLIENT_EVENT_HANDLER_OMPSS_HPP_ */
