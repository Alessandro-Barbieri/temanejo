/**
 * \file connect_manager.hpp
 * \brief header of the class connect_manager
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONNECT_MANAGER_HPP
#define CONNECT_MANAGER_HPP

#include <map>
#include <memory>

#include "connect_handler/connect_handler_dot.hpp"
#include "connect_handler/connect_handler_xml.hpp"
#include "connect_handler/connect_handler_socket.hpp"
#include "connect_handler/connect_handler_stdout_human.hpp"
#include "connect_handler/connect_handler_stdout_raw.hpp"
#include "connect_handler/connect_handler_temanejo.hpp"
#include "connect_handler/connect_handler_temanejo_old.hpp"
#include "intern_event/intern_event.hpp"

#include "ayudame_types.h"



class Ayudame;
class Connect_handler;


/**
 * \class Connect_manager
 * \brief Class Connect_manager
 * */
class Connect_manager {

    /**
     *
     */
    typedef std::map <ayu_connect_t, std::shared_ptr <Connect_handler>> connect_handlers_map;

    /**
     *
     */
    typedef std::map <ayu_connect_t,bool > connect_handlers_status_map;

public:

    /**
     *
     * @param ayu
     */
    Connect_manager(Ayudame& ayu);

    /**
     *
     */
    Connect_manager() = delete;

    /**
     *
     */
    ~Connect_manager();

    /**
     *
     * @param data
     */
    void msg_send_out(std::shared_ptr<Intern_event> data);

    /**
     *
     * @param connect
     */
    void init_connect(const ayu_connect_t connect);


    const std::shared_ptr<Intern_event> msg_read();

    /**
     *
     */
    void activate_connect(const ayu_connect_t connect);

    /**
     *
     * @param connect
     */
    void deactivate_connect(const ayu_connect_t connect);

    /**
     *
     */
    void deactivate_all_connect();

    /**
     *
     * @param connect
     * @return
     */
    Connect_handler* get_connect(const ayu_connect_t connect);


    void shutdown();

private:

    /**
     *
     * @param connect
     * @return
     */
    std::shared_ptr <Connect_handler> create_connect_handler(const ayu_connect_t connect);

    /**
     *
     */
    Ayudame& m_ayu;

    /**
     *
     */
    connect_handlers_map m_connect_handlers;

    /**
     *
     */
    connect_handlers_status_map m_connect_handlers_status;
};

#endif // CONNECT_MANAGER_HPP
