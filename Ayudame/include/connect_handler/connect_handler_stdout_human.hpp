/**
 * \file connect_handler_stdout_human.hpp
 * \brief header of the class Connect_handler_human
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONNECT_HANDLER_STDOUT_HUMAN_HPP_
#define CONNECT_HANDLER_STDOUT_HUMAN_HPP_

#include <cstdio>
#include <memory>

#include "connect_handler/connect_handler.hpp"

class Connect_manager;


/**
 * \class Connect_handler_human
 *  @copydoc Connect_handler
 * \brief Class Connect_handler_human
 * */
class Connect_handler_stdout_human : public Connect_handler {

public:

    /**
     *
     * @param manager
     * @param ayu
     */
    Connect_handler_stdout_human(Connect_manager& manager, Ayudame& ayu)
            : Connect_handler(manager, ayu) {}

    /**
     *
     */
    Connect_handler_stdout_human() = delete;

    void shutdown(){}
    /**
     *
     * @param data
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> data);
    virtual const std::shared_ptr<Intern_event> msg_read();
};

#endif /* CONNECT_HANDLER_STDOUT_HUMAN_HPP_ */
