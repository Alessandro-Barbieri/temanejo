/**
 * \file intern_request.hpp
 * \brief Header of the intern request event
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_REQUEST_HPP_
#define INTERN_REQUEST_HPP_

#include <iostream>

#include "intern_event/intern_event.hpp"
#include "ayudame_types.h"


class Intern_request: public Intern_event {

public:

    /**
     * \brief
     * no creation without initialisation (RAII)
     */
    Intern_request() = delete;

    /**
     * \brief ctor with information passed one by one
     *
     *
     * \param _client_id
     * \param _event
     * \param _timestamp
     * \param _property_owner_id
     * \param _key
     * \param _value
     */
    Intern_request(ayu_client_id_t _client_id,
                   ayu_count_t _timestamp,
                   ayu_request_t _request,
                   std::string _key,
                   std::string _value);


    /**
     * \brief ctor from packed data
     *
     * \param message
     * \todo maybe pass a protocol version eventually
     */
    Intern_request(const char *message);

    /**
     * \brief
     * \param message
     */
    void pack(char* message);

    /**
     * \brief
     * \return
     */
    virtual ayu_count_t get_message_size() const;

    /**
     * \brief
     * \param printformat
     * \param os
     */
    void print(ayu_printformat_t printformat,
               std::ostream &os = std::cout);


    /**
     * \brief tbd
     * \return
     */
    ayu_request_t get_request() const {
        return m_request;
    }

    std::string get_key() const {
        return m_key;
    }

    std::string get_value() const {
        return m_value;
    }

private:

    /**
     * \brief
     */
    ayu_request_t m_request;

    /**
     * \brief
     */
    std::string m_key;

    /**
     * \brief
     */
    std::string m_value;


protected:

};

#endif /* INTERN_REQUEST_HPP_ */
