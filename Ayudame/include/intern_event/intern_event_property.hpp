/**
 * \file intern_event_property.hpp
 * \brief Header of the internal event object for property
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_SET_PROPERTY_HPP_
#define INTERN_SET_PROPERTY_HPP_

#include <iostream>

#include "intern_event/intern_event.hpp"
#include "ayudame_types.h"

/**
 * \class Intern_event_property
 *
 * \brief Class Intern_event_property
 *
 * \ingroup intern_event
 * */
class Intern_event_property: public Intern_event {

public:

    /**
     * \brief
     * no creation withou initialisation (RAII)
     */
    Intern_event_property() = delete;

    /**
     * \brief ctor with information passed one by one
     *
     *
     * \param _client_id
     * \param _event
     * \param _timestamp
     * \param _property_owner_id
     * \param _key
     * \param _value
     */
    Intern_event_property(ayu_client_id_t _client_id,
                          ayu_event_t _event,
                          ayu_count_t _timestamp,
                          ayu_id_t _property_owner_id,
                          std::string _key,
                          std::string _value);

    /**
     * \brief
     * \param data
     */
    Intern_event_property(ayu_event_data_t data);

    /**
     * \brief ctor from packed data
     *
     * \param message
     * \todo maybe pass a protocol version eventually
     */
    Intern_event_property(const char *message);

    /**
     * \brief
     * \param message
     */
    void pack(char* message);

    /**
     * \brief
     * \return
     */
    virtual ayu_count_t get_message_size() const;

    /**
     *
     * \brief
     * @return
     */
    ayu_id_t get_property_owner_id() {
        return m_property_owner_id;
    }

    /**
     * \brief
     * \return
     */
    std::string get_key() {
        return m_key;
    }
    /**
     * \brief
     * \return
     */
    std::string get_value() {
        return m_value;
    }


    /**
     * \brief
     * \param printformat
     * \param os
     */
    void print(ayu_printformat_t printformat,
               std::ostream &os = std::cout);

private:

    /**
     * \brief
     */
    ayu_id_t m_property_owner_id;

    /**
     * \brief
     */
    std::string m_key;

    /**
     * \brief
     */
    std::string m_value;


protected:

};

#endif /* INTERN_ADD_PROPERTY_HPP_ */
