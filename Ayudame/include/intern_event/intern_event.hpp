/**
 * \file intern_event.hpp
 * \brief Header of the internal event object for event
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_EVENT_HPP_
#define INTERN_EVENT_HPP_

#include <iostream>
#include <memory>

#include "intern_event/intern_printable.hpp"
#include "ayudame_types.h"

/**
 * \defgroup intern_event Intern event representation
 *
 * tbd: details
 */

/**
 * \brief Class representing event data internally
 *
 * Base class for internal event data.
 *
 * \ingroup intern_event
 *
 */

class Intern_event_task;
class Intern_event_dependency;
class Intern_event_property;
class Intern_event_userdata;
class Intern_request;

class Intern_event : public Intern_printable {

public:

    /**
     * \brief Constructor with information passed one by one
     * \param [in] _client_id Identify the client sending the event
     * \param [in] _event The event type
     * \param [in] _timestamp Time at which the event occured
     */
    Intern_event(ayu_client_id_t _client_id,
                 ayu_event_t _event,
                 ayu_count_t _timestamp)
            : m_client_id(_client_id),
              m_event(_event),
              m_timestamp(_timestamp) {}


    /**
     * \brief Init with information from packed data
     * \param [in] message
     * \param msg_index
     * \param msg_size
     * \param label_size
     * \todo pass a protocol version?
     */
    void init(const char *message, ayu_count_t *m_msg_index , ayu_count_t *m_msg_size ,ayu_count_t *m_label_size);

    /**
     * \brief Pure virtual destructor
     */
    virtual ~Intern_event(){};

    /**
     * \brief Calculate the message size of the event
     * \return size of the message in bytes
     */
    virtual ayu_count_t get_message_size() const;

    /**
     * \brief Pack the common data into a message string
     *
     * The parameter "message" has to point to allocated memory. The common
     * event data is written to the message string in the following order:
     *
     * - message size in bytes (8 bytes)
     * - label size in bytes (8 bytes)
     * - client id (8 bytes)
     * - event type (8 bytes)
     * - time stamp  (8 bytes)
     *
     * \param [out] message string to which write the event message
     * \param msg_index
     * \param msg_size
     * \param label_size
     */
    virtual void pack(char* message, ayu_count_t *msg_index , ayu_count_t *msg_size ,ayu_count_t *label_size);

    /**
     * \TODO comment
     * @param message
     */

    virtual void pack(char* message);

    /**
     * \brief tbd
     * \param printformat
     * \param os
     */
    virtual void print(ayu_printformat_t printformat, std::ostream &os = std::cout);

    /**
     * \brief tbd
     * \return
     */
    ayu_client_id_t get_client_id() const {
        return m_client_id;
    }

    /**
     * \brief tbd
     * \return
     */
    ayu_event_t get_event() const {
        return m_event;
    }

    /**
     * \brief tbd
     * \return
     */
    ayu_count_t get_timestamp() const {
        return m_timestamp;
    }


    /**
     * \brief tbd
     * \param event
     * \param data
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_event(const ayu_event_t event, ayu_event_data_t data);

    /**
     * \brief tbd
     * \param message_buffer
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_event(const char *message_buffer);

    /**
     * \brief tbd
     * \param client_id
     * \param event
     * \param timestamp
     * \param task_id
     * \param scope_id
     * \param task_label
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_event_task(ayu_client_id_t client_id,
            ayu_event_t event,
            ayu_count_t timestamp,
            ayu_id_t task_id,
            ayu_id_t scope_id,
            ayu_label_t task_label);

    /**
     * \brief tbd
     * \param client_id
     * \param event
     * \param _timestamp
     * \param dependency_id
     * \param from_id
     * \param to_id
     * \param dependency_label
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_event_dependency(ayu_client_id_t client_id,
            ayu_event_t event,
            ayu_count_t _timestamp,
            ayu_id_t dependency_id,
            ayu_id_t from_id,
            ayu_client_id_t to_client_id,
            ayu_id_t to_id,
            ayu_label_t dependency_label);

    /**
     * \brief tbd
     * \param client_id
     * \param event
     * \param timestamp
     * \param property_owner_id
     * \param key
     * \param value
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_event_property(ayu_client_id_t client_id,
            ayu_event_t event,
            ayu_count_t timestamp,
            ayu_id_t property_owner_id,
            ayu_label_t key,
            ayu_label_t value);




    /**
     * \brief tbd
     * \param client_id
     * \param event
     * \param timestamp
     * \param request
     * \param key
     * \param value
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_request(ayu_client_id_t client_id,
            ayu_event_t event,
            ayu_count_t timestamp,
            ayu_request_t request,
            ayu_label_t key,
            ayu_label_t value);


    /**
     * \brief tbd
     * \param client_id
     * \param event
     * \param timestamp
     * \param size
     * \param data
     * \return
     */
    static std::shared_ptr <Intern_event> create_intern_event_userdata(ayu_client_id_t client_id,
            ayu_event_t event,
            ayu_count_t timestamp,
            ayu_label_t data);




    static std::shared_ptr <Intern_event_task> castToEventTask(std::shared_ptr <Intern_event> event);
    static std::shared_ptr <Intern_event_dependency> castToEventDependency(std::shared_ptr <Intern_event> event);
    static std::shared_ptr <Intern_event_property> castToEventProperty(std::shared_ptr <Intern_event> event);
    static std::shared_ptr <Intern_event_userdata> castToEventUserdata(std::shared_ptr <Intern_event> event);
    static std::shared_ptr <Intern_request> castToRequest(std::shared_ptr <Intern_event> event);
    //static std::shared_ptr <Intern_event> castRequestToEvent(std::shared_ptr <Intern_request> request);

protected:

    /**
     * \brief Deleted default constructor
     *
     * Used by derived classes only
     */
    Intern_event()
      : m_client_id(AYU_CLIENT_GENERIC),
        m_event(AYU_EVENT_NULL),
        m_timestamp(0) {}


    /// \todo make this a struct {} m_common?
    ayu_client_id_t m_client_id; ///< identify the client which sent the event
    ayu_event_t m_event; ///< event type
    ayu_count_t m_timestamp; ///< time stamp

    /*
    // these variables are for communication to ctors of derived event classes
    /// \todo avoid these temporary variables
    std::size_t m_msg_index; ///< temprary index while packing the data
    ayu_count_t m_msg_size; ///< message size, temporarily used while packing
    ayu_count_t m_label_size; ///< label size, temporarily set while packing
     */
};

#endif /* INTERN_EVENT_HPP_ */
