/**
 * \file intern_printable.hpp
 * \brief Header of the base class of internal event class
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_PRINTABLE_H
#define INTERN_PRINTABLE_H

#include <iostream>

/**
 * \brief
 */
typedef enum {
    RAW = 0,  //!< RAW
    HUMAN = 1,//!< HUMAN
    DOT = 2,//!< DOT
    XML = 3 //!< XML
} ayu_printformat_t;

static constexpr const char *xml_seperator = "#";

/**
 * \class Intern_printable
 * \brief Class Intern_printable
 *
 * \ingroup intern_event
 */
class Intern_printable {

public:

    /**
     * \brief
     */
    Intern_printable();

    /**
     * \brief
     */
    virtual ~Intern_printable();

    /**
     * \brief
     * \param other
     */
    Intern_printable(const Intern_printable&) = default;

    /**
     * \brief
     * \param other
     * \return
     */
    Intern_printable& operator=(const Intern_printable&) = default;


    /**
     * \brief
     * \param printformat
     * \param os
     */
    virtual void print(ayu_printformat_t printformat,
                       std::ostream &os = std::cout) = 0;

};

#endif // AYUDAME_INTERN_PRINTABLE_H
