/**
 * \file intern_event_dependency.hpp
 * \brief Header of the internal event object for dependencies
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_ADD_DEPENDENCY_HPP_
#define INTERN_ADD_DEPENDENCY_HPP_

#include <iostream>

#include "intern_event/intern_event.hpp"
#include "ayudame_types.h"

/**
 * \class Intern_event_dependency
 * \brief Class Intern_event_dependency
 *
 * tbd: details
 *
 * \ingroup intern_event
 */
class Intern_event_dependency: public Intern_event {

public:

    /**
     * \brief
     * no creation withou initialisation (RAII)
     */
    Intern_event_dependency() = delete;
    //


    /**
     * \brief ctor with information passed one by one
     *
     * \param _client_id
     * \param _event
     * \param _timestamp
     * \param _dependency_id
     * \param _from_id
     * \param _to_id
     * \param _to_client_id
     * \param _dependency_label
     */
    Intern_event_dependency(ayu_client_id_t _client_id,
                            ayu_event_t _event,
                            ayu_count_t _timestamp,
                            ayu_id_t _dependency_id,
                            ayu_id_t _from_id,
                            ayu_client_id_t _to_client_id,
                            ayu_id_t _to_id,
                            std::string _dependency_label);

    /**
     * \brief
     * \param data
     */
    Intern_event_dependency(ayu_event_data_t data);

    /**
     * \brief ctor from packed data
     *
     * \param message
     *

     * \todo maybe pass a protocol version eventually
     */
    Intern_event_dependency(const char *message);

    /**
     * \brief
     * \param message
     */
    void pack(char* message);

    /**
     * \brief
     * \return
     */
    ayu_count_t get_message_size() const;

    /**
     * \brief
     * \param printformat
     * \param os
     */
    void print(ayu_printformat_t printformat,
               std::ostream &os = std::cout);


    /**
     * \brief
     * \return
     */
    ayu_id_t get_dependency_id() const{
        return m_dependency_id;
    }

    /**
     * \brief
     * \return
     */
    ayu_id_t get_from_id() const{
        return m_from_id;
    }

    /**
     * \brief
     * \return
     */
    ayu_id_t get_to_id() const{
        return m_to_id;
    }

    /**
     * \brief
     * \return
     */


    ayu_client_id_t get_to_client_id() const {


        return m_to_client_id;
    }

    /**
     * \brief
     * \return
     */


    std::string get_dependency_label() const{

        return m_dependency_label;
    }

private:

    /**
     * \brief
     */
    ayu_id_t m_dependency_id;

    /**
     * \brief
     */
    ayu_id_t m_from_id;

    /**
     * \brief
     */
    ayu_client_id_t m_to_client_id;

    /**
     * \brief
     */
    ayu_id_t m_to_id;


    /**
     * \brief
     */
    std::string m_dependency_label;

protected:

};

#endif /* INTERN_ADD_DEPENDENCY_HPP_ */
