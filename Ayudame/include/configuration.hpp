/**
 * \file configuration.hpp
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include "config4cpp/Configuration.h"

#include <map>
#include <sstream>
#include <string>
#include <stdexcept>

namespace c4c = config4cpp;

class Configuration
{
public:
    // ctors and dtor
    Configuration();
    Configuration(const Configuration& _conf) = delete;
    virtual ~Configuration();

    // assignment operator
    const Configuration& operator=(const Configuration& _conf) = delete;

    // initialisation
    void init(const std::string filename = "");

    // getters and setters
    std::string get(const std::string key) const;
    std::string get_string(const std::string key) const;
    unsigned get_unsigned(const std::string key) const;
    bool get_bool(const std::string key) const;
    void set(const std::string key, const char* value);
    void set(const std::string key, const std::string value);
    void set(const std::string key, const unsigned value);
    void set(const std::string key, const bool value);
    void set_from_file(const std::string filename);
    void set_default();

private:

    std::string m_filename;

    std::map <std::string, std::string> m_data = {
                                                   { "connect.stdout_human", "true" },
                                                   { "connect.stdout_raw", "false" },
                                                   { "connect.temanejo1", "false" },
                                                   { "connect.temanejo", "false" },
                                                   { "connect.dot", "false" },
                                                   { "connect.dot_filename", "ayudame.dot" },
                                                   { "connect.xml", "false" },
                                                   { "connect.xml_filename", "ayudame.xml" },
                                                   { "connect.ayu_port", "5977" },
                                                   { "connect.ayu_host", "localhost" },
                                                   { "logging.error", "true" },
                                                   { "logging.warning", "true" },
                                                   { "logging.info", "true" },
                                                   { "logging.verbosity_level", "0" },
                                                   { "logging.debug", "false" }
    };

    const std::map <std::string, std::string> m_default_data = m_data;
};

#endif // CONFIGURATION_HPP
