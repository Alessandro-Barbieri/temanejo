/**
 * \file ayudame_socket.hpp
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef AYUDAME_AYU_SOCKET_HPP
#define AYUDAME_AYU_SOCKET_HPP

#include "ayudame_types.h"

#include <algorithm>
#include <thread>
#include <atomic>
#include <mutex>
#include <vector>
#include <map>

#include "socket.hpp"
#include "ayu_helper.hpp"
#include "intern_event/intern_event.hpp"

/**
 * \brief Class representing an ayu_socket connection
 * This class is derived from Socket which handles the
 * "real" socket communication
 * \class Ayu_Socket
 * \ingroup socket
 */


class Ayu_Socket : public Socket {

public:

    /**
     * Consturctor
     */
    Ayu_Socket();

    /**
     * Destructor
     */
    ~Ayu_Socket();



    //TODO
    void set_Temanejo(){
        m_temanjo =true;
    }

    /**
     * Sends an Event out
     * @param data an Inter_event
     * @return -1 if it fails
     */
    int socket_write( std::shared_ptr<Intern_event> data);

    /**
     * Performs an read on the buffer
     * @return an Intern_event or nullptr if the buffer is empty
     */
    const std::shared_ptr<Intern_event> socket_read();

    /**
     * Checks if the socket is shutdownable
     * @return true if you can shutdown the socket
     */
    bool is_shutdownable();



    /**
     * This function will shutdown the socket in a safe way.
     * After the buffer is empty it will call the derived shutdown function
     */
    void shutdown_socket();

    /**
     * Sets an vector of all hostnames
     * @param host_uniq vector of all hostnames
     */
    void set_host_uniq(std::vector<std::string> host_uniq);

    /**
     * Sets my hostname
     * @param hostname my hostname
     */
    void set_master_id(uint64_t master_id){
        m_master_id = master_id;
    }

    /**
     * Sets my hostname
     * @param hostname my hostname
     */
    void set_my_hostname(std::string hostname){
        m_my_hostname = hostname;
    }

private:

    bool add_event_to_read_buffer(const char * message_buffer, int infd, struct bufferevent* bev);

    int get_read_buffer_size();

    void register_fd_for_hostname(std::string hostname, int fd, struct bufferevent* bev);
    void delete_fd(int fd);

    int get_fd_for_hostname(uint64_t index);
    struct bufferevent* get_bev_for_hostname(uint64_t k_index_to);
    int get_fd_for_proc(uint64_t proc);
    struct bufferevent* get_bev_for_proc(uint64_t procc);


    uint64_t calculate_host(uint64_t destination);



    std::map<std::string, std::pair<int, struct bufferevent*>> m_hostename_to_fd;
    std::map<ayu_client_id_t, std::pair<int, struct bufferevent*>> m_proc_to_fd;
    std::vector<std::string> m_host_uniq;
    uint64_t m_master_id;
    std::string m_my_hostname;


    bool m_temanjo;

};

#endif // AYUDAME_SOCKET_HPP
