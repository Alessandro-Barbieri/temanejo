/**
 * \file ayudame_types.hpp
 * \brief Header file for the event and request types of the AYUDAME package
 *
 * This header file declares the types used in the Ayudame library
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \author José Gracia <gracia@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 ******/

#ifndef AYUDAME_TYPES_H
#define AYUDAME_TYPES_H

#define __STDC_LIMIT_MACROS
#include <stdint.h>



#define MAX_CORES_PER_NODE 48
#define MAX_CHANNELS_PER_NODE 64
#define MAX_REMOTE_CHANNELS_PER_NODE (MAX_CHANNELS_PER_NODE - MAX_CORES_PER_NODE)

/**
 * \brief enum of events
 *
 * ayu_event_t specifies which type of event happened.
 *
 */
typedef enum
{
    AYU_EVENT_NULL = 0, ///< for initialisation
    AYU_INIT = 1, ///< when ayudame is initialised
    AYU_FINISH = 2, ///< when ayudame is shut down
    AYU_START = 3, ///< when parallel section starts // is this a property of a scope=task?
    AYU_END = 4, ///< when parallel section ends // is this a property of a scope=task?
    AYU_ADDTASK = 5, ///< when task is added; a task has two attributes: id, scope
    AYU_ADDDEPENDENCY = 6, ///< when dependency is added; a dep. has three attributes: id, from, to
    AYU_REGISTERPROPERTY = 7, ///< to register a property; can be of task, dep., client, global, ...; e.g. "function_name" for tasks
    AYU_REGISTERPROPERTYVALUE = 8, // register a possible value for a property; e.g. "foo_task" for prop. "function_name"
    AYU_SETPROPERTY = 9, ///< sets a property for a certain object, i.e. task, dep. etc.
    AYU_BARRIER = 10, ///< when barrier is reached
    AYU_ATTACH = 11, ///< to attach to client, i.e. start taking events
    AYU_DETACH = 12, ///< to dettach from client, i.e. stop taking events
    AYU_USERDATA = 13, ///< to send arbitrary data
    AYU_REQUEST = 14
} ayu_event_t;


/**
 * \brief Array of event names for string output etc.
 *
 * \see ayu_event_t
 *
 */
static const char * const ayu_event_str[] = { "AYU_EVENT_NULL",
                                              "AYU_INIT",
                                              "AYU_FINISH",
                                              "AYU_START",
                                              "AYU_END",
                                              "AYU_ADDTASK",
                                              "AYU_ADDDEPENDENCY",
                                              "AYU_REGISTERPROPERTY",
                                              "AYU_REGISTERPROPERTYVALUE",
                                              "AYU_SETPROPERTY",
                                              "AYU_BARRIER",
                                              "AYU_ATTACH",
                                              "AYU_DETACH",
                                              "AYU_USERDATA",
                                              "AYU_REQUEST"
                                            };

/**
 * \brief enum of requests
 *
 * tbd: details
 */
typedef enum
{
    AYU_REQUEST_NULL = 0, ///< for initialisation
    AYU_NOREQUEST = 1, ///< explicit "no request"
    AYU_CONTINUE = 2,
    AYU_BREAK = 3, ///< block request \see AYU_PAUSEONEVENT
    AYU_STEP = 4, ///< run until next pause condition is reached
    AYU_REQUEST_ATTACH = 5, ///< tbd
    AYU_REQUEST_DETACH = 6, ///< tbd
    AYU_REQUEST_SETPROPERTY = 7, ///< tbd
    AYU_BLOCK_TASK = 8,
    AYU_UNBLOCK_TASK =9,
    AYU_BLOCK_THREAD = 8,
    AYU_UNBLOCK_THREAD =9
} ayu_request_t;

/**
 * \brief Array of request names for string output etc.
 *
 * \see ayu_request_t
 *
 */
static const char * const ayu_request_str[] = { "AYU_REQUEST_NULL",
                                                "AYU_NOREQUEST",
                                                "AYU_CONTINUE",
                                                "AYU_BREAK",
                                                "AYU_STEP",
                                                "AYU_REQUEST_ATTACH",
                                                "AYU_REQUEST_DETACH",
                                                "AYU_REQUEST_SETPROPERTY",
                                                "AYU_BLOCK_TASK",
                                                "AYU_UNBLOCK_TASK",
                                                "AYU_BLOCK_THREAD",
                                                "AYU_UNBLOCK_THREAD"
                                               };

/**
 * \brief Client specifiers
 *
 * \todo links in documentation
 */

typedef enum
{
    AYU_CLIENT_GENERIC = 0, ///< no or unknown runtime, used for initialisation
    AYU_CLIENT_CPPSS = 1, ///<  CPPSS runtime
    AYU_CLIENT_SMPSS = 2, ///< SmpSs runtime
    AYU_CLIENT_OMPSS = 3, ///< OmpSs runtime
    AYU_CLIENT_STARPU = 4, ///< StarPU runtime
    AYU_CLIENT_FASTFLOW = 5, ///< FastFlow runtime
    AYU_CLIENT_MPI = 6, ///< MPI runtime
    AYU_CLIENT_GOMP = 7, ///< GNU OpenMP runtime
    AYU_CLIENT_CILK = 8, ///< CILK runtime
    AYU_CLIENT_DUMMY = 9, ///< for test purposes
    AYU_CLIENT_RESERVED = 99 ///< reserved for future use
} ayu_client_t;

/**
 * \brief Array of client names for string output etc.
 *
 * \see ayu_client_t
 *
 */
static const char * const ayu_client_str[] = { "AYU_CLIENT_GENERIC",
                                                "AYU_CLIENT_CPPSS",
                                                "AYU_CLIENT_SMPSS",
                                                "AYU_CLIENT_OMPSS",
                                                "AYU_CLIENT_STARPU",
                                                "AYU_CLIENT_FASTFLOW",
                                                "AYU_CLIENT_MPI",
                                                "AYU_CLIENT_GOMP",
                                                "AYU_CLIENT_CILK",
                                                "AYU_CLIENT_DUMMY",
                                                "AYU_CLIENT_RESERVED"
                                               };

typedef enum
{
    TCA_0 = 0, ///< no or unknown runtime, used for initialisation
    TCA_1 = 1, ///<  CPPSS runtime
    TCA_2 = 2, ///< SmpSs runtime
    TCA_REGISTER_GET_WD_ID = 3, ///< get_wd_id from the ompss runtime
} tca_callback_t;


/**
 */
static const char * const tca_callback_str[] = { "TCA_0",
                                                "TCA_1",
                                                "TCA_2",
                                                "TCA_REGISTER_GET_WD_ID"
                                               };





/**
 * \brief  connector types
 *
 * tbd: details
 */
typedef enum
{
    AYU_CONNECT_GENERIC = 0, ///< for initialisation
    AYU_CONNECT_STDOUT_RAW = 1, ///< print raw numbers to stdout
    AYU_CONNECT_STDOUT_HUMAN = 2, ///< print human readable output to stdout
    AYU_CONNECT_TEST = 3, ///< ???
    AYU_CONNECT_TEMANEJO1 = 4, ///< connect to Temanejo version 1.x
    AYU_CONNECT_TEMANEJO = 5, ///< connect to Temanejo version 2.x
    AYU_CONNECT_DOT = 6, ///
    AYU_CONNECT_SOCKET = 7, ///< pass messages to socket
    AYU_CONNECT_XML = 8, ///< write xml file
    AYU_CONNECT_RESERVED = 99 ///< reserved for future use

} ayu_connect_t;

/**
 * \brief basic types
 */
typedef uint64_t ayu_client_id_t;
typedef uint64_t ayu_id_t;
typedef uint64_t ayu_count_t;
typedef int64_t ayu_int_t;
typedef double ayu_float_t;
typedef const char *ayu_label_t;

// ayu_id_t, ayu_count_t, ayu_int_t and ayu_float_t are required to be
// of this size:

/**
 * \brief size of value type
 */
static const ayu_count_t ayu_sizeof_value = 8; // sizeof(unit64_t)
/*

// initialisation values
const ayu_client_id_t ayu_client_id_init = 0;
const ayu_id_t ayu_id_init = 0;
const ayu_count_t ayu_count_init = 0;
const ayu_int_t ayu_int_init = 0;
const ayu_float_t ayu_float_init = 0.0;
const ayu_label_t ayu_label_init = "";
*/

/**
 * \brief  error types
 */
typedef enum
{
    AYU_SUCCESS = 0, AYU_ERROR = 1
} ayu_error_t;

/**
 * \brief types of objects that can have properties
 */
typedef enum
{
    AYU_TASK = 1, AYU_DEPENDENCY = 2, AYU_CLIENT = 3, AYU_GLOBAL = 4
} ayu_property_holder_t;


/**
 * \defgroup Event_data_types Event data types for different events
 *
 * tbd: details
 *
 * @{
 */

/**
 * \brief Common data for events
 */
typedef struct
{
    // client sending the event
    ayu_client_id_t client_id;
    // event type. For internal use. The programmer should not set this value.
    ayu_event_t event;
    // timestamp. set by Ayudame when event is issued
    // TODO: set timestamp on creation of this object? would have to move
    //       get_timestamp to namespace ayu
    //       replace class Ayudame with namespace?
    ayu_count_t timestamp;
    // number of values in data type where ayu_data_common_t is contained
    // TODO: this should be const
    //ayu_count_t n_values;
} ayu_data_common_t;

/**
 * \brief Data type for initialisation etc.
 */
typedef struct
{
    ayu_data_common_t common;
} ayu_data_null_t;

/**
 * \brief Data for event AYU_INIT
 */
typedef struct
{
    ayu_data_common_t common;
} ayu_data_init_t;

/**
 * \brief Data for event AYU_FINISH
 */
typedef struct
{
    ayu_data_common_t common;
    // ...
} ayu_data_finish_t;

/**
 * \brief Data for event AYU_START
 */
typedef struct
{
    ayu_data_common_t common;
    // ...
} ayu_data_start_t;

/**
 * \brief Data for event AYU_END
 */
typedef struct
{
    ayu_data_common_t common;
    // ...
} ayu_data_end_t;

/**
 * \brief Data for event AYU_ADDTASK
 */
typedef struct
{
    ayu_data_common_t common;
    ayu_id_t task_id;
    ayu_id_t scope_id;
    ayu_label_t task_label;
} ayu_data_add_task_t;

/**
 * \brief Data for event AYU_ADDDEPENDENCY
 */
typedef struct
{
    ayu_data_common_t common;
    ayu_id_t dependency_id;
    ayu_id_t from_id;
    ayu_id_t to_id;
    ayu_label_t dependency_label;
} ayu_data_add_dependency_t;

/**
 * \brief Data for event AYU_SETPROPERTY
 */
typedef struct
{
    ayu_data_common_t common;
    ayu_id_t property_owner_id;
    ayu_label_t key;
    ayu_label_t value;
} ayu_data_set_property_t;

/**
 * \brief Data for event AYU_BARRIER
 */
typedef struct
{		// barrier is as addtask. Barrier might even have deps, eg SMPSs waiton
    ayu_data_common_t common;
    ayu_id_t barrier_id;
    ayu_id_t scope_id;
    ayu_label_t barrier_label;
} ayu_data_barrier_t;

/**
 * \brief Data for event AYU_ATTACH
 */
typedef struct
{
    ayu_data_common_t common;
    // ...
} ayu_data_attach_t;

/**
 * \brief Data for event AYU_DETACH
 */
typedef struct
{
    ayu_data_common_t common;
    // ...
} ayu_data_detach_t;

/**
 * \brief Data for event AYU_USERDATA
 */
typedef struct
{
    ayu_data_common_t common;
    ayu_label_t data;
} ayu_data_userdata_t;

/**
 * \brief General event data type
 */
typedef union
{
//  uint8_t raw[ayu_data_size]; // we don't need this
    ayu_data_common_t common;
    ayu_data_null_t event_null;
    ayu_data_init_t init;
    ayu_data_finish_t finish;
    ayu_data_start_t start;
    ayu_data_end_t end;
    ayu_data_add_task_t add_task;
    ayu_data_add_dependency_t add_dependency;
    ayu_data_set_property_t set_property;
    ayu_data_barrier_t barrier;
    ayu_data_attach_t attach;
    ayu_data_detach_t detach;
    ayu_data_userdata_t userdata;
} ayu_event_data_t;

/** @} */

/**
 * \brief request data types for different events
 */

/**
 * \brief request data type
 *
 * \todo fill ayu_request_data_t
 */
typedef union
{
//  uint8_t raw[ayu_data_size]; // we don't need this
    /*TODO: fill in request data types for different events */
} ayu_request_data_t;

#endif // AYUDAME_TYPES_H
