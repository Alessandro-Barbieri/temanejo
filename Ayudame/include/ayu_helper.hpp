/**
 * \file ayudame_helper.hpp
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CLIENT_EVENT_HELPER_HPP
#define CLIENT_EVENT_HELPER_HPP

#define __STDC_LIMIT_MACROS
#include <cstdint>
#include <iostream>

/**
 * Sets the first three bytes of m_id
 * @param m_id
 * @param master_id
 */
static void set_master_id(uint64_t *m_id, uint64_t master_id) {
    *m_id |= master_id << 0;
}

/**
 *  Sets the 4. to the 6. Byete
 * @param m_id
 * @param proc_id
 */

static void set_proc_id(uint64_t *m_id, uint64_t proc_id) {
    *m_id |= proc_id << 24;
}

/**
 *
 * @param m_id
 * @param client_id
 */
static void set_client_id(uint64_t *m_id, uint64_t client_id) {
    *m_id |= client_id << 48;
}


/**
 * 0000000000000000000000000000000000000000111111111111111111111111
 * @param m_id
 * @return
 */
static uint64_t get_master_id(uint64_t m_id) {
    //return (*m_id & 0b111111111111111111111111) >> 0;
    return (m_id & 0xFFFFFF) >> 0;

}

/**
 * 0000000000000000111111111111111111111111000000000000000000000000
 * @param m_id
 * @return
 */
static uint64_t get_proc_id(uint64_t m_id) {
    //return (*m_id & 0b111111111111111111111111000000000000000000000000) >> 24;
    return (m_id & 0xFFFFFF000000) >> 24;
}

/**
 * 1111111111111111000000000000000000000000000000000000000000000000
 * @param m_id
 * @return
 */
static uint64_t get_client_id(uint64_t m_id) {
    //return (*m_id & 0b1111111111111111000000000000000000000000000000000000000000000000) >> 48;
    return (m_id & 0xFFFF000000000000) >> 48;
}

/**
 *
 * @param m_id
 */
static void binary_print(uint64_t *m_id) {
    printf("huhu\n");
    std::cout << *m_id;
    printf("\n");
}

#endif // EVENT_HELPER_HPP
