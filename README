See the manual ./doc/Temanejo_manual.pdf for detailed documentation. 

Temanejo -- a debugger for task-parallel programming
-----------------------------------------------------

Temanejo is a graphical debugger for task-based programming models as SMPSs and
OmpSs, but also OpenMP tasks and StarPU. Temanejo is developed at the 
High Performance Computing Center (HLRS), University of Stuttgart, Germany.

The tool consists of two components: 1) the graphical fronted "Temanejo GUI" 
(but mostly simply Temanejo), and 2) the supporting library "Ayudame" which is
sandwiched between Temanejo and the parallel tasking runtime system.

More information and the latest release of Temanejo can be found at
http://www.hlrs.de/temanejo/. The developers may be contacted at the email
temanejo@hlrs.de. Announcements and user discussion are done through the mailing
list temanejo-users@hlrs.de (see the webpage on subscription instructions).


 Short Install Instructions
----------------------------

See the user manual at ./doc/Temanejo_manual.pdf for full installation
and usage instructions. See the file INSTALL in this directory for generic
autotools installation instructions.

Temanejo is a Python application and requires a Python v2.7 installation.
More recent versions from the v3 series have not been tested yet.

In addition Temanejo depends on the following Python modules:
- gtk
- cairo
- networkx (http://networkx.lanl.gov/)

Ayudame requires a C++ compiler. It is tested with GCC v4.7.2.

We assume that you will install Temanejo into the directory $TEMANEJO_HOME.
In the simplest case installation proceeds as:
   1. ./configure --prefix=$TEMANEJO_HOME
   2. make && make install

In case the configure complains about a missing python installation, make sure
it is available in the search path. On many HPC systems you might need to load
a python module
   0a. module load python

In case the configure scripts returns other errors, try to regenerate the
configure script with:
   0b. autoreconf -fiv

After installation you might want to add $TEMANEJO_HOME/bin the search path
$PATH, and $TEMANEJO_HOME/lib to the library search path $LD_LIBRARY_PATH.
On bash, e.g.:
   3. export PATH=$TEMENEJO_HOME/bin:$PATH
      export LD_LIBRARY_PATH=$TEMENEJO_HOME/lib:$LD_LIBRARY_QPATH

If necessary you might disable the installation of the Temanejo GUI or the
Ayudame library, respectivey, with the configure options:
  --disable-temanejo
  --disable-ayudame
In particular, building the Ayudame library on Mac OS is not suported yet
and you will have to disable it. 

You can also ask the build system to install a modulefile from a user-provided
template with
  --enable-modulefile[=template_file]

If no template file is provided, it will use modulefile/example_modulefile.in.

See the output of ./configure --help for further ways to control the building
and installation process.

See 