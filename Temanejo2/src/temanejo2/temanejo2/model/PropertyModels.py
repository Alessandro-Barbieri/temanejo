'''!
@brief This module holds the models for the nodeproperties.
       
The Node Properties are created and updated dynamicly from the remote application so 
the models will be created at startup and be filled out of the remote controller where all
the datanodes are stored in. There are 2 different modeltypes which will be created:
* The PropertySelectionModel \\
This model is responsible for displaying all the wished properties in the dropdowns
* The PropertyDisplayModel \\
This model is responsible for displaying the values of the selected property in a tableview

@TODO Make nice Description

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import logging

from PySide import QtCore

import temanejo2.temanejo2.lib.easymodel.treemodel as easyModel

logger = logging.getLogger(__name__)



##
#    This class defines a listModel which is responsible for holding the property types 
#    and will be set to the dropdown menus of the propertyselection part
class PropertySelectionModel(QtCore.QAbstractListModel):

    ##
    #
    # @param properties The list of properties a node has
    # @param parent The parent of the model 
    def __init__(self,properties=None,parent=None):
        QtCore.QAbstractListModel.__init__(self,parent)
        self.__properties = [] if properties is None else properties
        self.__propertyIndexMapper = dict()
        
        if properties is not None:
            for prop in properties:
                self.insertRows(self.rowCount(), 1, prop)
    
    ##
    #  Returns the number of items in the Model.\n
    #  Needs to be implemented so the View will display the correct number of items 
    # @param parent Only needed for hierarchical datastructures so for this ListModel 
    #        you can ignore this parameter completely
    def rowCount(self, parent=None):
        return len(self.__properties)
    
    ##
    #  Returns the data stored in the model at the index position.
    #  For this list model we only need to query index.row()
    # @param index The index of the data which should be returned from the model
    # @param role  Given from the view in order to get the correct item displayed...
    #              Is the role of the caller like DisplayRole or TooltipRole etc.
    def data(self, index, role):
        if role == QtCore.Qt.EditRole:
            self.__properties[index.row()].name()
        
        # Check if the data is queried from any view
        if role == QtCore.Qt.DisplayRole:
            return self.__properties[index.row()].name()
    ##
    #
    def flags(self,index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable 
    
    ##
    #
    def insertRows(self, position, rows, parent = QtCore.QModelIndex(), value = None):
        self.beginInsertRows(parent, position, position + rows - 1)
        for row in range(rows):
            self.__properties.insert(position,value)
            self.__propertyIndexMapper[value.name()] = position
        self.endInsertRows()
    ##
    #
    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        self.beginRemoveRows(parent, position, position + rows - 1)
        for row in range(rows):
            self.__properties.pop(position)
        self.endRemoveRows()
        return True
    
    ##
    #
    def clearModel(self):
        self.removeRows(0,self.rowCount())
        self.reset()
        
    ##
    #
    def getPropertyKeys(self):
        propKeys = []
        for i in self.__properties:
            propKeys.append(i.name())
        return propKeys

    ##
    #
    def getPropKeyIndex(self, propKey):
        index = None
        try:
            index = self.__propertyIndexMapper[propKey]
        except:
            logger.error("Property %s is not in model"%propKey)
        
        return index

##
# This is just an small extension to the Treemodel in order to have a Key Value Store for 
# more convenient querying
class DisplayViewModel(easyModel.TreeModel):
    def __init__(self, temanejo, propDisplayModelRootItem=None):
        easyModel.TreeModel.__init__(self,propDisplayModelRootItem);
        ##@member The kvStore is a nested dict which holds all values of the according Property
        self.kvStore = dict()
        # Extension for Temanejo: A list of the propertyRootItems which are sorted in incoming order 
        self.propertyRootItems = []
        self.temanejo = temanejo
        
    ##
    #   Clears the display model
    def clearModel(self):
        self._root.childItems = []
        self.reset()
        self.propertyRootItems = []
        self.temanejo.dataParser.resetToInitialState()
        
    ##
    #   Adds the propvalues to the kvStore
    def addToKVStore(self, propRoot, value=None, itemId=None):
        if value is None:
            self.kvStore[propRoot]=dict()
        else:
            itemId_str = str(itemId)
            for a_value, an_itemset in self.kvStore[propRoot].items():
                try:
                    an_itemset.remove(itemId_str) # may throw a ValueError
                except ValueError:
                    pass # ignore, was not in the list
            if not value in self.kvStore[propRoot].keys(): 
                self.kvStore[propRoot][value] = [itemId_str]
            else:
                self.kvStore[propRoot][value].append(itemId_str)
    
    ##
    #   Checks if a value depending on the property already exists      
    def propValueExists(self, propRoot, value):
        return value in self.kvStore[propRoot].keys()    
