'''!
@brief A module for random color generation


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import colorsys
import random

from PySide import QtGui
from PySide.QtCore import QThread as Thread


def palette_rgb_inf(i, s=1.0):
    h = i/2.7345
    v = 1.
    h -= int(h)
    rgb = colorsys.hsv_to_rgb(h, s, v)
    return tuple(rgb)

##
#   Generates a random list of colors with a given length
def generateRandomColors(numOfColors):
    colorList = []
    r = lambda: random.randint(0,255)
    for i in range(1,numOfColors):
        colorList.append(QtGui.QColor(r(),r(),r()))
    
    return colorList

##
#
class ColorGenerator(Thread):
    ##
    #
    def __init__(self,numOfColors):
        super(ColorGenerator,self).__init__()
        self.__colors = []
        self.__numOfColors = numOfColors
    ##
    #
    def run(self):
        self.__colors = generateRandomColors(self.__numOfColors)
    
    ##
    #
    def generateColors(self):
        self.start()
        
        self.terminate()
        return self.__colors
