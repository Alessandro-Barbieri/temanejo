'''!
@brief Primitive shapes for the nodes etc. will be defined in here

This module provides some basic forms for the nodes in order to paint them onto the TemanejoGraph
All basic forms should be defined in here, so modifications of the appearance of the node
can be done independently from modifiing the node itself.
@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import math

from PySide import QtGui, QtCore

from temanejo2.temanejo2.lib import configuredValues


##
#   This is a convenience Funktion in order to provide a simple way to draw a arc from one to another position
def drawCurvedLine(painter, startLoc_x, startLoc_y, endLoc_x, endLoc_y, numberShift):

    path = QtGui.QPainterPath()
    path.moveTo(startLoc_x, startLoc_y)
    clineCenterX = (startLoc_x + endLoc_x)/2
    clineCenterY = (startLoc_y + endLoc_y)/2

    ctrlPointX = clineCenterX - numberShift * (endLoc_y - startLoc_y) / 30.0
    ctrlPointY = clineCenterY + numberShift * (endLoc_x - startLoc_x) / (0.5*(endLoc_y - startLoc_y)+0.0001)
    path.quadTo(ctrlPointX, ctrlPointY, endLoc_x, endLoc_y)

    painter.drawPath(path)


##
#   Draws a Triangle via the given Painter.\n
#   The Triangle will be equilateral.
#   @param painter: The Painter helds the widget and paints on it by the given operations
#   @param color: The color in which the triangle should be painted
#   @param leftUpPos: The upper left Corner of the triangle
#   @param width: This is the length of all lateral of the triangle
def drawTriangle(painter, color, leftUpPos, width):
    drawShape('varpentagra',painter, color, leftUpPos, width)
    """
    #Create the path for the triangle
    path = QtGui.QPainterPath()
    path.moveTo(leftUpPos[0],leftUpPos[1])
    path.lineTo(leftUpPos[0] + width,leftUpPos[1])
    path.lineTo(leftUpPos[0] + width / 2, leftUpPos[1] + configuredValues.SQRT_THREE / 2 * width)
    path.lineTo(leftUpPos[0],leftUpPos[1])

    painter.fillPath(path, QtGui.QBrush(color))
    """
##
#   Draws a Circle via the given Painter.\n
#   @param painter: The Painter helds the widget and paints on it by the given operations
#   @param color: The color in which the circle should be painted
#   @param leftUpPos: The upper left Corner of the bounding box of the circle
#   @param width: This is the diameter of the circle
def drawCircle(painter,color,leftUpPos, width):

    path = QtGui.QPainterPath()
    painterPath = __get_points_of_shape('circle',leftUpPos[0], leftUpPos[1], width)
    path.addEllipse(painterPath[0][0], painterPath[0][1], painterPath[1], painterPath[1])
    painter.fillPath(path, QtGui.QBrush(color))

##
#  TODO Docstring
def drawShape(shape, painter, color, leftUpPos, width):
    if shape == 'circle':
        drawCircle(painter, color, leftUpPos, width)
    else:
        painterPath = __get_points_of_shape(shape, leftUpPos[0] + width / 2, leftUpPos[1] + width / 2, width)
        path = QtGui.QPainterPath()
        path.moveTo(painterPath[0][0], painterPath[0][1])
        for point in painterPath[1:]:
            path.lineTo(point[0], point[1])
        painter.fillPath(path, QtGui.QBrush(color))

##
#  TODO Docstring
def drawMargin(shape, painter, color, leftUpPos, width):
    if shape == 'circle':
        drawCircleMargin(painter, color, leftUpPos, width)
    else:
        painterPath = __get_points_of_shape(shape, leftUpPos[0] + width / 2, leftUpPos[1] + width / 2, width)
        path = QtGui.QPainterPath()
        path.moveTo(painterPath[0][0], painterPath[0][1])
        for point in painterPath[1:]:
            path.lineTo(point[0], point[1])
        path.lineTo(painterPath[0][0], painterPath[0][1])

        painter.setPen(QtGui.QPen(color, configuredValues.DEFAULT_MARGINSIZE, QtCore.Qt.SolidLine,
                    QtCore.Qt.FlatCap, QtCore.Qt.MiterJoin))
        painter.drawPath(path)

    painter.fillRect(leftUpPos[0], leftUpPos[1]-width/5,  width, width/5,color)
    painter.fillRect(leftUpPos[0], leftUpPos[1]+width,  width, width/5,color)


def drawCircleMargin(painter,color,leftUpPos, width):

    path = QtGui.QPainterPath()
    painterPath = __get_points_of_shape('circle', leftUpPos[0], leftUpPos[1], width)
    path.addEllipse(painterPath[0][0], painterPath[0][1], painterPath[1], painterPath[1])
    painter.setPen(QtGui.QPen(color, configuredValues.DEFAULT_MARGINSIZE, QtCore.Qt.SolidLine,
                    QtCore.Qt.FlatCap, QtCore.Qt.MiterJoin))
    painter.drawPath(path)

##
#  TODO Docstring
def __get_points_of_shape(shape, x0, y0, r):

    if shape == 'circle':
        p = [(x0, y0), r]  # default is circle

    r *= 0.75
    if shape == 'arrow':
        p = [(x0+.4*r, y0+.9*r),
             (x0-.4*r, y0+.9*r),
             (x0-.4*r, y0-.2*r),
             (x0-.8*r, y0-.2*r),
             (x0, y0-r),
             (x0+.8*r, y0-.2*r),
             (x0+.4*r, y0-.2*r),
             (x0+.4*r, y0+.9*r)]

    elif shape == 'box':
        p = [(x0-r/1.414, y0+r/1.414),
             (x0-r/1.414, y0-r/1.414),
             (x0+r/1.414, y0-r/1.414),
             (x0+r/1.414, y0+r/1.414)]
    elif shape == 'trapezium':
        p = [(x0-1.3*r/1.414, y0+r/1.414),
             (x0-0.7*r/1.414, y0-r/1.414),
             (x0+0.7*r/1.414, y0-r/1.414),
             (x0+1.3*r/1.414, y0+r/1.414)]
    elif shape == 'invtrapezium':
        p = [(x0-0.7*r/1.414, y0+r/1.414),
             (x0-1.3*r/1.414, y0-r/1.414),
             (x0+1.3*r/1.414, y0-r/1.414),
             (x0+0.7*r/1.414, y0+r/1.414)]
    elif shape == 'diamon':
        p = [(x0, y0+r),
             (x0-r, y0),
             (x0, y0-r),
             (x0+r, y0)]
    elif shape == 'triangle':
        y0 -= .2
        r *= 1.24
        p = [(x0, y0+r),
             (x0-r*.866, y0-r*.5),
             (x0+r*.866, y0-r*.5)]
    elif shape == 'invtriangle':
        y0 += .2
        r *= 1.24
        p = [(x0, y0-r),
             (x0-r*.866, y0+r*.5),
             (x0+r*.866, y0+r*.5)]
    elif shape == 'pentago':
        p = [(x0+.7*r, y0+.8*r),
             (x0-.7*r, y0+.8*r),
             (x0-r, y0-.2*r),
             (x0, y0-r),
             (x0+r, y0-.2*r)]
    elif shape == 'invpentago':
        p = [(x0+.7*r, y0-.8*r),
             (x0-.7*r, y0-.8*r),
             (x0-r, y0+.2*r),
             (x0, y0+r),
             (x0+r, y0+.2*r)]
    elif shape == 'pentagra':
        p = [(x0+.7*r, y0+.8*r),
             (x0-r, y0-.2*r),
             (x0+r, y0-.2*r),
             (x0-.7*r, y0+.8*r),
             (x0, y0-r)]
    elif shape == 'varpentagra':
        p = [(x0+.7*r, y0+.8*r),
             (x0, y0+.4*r),
             (x0-.7*r, y0+.8*r),
             (x0-.45*r, y0+.2*r),
             (x0-r, y0-.25*r),
             (x0-.3*r, y0-.25*r),
             (x0, y0-r),
             (x0+.3*r, y0-.25*r),
             (x0+r, y0-.25*r),
             (x0+.45*r, y0+.2*r)]
    elif shape == 'hexago':
        p = [(x0+.5*r, y0+.8*r),
             (x0-.5*r, y0+.8*r),
             (x0-r, y0),
             (x0-.5*r, y0-.8*r),
             (x0+.5*r, y0-.8*r),
             (x0+r, y0)]
    elif shape == 'house':
        p = [(x0+.8*r, y0+.8*r),
             (x0-.8*r, y0+.8*r),
             (x0-.8*r, y0-.2*r),
             (x0, y0-r),
             (x0+.8*r, y0-.2*r)]
    elif shape == 'invhouse':
        p = [(x0+.8*r, y0-.8*r),
             (x0-.8*r, y0-.8*r),
             (x0-.8*r, y0+.2*r),
             (x0, y0+r),
             (x0+.8*r, y0+.2*r)]
    return p

##
#   Draws a pause sign.
#   This shall be painted as an overlay onto the node, if the user want to block it
#   @param painter: The Painter helds the widget and paints on it by the given operations
def drawPause(painter, color, leftUpPos,width):
    painter.setBrush(color)
    #Draws left side of the pause sign
    painter.drawRect(leftUpPos[0] + width * 2 / 10,leftUpPos[1], width / 10,width)
    #Draws right side of the pause sign
    painter.drawRect(leftUpPos[0] + width * 6 / 10,leftUpPos[1], width / 10,width)
