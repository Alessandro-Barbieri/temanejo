'''!
@brief This module provides some converter Functions for mathmatical stuff

In here all complicated mathmatical computations should be written. Stuff like getting the 
average of locations etc.


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

##
#    Computes the average of a list of 2D Locations.
#    @param list_in: This is a list of locations for which the average should be computed.
#                    It has to of the form [[a,b]...[x,y]]\n
#                    Example: the value [2,2] will be returned for [[1,1],[2,2],[3,3]]
#    @return The average (nonweighted) value of the of the 2D locations 
def computeAverageOfLocations(list_in):

    result =[0,0]
    # Sum locations
    for i in list_in:
        result[0] += i[0]
        result[1] += i[1]
    #Divide through length
    result[0] /= len(list_in)
    result[1] /= len(list_in)
    
    return result
