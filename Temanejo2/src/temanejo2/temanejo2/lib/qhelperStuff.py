'''!
@brief A library for small helperfunctionality based on qtlibs

All helper for Qt Items which could be used in different modules are defined in here  

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

from PySide import QtCore


##
#   This helperMethod computes the currently visible Rectangle of any QGraphicsView.\n
#   It only works correctly if the view isn't rotated. But since there is no need in the
#   Application this doesn't matter.
#   @param view The view which will be analised. Type must be a QGraphicsView
#   @return Returns a QRectF with the lower left point and the width and height of the view
def getVisibleRect(view):
    # get lower left point of the view
    a = view.mapToScene( QtCore.QPoint(0,0) )
    # get the width and height of the view
    b = view.mapToScene( QtCore.QPoint(view.viewport().width(), view.viewport().height()))
    # return a QRectF with the dimensions of the view
    return QtCore.QRectF( a, b )


def compare_2_tuple_by_value(t1, t2):
    if len(t1) != len(t2):
        return False
    for i, item in enumerate(t1):
        if item != t2[i]:
            return False
    return True
