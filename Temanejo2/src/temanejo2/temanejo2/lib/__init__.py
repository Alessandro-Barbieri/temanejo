'''!
@brief This package provides some convenience Functions

In order to get the objects smaller and to reduce code duplication convenience Functions
and also more abstract converter etc. are defined in this package\n

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

__all__ = ["math"]