'''!
@brief This module provides all path conversion methods

In order to stay plattformindependent it is necessary that all paths are build at
runtime. \n
This module provides some methods in order to build paths out of a list if tokens etc... 


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import os


def makePathOfTokens(tokens=[]):
    path = ''
    for token in tokens:
        path = os.path.join(path, token)
    return path
