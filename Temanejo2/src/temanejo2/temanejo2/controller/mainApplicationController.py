'''!
@brief All Events on Application level will be handled in here

This module provides a Controller Object in which all events which relate to the main Application
are handled.\n
These can be 
- Close application
- Show application info 
- Misc. Preferences 
- etc.

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from temanejo2.temanejo2.core.items import miscDialogs
from temanejo2.temanejo2.lib import configuredValues
from temanejo2.temanejo2.controller.graph.remoteControls import ddtComunicator


logger = logging.getLogger(__name__)
##
#  A Controller Object for all actions and tasks on application level.
#  Can be everything from close application to show infoscreen
class MainAppControll(object):
    def __init__(self,temanejo):
        self.temanejo = temanejo
    
    ##
    #  Closes the application:
    #  - Disconnects From remoteControl. So the open threads and sockets are closed
    #  - calls destroy for the main Temanejo widget
    def closeApp(self,evt):
        #Disconnect from Ayudame
        if self.temanejo.remoteControl.rmc is not None:
            self.temanejo.remoteControl.disconnectFromRemote()
            logger.debug("Closed remoteconnection") 
        else:
            logger.debug("There was no connection to a Remote Application.\n Closed without killing any connections to remote controllers")

        self.temanejo.remoteControl.closeDDTconnection()
        #Kill Layoutworker
        if self.temanejo.layoutWorker.isActive:
            self.temanejo.layoutWorker.terminate()
        try:
            #Destroy Gui
            #self.temanejo.destroy()
            evt.accept()
            logger.info("Temanejo closed clean")
        except:
            logger.info("Temanejo closed clean")
    
    ##
    #  Connects to a Ayudame Instance by doing the following things:
    #  - Creates a Connection dialog
    #  - Gets the serverport from the connection dialog 
    #  - Calls the openServer Method from the remoteController of Temanejo
    def openServer(self):
        dialog = miscDialogs.ConnectionDialog()
        dialog.exec_()
        self.temanejo.remoteControl.openServer(dialog.connPort())

    def openDDTserver(self):
        dialog = miscDialogs.ConnectionDialog()
        dialog.exec_()
        self.temanejo.remoteControl.openDDTServer(port=dialog.connPort())

    ##
    #   Show Info Box
    def showAbout(self):
        miscDialogs.messageDialog(configuredValues.ABOUT_STRING,title="ABOUT")
        
    ##
    #   Here a custom report dialog will be created. 
    #   The user can create a email where he can report a bug or request a feature
    #   The email will be send to the developer-list.
    #   @TODO: Create BugDialog, for now it only shows annoying message
    def reportBug(self):
        message = """Sorry this feature is not yet implemented
                  """
        miscDialogs.messageDialog(message,title="Report Bug")
