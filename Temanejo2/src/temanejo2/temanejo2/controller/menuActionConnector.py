'''!
@brief Standard Menu Actions will be connected here

In this module an Object which connects the menuItems to their according actions
It will not define any functionality. Just the correct action for clicking on the items
will be connected.

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging
from functools import partial

from temanejo2.temanejo2.core.items import preferencesDialog
logger = logging.getLogger(__name__)

##
#    This object connects the menuItems to their according Action
class MenuActionConnector(object):
    ##
    #    Nothing fancy to do here. Just get the Instance of the mainWindow
    #    in order to be able to query the menuItems and bind them to a action 
    def __init__(self, temanejo):
        self.temanejo = temanejo
        
    ##
    #  Connect all Menuactions to their according controller
    #  Essentially just a wrapper for all kind of menu Actions     
    def connectMenuActions(self):
        self.connectGraphActions()
        self.connectFileActions()
        self.connectHelpActions()
        self.connectWindowActions()
        
        
    #===============================================================================================        
    #===============================================================================================
    #=====  Graph ==================================================================================
    #===============================================================================================
    #===============================================================================================
    ##
    #   Connects all Actions called by any MenuItem which are related to the Graph
    def connectGraphActions(self):
        self.connectGraphClearanceAction()
        self.connectGraphLayoutActions()
        self.connectNodeAppearanceActions()
        self.connectEdgeAppearanceActions()
        self.connectExporterActions()
        
    def connectExporterActions(self):
        self.connectExportToDotAction()
        self.connectExportToPDFAction()
        self.connectExportToPNGAction()
        self.connectImportFromDotAction()
        self.connectImportFromXMLAction()
        self.connectScreenshotAction()

    def connectScreenshotAction(self):
        self.temanejo.ui.action_svg_Screenshot.triggered.connect(
            partial(self.temanejo.importExportExternEventHandler.exportScreenshot))

    def connectExportToDotAction(self):
        self.temanejo.ui.actionExport_Graph_to_dot.triggered.connect(
            partial(self.temanejo.importExportExternEventHandler.exportToDot))

    def connectExportToPDFAction(self):
        self.temanejo.ui.actionExport_Graph_to_pdf.triggered.connect(
            partial(self.temanejo.importExportExternEventHandler.exportToPdf))

    def connectExportToPNGAction(self):
        self.temanejo.ui.actionExport_Graph_to_png.triggered.connect(
            partial(self.temanejo.importExportExternEventHandler.exportToPng))

    def connectImportFromDotAction(self):
        self.temanejo.ui.actionImport_Graph_from_dot.triggered.connect(
            partial(self.temanejo.importExportExternEventHandler.importFromDot))

    def connectImportFromXMLAction(self):
        self.temanejo.ui.actionImport_Graph_from_xml.triggered.connect(
            partial(self.temanejo.importExportExternEventHandler.importFromXML))

    # ===============================================================================================
    # =====  Graphlayout ============================================================================
    # ===============================================================================================
    def connectGraphClearanceAction(self):
        self.temanejo.ui.actionDeleteGraph.triggered.connect(
            partial(self.temanejo.graphAppearanceEventHandler.clearAllModels,self.temanejo))
    
    
    ##
    #   Connects all Actions called by any MenuItem which are related to the layout of the Graph
    def connectGraphLayoutActions(self):
        self.connectDotLayoutAction()
        self.connectCircoLayoutAction()
        self.connectTwopiLayoutAction()
        self.connectNeatoLayoutAction()
        self.connectFdpLayoutAction()
        self.connectNopLayoutAction()
        self.connectHLRSLayoutAction()
        
    def connectDotLayoutAction(self):
        self.temanejo.ui.actionDOT.triggered.connect(
            partial(self.runConnectPGVLayout,"dot"))
        
    
    def connectCircoLayoutAction(self):
        self.temanejo.ui.actionCirco.triggered.connect(
            partial(self.runConnectPGVLayout,"circo"))
        
         
    def connectTwopiLayoutAction(self):
        self.temanejo.ui.actionTwopi.triggered.connect(
            partial(self.runConnectPGVLayout,"twopi"))
        
        
    def connectNeatoLayoutAction(self):
        self.temanejo.ui.actionNeato.triggered.connect(
            partial(self.runConnectPGVLayout,"neato"))
        
    
    def connectFdpLayoutAction(self):
        self.temanejo.ui.actionFdp.triggered.connect(
            partial(self.runConnectPGVLayout,"fdp"))
        
    
    def connectNopLayoutAction(self):
        self.temanejo.ui.actionNop.triggered.connect(
            partial(self.runConnectPGVLayout,"nop"))
        
    def connectHLRSLayoutAction(self):
        self.temanejo.ui.actionHLRS_Layout.triggered.connect(
            partial(self.temanejo.graphlayoutIO.setRoutine,"hlrs_custom"))
    
    ##
    #   Convenience Method in order to run more than one action on pgv layout...
    def runConnectPGVLayout(self,routine):
        self.temanejo.graphlayoutIO.setRoutine("graphviz")
    
    #===============================================================================================
    #=====  NodeAppearance =========================================================================
    #===============================================================================================
    def connectNodeAppearanceActions(self):
        self.connectNodeColorAction()
        self.connectNodeMarginColorAction()
        
    def connectNodeColorAction(self):
        self.temanejo.ui.actionSet_Node_Colors.triggered.connect(
            partial(self.temanejo.graphAppearanceEventHandler.changeAllNodeColors))
    
    def connectNodeMarginColorAction(self):
        self.temanejo.ui.actionSet_Margin_Colors.triggered.connect(
            partial(self.temanejo.graphAppearanceEventHandler.changeAllNodeMarginColors))
    
    
    #===============================================================================================
    #=====  EdgeAppearance =========================================================================
    #===============================================================================================
    def connectEdgeAppearanceActions(self):
        self.connectEdgeColorAction()
        self.connectEdgeWidthAction()
        
    def connectEdgeColorAction(self):
        self.temanejo.ui.actionEdge_Color.triggered.connect(
            partial(self.temanejo.graphAppearanceEventHandler.changeAllEdgeColors))

    def connectEdgeWidthAction(self):
        self.temanejo.ui.actionEdge_Width.triggered.connect(
            partial(self.temanejo.graphAppearanceEventHandler.changeAllEdgeWidths))

    #===============================================================================================
    #===============================================================================================
    #=====  Window =================================================================================
    #===============================================================================================
    #===============================================================================================
    def connectWindowActions(self):
        self.connectPreferencesDialog()

    def connectPreferencesDialog(self):
        self.temanejo.ui.actionPreferences.triggered.connect(partial(self.temanejo.mainAppCtrl.reportBug))
        # self.temanejo.ui.actionPreferences.triggered.connect(
        #     partial(preferencesDialog.PreferencesDialog, self.temanejo, parent=self.temanejo))

    #===============================================================================================
    #===============================================================================================
    #=====  File ===================================================================================
    #===============================================================================================
    #===============================================================================================
    def connectFileActions(self):
        self.connectOpenServerAction()
        self.connectOpenDDTServerAction()
        self.connectCloseAction()
        
    
    def connectCloseAction(self):
        self.temanejo.ui.actionExit.triggered.connect(
            partial(self.temanejo.close))
 
    def connectOpenServerAction(self):
        self.temanejo.ui.actionOpenServer.triggered.connect(
            partial(self.temanejo.mainAppCtrl.openServer))

    def connectOpenDDTServerAction(self):
        self.temanejo.ui.actionOpenDDTServer.triggered.connect(
            partial(self.temanejo.mainAppCtrl.openDDTserver))

    #===============================================================================================        
    #===============================================================================================
    #=====  Help ===================================================================================
    #===============================================================================================
    #=============================================================================================== 
    def connectHelpActions(self):
        self.connectAboutAction()
        self.connectReportBugAction()
        
    def connectReportBugAction(self):
        self.temanejo.ui.actionReportBug.triggered.connect(
            partial(self.temanejo.mainAppCtrl.reportBug))
        
    def connectAboutAction(self):
        self.temanejo.ui.actionAbout.triggered.connect(
            partial(self.temanejo.mainAppCtrl.showAbout))
