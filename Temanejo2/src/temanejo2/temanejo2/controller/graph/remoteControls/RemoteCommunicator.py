'''!
@brief This module provides the Connector Object in order to communicate to Ayudame

The RemoteComunicator is set up here.\n
It communicates to the remote Application in a separate Thread.
There could be set Up multiple connections if necessary...\n
The use is just like with any other object which extends Thread.\n
Just build the object and call start.\n
A terminate is also implemented in order to shutdown the socket...


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import logging

from PySide import QtCore
from PySide.QtCore import QThread as Thread

from ayudame import ayu_socket

logger = logging.getLogger(__name__)




##
#   This class defines the communication between Temanejo
#   and the remote application.\n
#   The communication will be handled in a seperate Thread. Therefore the 
#   RemoteCommunicator extends the class QThread.\n
#   To instance the class you need to specify a port. By calling start() you start
#   the communication. \n
#   The connector gathers the events in a list called gatheredEvents this method can
#   then be called by any other method which is responsible for maipulating the gathered 
#   Data so it can be displayed in Temanejo.
class RemoteComunicator(Thread):
    
    ##
    #   Initializes the remote connection. It builds up a thread which can be started.\n
    #   In order to run this connector properly you should pass a initialized ayu_socket to the 
    #   Application.
    #   @param socket The socket should be an active/initialized Ayusocket. (Needs to be a serversocket...)
    def __init__(self):
        super(RemoteComunicator,self).__init__()
        ##@member self.isActive The flag for the readloop in the runmethod of the thread 
        self.isActive = False
        logging.info("Activated Remote Connection")
        ##@member self.gatheredEvents The list of catched events from the inputStream.
        self.gatheredEvents = []
        self.gatheredTaskCounter = 0
        self.gatheredDepCounter = 0
        ##@member self.signalizer Defines the signals for sending data since it cannot be in done
        #         in an object which extends Thread...
        self.signalizer = SignalsForRemoteConnection()
        
    ##
    #
    def setSocket(self,socket):
        ##@member self.socket A instance of a initialized ayu_Socket
        self.socket = socket
        
    ##
    #   The run method of the communicator. it runs in a while loop till the isActive flag is
    #   set to false.\n
    #   It reads from the socket and appends the incomming event to the gatheredEvents List 
    #   After that it sleeps for 10 milliseconds before starting to reading the next event
    #   out of the stream...
    def run(self):
        self.isActive = True
        logging.info("Remoteconnection started")
        inCount = 0
        while self.isActive:
            event = self.socket.socket_read()
            if event is not None:
                inCount = 0
                # For now it will send every new event so it do not gathering new tasks etc. but
                # it will gathering their updates 
                self.gatheredEvents.append(event)
                if event.get_event() == ayu_socket.AYU_ADDTASK:
                    self.gatheredTaskCounter+=1
                    task = ayu_socket.Intern_event_castToEventTask(event)
                    self.signalizer.addedTask.emit(task)
                    
                if event.get_event() == ayu_socket.AYU_ADDDEPENDENCY:
                    dep = ayu_socket.Intern_event_castToEventDependency(event)
                    self.signalizer.addedDependency.emit(dep)

                if event.get_event() == ayu_socket.AYU_SETPROPERTY:
                    prop = ayu_socket.Intern_event_castToEventProperty(event)
                    self.signalizer.addedProperty.emit(prop)

                if event.get_event() == ayu_socket.AYU_USERDATA:
                    userdata = ayu_socket.Intern_event_castToEventUserdata(event)
                    self.signalizer.addedUserdata.emit(userdata)

                
            else:
                inCount += 1
                Thread.yieldCurrentThread()
                if(inCount > 50):
                    Thread.msleep(50)
                elif(inCount > 100):
                    Thread.msleep(200)

    def socket_write(self, event):
        self.socket.socket_write(event)

    ##
    #   This method terminates the thread.\n 
    #   It sets the isActive flag for the while loop in the run() method to false.\n
    #   After doing that it checks if the socket is shutdownable and prints the result of that and
    #   finally it makes a force shutdown onto the socket.
    def terminate(self):
        self.isActive = False
        if self.socket.is_shutdownable:
            logging.info("Disconnected clean")
        else:
            logging.warning("There were unread events from Ayudame so couldn't disconnect clean \n Forced shutdown")
        
        self.socket.force_shutdown()
        
    ##
    #   This is a custom event method which should be called in order to get the 
    #   the gathered events. \n
    #   This is done that way in order to be able to buffer events till the user wants to update 
    #   the appearance of the graph. Typically this should be specified in a user preference.\n
    #   The method just clones the gatheredEvents list reset it and send the clone
    #   to the caller.
    def getGatheredDataEvent(self):
        # Temp gathered events List in order to return them after cleaning up
        # the gathered list.
        tempGatheredEvents = list(self.gatheredEvents)
        #Clear Gathered Data
        self.gatheredEvents = []
        self.gatheredTaskCounter = 0
        self.gatheredDepCounter = 0
        return tempGatheredEvents
    
    ##
    #   Just returns the number of gathered tasks.\n
    #   Necessary if the user wants to update the gui e.g. everytime when there 
    #   are more than 10 tasks gathered or similar
    def getGatheredTaskCount(self):
        return self.gatheredTaskCounter
    
    ##
    #   Just returns the number of gathered dependencies.\n
    #   Necessary if the user wants to update the gui e.g. everytime when there 
    #   are more than 10 dependencies gathered or similar
    def getGatheredDepCount(self):
        return self.gatheredDepCounter

##
# @TODO Description
class SignalsForRemoteConnection(QtCore.QObject):
    ##
    # Signal which will be send when a new Task comes in
    addedTask = QtCore.Signal(ayu_socket.Intern_event_task)
    addedDependency = QtCore.Signal(ayu_socket.Intern_event_dependency)
    addedProperty = QtCore.Signal(ayu_socket.Intern_event_property)
    addedUserdata = QtCore.Signal(ayu_socket.Intern_event_userdata)