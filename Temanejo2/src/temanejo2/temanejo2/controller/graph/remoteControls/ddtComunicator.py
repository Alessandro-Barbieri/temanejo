import json
import logging
import select
import socket
import struct
import sys

from PySide import QtCore
from PySide.QtCore import QThread as Thread

from temanejo2.temanejo2.core import mvcConnector
from temanejo2.temanejo2.core.items import miscDialogs

from temanejo2.temanejo2.core.items import DataProperties
logger = logging.getLogger(__name__)


##
#   This class defines the communication between Temanejo2 and ddt.
#   The recieved data is handled in as separate thread. So it is constantly listening to any data
#   passed through the port. It divides the
class DDTComunicator(Thread):
    def __init__(self,temanejo, host='localhost', port=8889):
        self.temanejo=temanejo
        super(DDTComunicator, self).__init__()
        self.ddt_socket = None
        self.clients=0
        self.outputs = []
        self.connect(host, port)
        logger.info("Generated socket for communication ddt")
        ## @member self.isActive The flag for the readloop in the runmethod of the thread
        self.isActive = False
        ## @member self.gatheredEvents The list of catched events from the inputStream.
        self.gatheredEvents = []
        logger.info("Generated connector to ddt")
        self.ddt_signals = DDTsignals()

    ##
    #  Creates a socket binds it to the parameter host and ports and listens to max 5 connections
    def connect(self, host, port):
        logger.debug("connect", host, ":", port)
        self.ddt_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ddt_socket.bind((host, port))
        self.ddt_socket.settimeout(0.1)
        #become a server socket
        self.ddt_socket.listen(5)

    ##
    #   Sends a message to all currently available clients
    def _send(self, msg):
        for i in self.outputs:
            i.send(msg)

    ##
    # Sens a message to ddt where msg is not yet in a defined state. It is basically any data.
    # In order to let the receiver know how much data has to be read, it is necessary to send
    # a message of 24 bytes at first, where the amount of data which will be send is needed to
    # defined. This done automatically by sending the msglen = len(msg) at first.
    def ddt_send(self, jsonObj):
        msg = json.dumps(jsonObj)
        if self.ddt_socket:
            frmt = "=%ds" % len(msg)
            packedMsg = struct.pack(frmt, msg)
            packedHdr = struct.pack('=I', len(packedMsg))
            if (len(self.outputs) == 0):
                logger.error("There is currently no connection ready for sending messages to ddt")
            else:
                self._send(packedHdr)
                self._send(packedMsg)
        else:
            logger.error("No connection was available for sending messages to ddt")


    ##
    #   Actually runs the communication thread
    def run(self):
        # The inputs from where to query from
        inputs = [self.ddt_socket, sys.stdin]
        # The outputs where to send to
        self.outputs = []
        # Thread run variable
        self.isActive = True
        # Really run the communication thread
        while self.isActive:
            # Check for Inputs and outputs
            try:
                inputready, outputready, exceptready = select.select(inputs, self.outputs, [])
            except select.error, e:
                break
            except socket.error, e:
                break
            # Read all available inputs
            for s in inputready:
                if s == self.ddt_socket:
                    # handle the server socket
                    client, address = self.ddt_socket.accept()
                    logger.info('Temanejo: Recieved connection %d from %s' % (client.fileno(), address))

                    # Compute client name and send back
                    self.clients += 1
                    inputs.append(client)
                    self.outputs.append(client)

                elif s == sys.stdin:
                    # handle standard input
                    junk = sys.stdin.readline()
                    logger.info("Temanejo: Closing %s" % junk)
                    self.isActive = False
                else:
                    # handle all other sockets
                    try:
                        data = s.recv(4)
                        if data:
                            msg_len = struct.unpack('=I', data)
                            data = s.recv(msg_len[0])
                            frmt = "=%ds" % msg_len
                            msg = struct.unpack(frmt, data)
                            event = json.loads(msg[0])
                            self.ddt_signals.dtt_communicated.emit(event)
                        else:
                            logger.info('Temanejo: %d was closed' % s.fileno())
                            self.clients -= 1
                            s.close()
                            inputs.remove(s)
                            self.outputs.remove(s)

                    except socket.error, e:
                        # Remove
                        inputs.remove(s)
                        self.outputs.remove(s)

        self.ddt_socket.close()


    def close(self):
        if self.ddt_socket is None:
            return
        self.ddt_socket.shutdown(socket.SHUT_RDWR)
        self.ddt_socket.close()
        self.ddt_socket = None
##
#  This is a class which handles input and output events between temanejo and ddt
class DDTMessage(object):


    def __init__(self, temanejo):
        self.temanejo = temanejo
        self.ddt_id = 1
        self.map_id_key = {}

    # @QtCore.Slot(str)
    def parse_ddt_event(self, communicator_input):
        for key in communicator_input.keys():
            if 'request' in key:
                self.ddt_request(key, communicator_input[key])
            elif 'response' in key:
                self.ddt_response(key, communicator_input[key])

    ##
    #   Response to request from ddt
    def ddt_request(self, req_key, request_json):
        logger.debug("DDT-Communication: Got request from ddt: %s"%req_key)
        self.temanejo_response_to_request(req_key, request_json)

    ##
    #   Handle responses from ddt after requesting stuff
    def ddt_response(self, resp_key, response_json):
        if 'responseFunctionInfo' == resp_key:
            function = response_json["function"]
            mpiRank = response_json["mpiRank"]
            propOwnerId = self.map_id_key[response_json["id"]]
            address = response_json["address"]
            self.temanejo.dataParser.parseProperty_plain(propOwnerId, "ddt_function name", function)
        elif 'responseAddBreakpoint' == resp_key:
            logger.error("No response to add breakpoint is yet implemented in temanejo. Values are: %s"%json.dumps(response_json))
        else:
            logger.error("DDT-Communication: The case %s is not yet implemented"%resp_key)

    ##
    #   Response to a ddt request
    def temanejo_response_to_request(self, req_key, request_json):
        responded_json = json.loads('{"responseColorMap": { "id": 1, "array": [ { "threadId": 1, "active": "#111111", '
                                    '"inactive": "#000000" }, { "threadId": 2, "active": "#222222", '
                                    '"inactive": "#000000"}, { "threadId": 3,"active": "#333333","inactive": "#000000"}]}}')
        miscDialogs.messageDialog("This Feature is currently under construction. Using it might not result in "
                                  "the expected behaviour", "WARNING")
        logging.error("This Feature is currently under construction. Using it might not result in the expected behaviour")
        self.temanejo.remoteControl.ddt_communicator.ddt_send(responded_json)

    ##
    #   Request function info from ddt
    def requestFunctionInfo(self):

        #todo mpi rank
        mpiRank =1
        requestFunctionInfo_tmp = '{"requestFunctionInfo": { "id": %s, "mpiRank": %s, "address": "%s"}}'

        datanodes     = self.temanejo.remoteControl.datanodes
        #key, node = datanodes.items()[1]
        #ad = node.getProperty("pointer_address")
        #self.map_id_key[self.ddt_id] = key
        #requestFunctionInfo = requestFunctionInfo_tmp % (self.ddt_id, mpiRank, ad)
        #self.ddt_id += 1
        #logger.info(requestFunctionInfo)
        #requestFunctionInfo_json = json.loads(requestFunctionInfo)
        #self.temanejo.remoteControl.ddt_communicator.ddt_send(requestFunctionInfo_json)

        for key, node in datanodes.items():
            if node.getProperty("pointer_address") != None:
               ad=node.getProperty("pointer_address")
               self.map_id_key[self.ddt_id] = key
               requestFunctionInfo = requestFunctionInfo_tmp % (self.ddt_id, mpiRank, ad)
               self.ddt_id += 1
               requestFunctionInfo_json = json.loads(requestFunctionInfo)
               self.temanejo.remoteControl.ddt_communicator.ddt_send(requestFunctionInfo_json)

    ##
    #   Updates the temanejo state when using ddt
    def ddt_update_state(self):
        self.requestFunctionInfo()
        logging.error("This Feature is currently under construction. Using it might not result in the expected behaviour")

    ##
    #
    def request_add_breakpoint(self, function_name):
        # Specs: choose one solution "requestAddBreakpoint": {"id": 1,"array": [{"address": "0x1"},{"file": "main.c","line": 1},{"function": "main"}]},
        requestAddBreakpoint_tmp = '{"requestAddBreakpoint": { "id": %s, "array":[{"function": "%s"}]}}'

        datanodes = self.temanejo.remoteControl.datanodes
        requestAddBreakpoint = requestAddBreakpoint_tmp % (self.ddt_id, function_name)

        requestAddBreakpoint_json = json.loads(requestAddBreakpoint)
        self.temanejo.remoteControl.ddt_communicator.ddt_send(requestAddBreakpoint_json )
        self.ddt_id += 1




##
#   Container for different signals used for the ddt communication
class DDTsignals(QtCore.QObject):

        dtt_communicated = QtCore.Signal(dict)
