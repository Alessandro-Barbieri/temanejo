'''!
@brief This package provides all the code for remote connecting and parsing the input Data

All actions needed to display the data from a remote Application or a dotFile etc. 
will be implemented in here.\n
This will be everything from the setup of a remote connection to parsing the data and setting up
the datastructures needed for displaying the Data in Temanejo properly  

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''