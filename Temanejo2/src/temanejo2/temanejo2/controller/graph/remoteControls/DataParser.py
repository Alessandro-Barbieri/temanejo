'''!
@brief The DataParser will parse the incoming data.

All incomming data will be parsed into a proper format in order to be able to display the data 
correctly in Temanejo. 

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtCore

from temanejo2.temanejo2.core.items import DataEdge, DataNode, DataProperties
from temanejo2.temanejo2.lib import configuredValues
from temanejo2.temanejo2.lib.easymodel import buttonDelegate, treemodel
from temanejo2.temanejo2.controller.graph.remoteControls import helpers

logger = logging.getLogger(__name__)

##
#  The DataParser is an object which parses the Incoming Data
#  Since only Ayudame is supported for now there is no need 
#  to define different Parser. When new tools will be supported 
#  This class has to be refactored and a Interface needs to be specified... 
class DataParser(object):

    ##
    #  Constructor of the DataParser
    #  @param temanejo The instance of the main application
    def __init__(self,temanejo):
        self.temanejo = temanejo
        self._initialMode = True
        self._dep_initialMode = True

    ##
    #   Resets the parserState to initial
    def resetToInitialState(self):
        self._initialMode = True
        self._dep_initialMode = True
    

    def parseTask(self, task):
        nodeAttributes = dict()
        taskId = int("%s%s"%(task.get_client_id(), task.get_task_id()))
        clientId = task.get_client_id()

        #Set the nodeAttributes
        nodeAttributes["None"]        = "None"
        nodeAttributes["id"]          = taskId
        nodeAttributes["taskId"]      = task.get_task_id()
        nodeAttributes["clientId"]    = clientId
        nodeAttributes["hostId"]      = helpers.get_hostId(clientId)
        nodeAttributes["processId"]   = helpers.get_processId(clientId)
        nodeAttributes["label"]       = task.get_task_label()
        nodeAttributes["state"]       = False
        nodeAttributes["paused"]      = False
        nodeAttributes["scope"]       = task.get_scope_id()
        nodeAttributes["scopeClient"] = task.get_scope_client_id()

        self.parseTask_plain(taskId, nodeAttributes)

    ##
    #  Parses an incoming Task by doing the following:
    #  - Fetching the existing datanodes
    #  - Querying the delivered Attributes
    #  - add the node to the PGV-Graph to be layouted
    #  - Create a dataNode for the Task 
    def parseTask_plain(self, taskId, nodeAttributes):

        # Get handle to the datanodes
        datanodes = self.temanejo.remoteControl.datanodes
        
        #Check if there are some props in the list of unknownProps yet
        for prop in self.temanejo.unknownProperties:
            if taskId == prop[0]:
                nodeAttributes[prop[1]] = prop[2]
                self.temanejo.unknownProperties.remove(prop)

        
        # Create the Dataproperty
        for attr in nodeAttributes.keys():
            self.parseTaskDepAddedProperty((attr, nodeAttributes[attr]), taskId, "node")


        # Add node to the graph object of the layout routine
        self.temanejo.graphlayoutIO.addNode(taskId)
        #logger.debug("Node with id: %s"%taskId)
        # Update the attributes with the pgv attrs...
        # Not in use now since pgv attributes shouldn't be in use anymore
        # nodeAttributes.update(self.temanejo.graphlayoutIO.agraph.get_node(taskId).attr)

        #Create a DataNode for the Task
        datanodes[taskId] = DataNode.DataNode(taskId, None, properties = nodeAttributes)
        if self.temanejo.graphModel.nodeExists(taskId):
            self.temanejo.graphModel.getNode(taskId).setDataNode(datanodes[taskId])

        
    def parseDependency(self, dep):
        edgeAttributes = dict()
        depId = int(u"%s%s"%(dep.get_client_id(), dep.get_dependency_id()))
        fromToId = (u"%s%s"%(dep.get_client_id(), dep.get_from_id()), u"%s%s"%(dep.get_to_client_id(), dep.get_to_id()))

        #Set the dependency attributes
        edgeAttributes["None"] = "None"
        edgeAttributes["depId"] = depId
        edgeAttributes["id"] = dep.get_dependency_id()
        edgeAttributes["label"] = dep.get_dependency_label()
        edgeAttributes["fromToId"] = fromToId
        self.parseDependency_plain(depId, fromToId, edgeAttributes)

    ##
    #   Parses an incoming Task by doing the following:
    #  - Fetching the existing dataEdges
    #  - Querying the delivered Attributes
    #  - add the edge to the PGV-Graph to be layouted
    #  - Create a dataEdge for the Dependency 
    def parseDependency_plain(self, depId, fromToId, edgeAttributes):

        #Get Handle to the 
        dataedges = self.temanejo.remoteControl.dataedges

        #Get original dependency ID
        #origDepId = dep.get_dependency_id()
        
        
        #Check if there are some props in the list of unknownProps yet

        for prop in self.temanejo.unknownProperties:    
            if depId == prop[0]:
                edgeAttributes[prop[1]] = prop[2]
                self.temanejo.unknownProperties.remove(prop)



        # Create the Dataproperty
        for attr in edgeAttributes.keys():
            self.parseTaskDepAddedProperty((attr, edgeAttributes[attr]), depId, "dep")
        
        # Add dependency to the graph object of the layout routine
        self.temanejo.graphlayoutIO.addDep(fromToId[0], fromToId[1], depId)
        
        # Update the attributes with the pgv attrs...
        # Not in use now since pgv attributes shouldn't be in use anymore
        # edgeAttributes.update(self.temanejo.graphlayoutIO.agraph.get_edge(depId[0],depId[1]).attr)

        dataedges[depId] = DataEdge.DataEdge(depId, edgeAttributes)
        # logger.debug("Dependencycount: %s"%len(dataedges))




    def parseProperty(self, prop):
        propOwnerId   = int("%s%s"%(prop.get_client_id(), prop.get_property_owner_id()))
        key = prop.get_key()
        value = prop.get_value()

        self.parseProperty_plain(propOwnerId, key, value)

    ##
    #   Parses the given property and adds it to the proplist model
    #   If it already exists it updates the attributes...
    #   This method is called when a property comes along alone
    #   If there is no task or dependency yet to which the property can be assigned yet
    #   it is added to a list of unknownProperties which will be hold int the temanejo object. 
    #   It also adds the property to the property models. 
    #   @param property 
    def parseProperty_plain(self, propOwnerId, key, value):
        # Search the according item by id
        # If found add the property
        #     If property already exists update the value else create the property
       
        dataedges     = self.temanejo.remoteControl.dataedges
        datanodes     = self.temanejo.remoteControl.datanodes
        dataprops     = self.temanejo.remoteControl.dataproperties
        dep_dataprops = self.temanejo.remoteControl.dep_dataproperties
        item = None
        p_type = None
        #Check if Prop correspond to a task
        try:
            item = datanodes[propOwnerId]
            p_type = "node"
        except:
            pass
        #Check if Prop correspond to a dep
        try:
            item = dataedges[propOwnerId]
            p_type = "dep"
        except:
            pass
        #If item is None add it to unknown props and skip the rest 
        if item is None:
            self.temanejo.unknownProperties.append((propOwnerId, key, value, p_type))
            return
        #Else update the item
        else: 
            item.setProperty(key, value)
            #Create a Property Data Object
            if p_type == 'node':
                dataprops[key] = DataProperties.DataProperty(key, value, p_type)
            else:
                dep_dataprops[key] = DataProperties.DataProperty(key, value, p_type)
        # === Add the property to the models ===
        #First check if prop shall be ignored in the views
        # If it shall be ignored just skip the rest
        if key in configuredValues.PROP_DISPLAY_IGNORE_LIST:
            return


        # Add prop to node props
        if p_type == "node":
            #Add it to the Selectionmodel
            propSelectionModel = self.temanejo.propSelectionModel
            propViewModel = self.temanejo.propDisplayModel
            existingKeys = propSelectionModel.getPropertyKeys()
            if key not in existingKeys:
                    propSelectionModel.insertRows(propSelectionModel.rowCount(), 1, value=dataprops[key])
                    self.addPropRootToTreeView((key, value), propOwnerId, propViewModel,
                                               self.temanejo.propDisplayModelRootItem)
            #else update the propvalue list
            else:
                #first get the parent item
                propRootItem = propViewModel.propertyRootItems[propSelectionModel.getPropKeyIndex(key)]
                #then add it to the model
                self.addPropValueToTreeView(propRootItem, (key, value), propOwnerId, propViewModel)

        # Add prop to dep props
        elif p_type == "dep":

            #Add it to the Selectionmodel
            depPropSelectionModel = self.temanejo.depPropSelectionModel
            depPropViewModel = self.temanejo.depPropDisplayModel
            existingKeys = depPropSelectionModel.getPropertyKeys()
            if key not in existingKeys:
                    depPropSelectionModel.insertRows(depPropSelectionModel.rowCount(), 1, value=dep_dataprops[key])
                    self.addPropRootToTreeView((key, value), propOwnerId, depPropViewModel,
                                               self.temanejo.depPropDisplayModelRootItem)
            #else update the propvalue list
            else:
                #first get the parent item
                propRootItem = depPropViewModel.propertyRootItems[depPropSelectionModel.getPropKeyIndex(key)]
                #then add it to the model
                self.addPropValueToTreeView(propRootItem, (key, value), propOwnerId, depPropViewModel)

        # Update the view so the change of propertyvalues is recognized directly
        for item in self.temanejo.propertyWidgets + self.temanejo.dep_propertyWidgets:
            item.enforceUpdate()

    ##
    #   Parses the given property and adds it to the propList model
    #   This method is called when properties come with tasks and dependencies
    #   @param property It has to be a tuple with name and attributes of the property 
    def parseTaskDepAddedProperty(self, prop, propOwnerId, p_type):
        props = self.temanejo.remoteControl.dataproperties
        dep_props = self.temanejo.remoteControl.dep_dataproperties
        # Handles to the propertymodels
        propSelectionModel = self.temanejo.propSelectionModel
        propViewModel = self.temanejo.propDisplayModel
        depPropSelectionModel = self.temanejo.depPropSelectionModel
        depPropViewModel = self.temanejo.depPropDisplayModel
        #First check if prop shall be ignored in the views
        # If it shall be ignored just skip the rest
        if prop[0] in configuredValues.PROP_DISPLAY_IGNORE_LIST:
            return
        # Check if prop already exists
        # if not create it and add it to the propSelectionModel
        if p_type == "node":
            if not prop[0] in props.keys():
                #Add the prop to the list selection model
                props[prop[0]] = DataProperties.DataProperty(prop[0], attributes=prop[1], p_type=p_type)

                propSelectionModel.insertRows(propSelectionModel.rowCount(), 1, value=props[prop[0]])
                #Add the prop to the prop value display view
                #Set the new Property Root item
                self.addPropRootToTreeView(prop, propOwnerId, propViewModel, self.temanejo.propDisplayModelRootItem)

            #else update the propvalue list
            else:
                #first get the parent item
                propRootItem = propViewModel.propertyRootItems[propSelectionModel.getPropKeyIndex(prop[0])]
                #then add it to the model
                self.addPropValueToTreeView(propRootItem, prop, propOwnerId, propViewModel)


        elif p_type == "dep":
            if not prop[0] in dep_props.keys():
                #Add the prop to the list selection model
                dep_props[prop[0]] = DataProperties.DataProperty(prop[0], attributes=prop[1], p_type=p_type)
                depPropSelectionModel.insertRows(depPropSelectionModel.rowCount(), 1, value=dep_props[prop[0]])
                #Add the prop to the prop value display view
                #Set the new Property Root item
                self.addPropRootToTreeView(prop, propOwnerId, depPropViewModel, self.temanejo.depPropDisplayModelRootItem)

            #else update the propvalue list
            else:
                #first get the parent item
                depPropRootItem = depPropViewModel.propertyRootItems[depPropSelectionModel.getPropKeyIndex(prop[0])]
                #then add it to the model
                self.addPropValueToTreeView(depPropRootItem, prop, propOwnerId, depPropViewModel)


        #Set the correct index to the treeview
        if self._initialMode or self._dep_initialMode:
            if p_type == "node":
                for propWidget in self.temanejo.propertyWidgets:
                    propWidget.setPropRootIndex(propSelectionModel.getPropKeyIndex(prop[0]))
                self._initialMode = False
            elif p_type == "dep":
                for dep_propWidget in self.temanejo.dep_propertyWidgets:
                    dep_propWidget.setPropRootIndex(depPropSelectionModel.getPropKeyIndex(prop[0]))
                    dep_propWidget.setPropRootIndex(1)
                    dep_propWidget.setPropRootIndex(depPropSelectionModel.getPropKeyIndex(prop[0]))
                self._dep_initialMode = False

            
    #------------ Helper for adding the stuff to the models -----------
    #TODO: Port this stuff to the modelclasses so here only a call to the according model has to be made
    
    ##
    #    
    def addPropRootToTreeView(self, property, ownerId, propViewModel, root_item):
        propViewModel.addToKVStore(property[0])
        propRootItem = treemodel.TreeItem(treemodel.ListItemData([property[0],["Testwert","Testwert2"]]),
                                          parent=root_item,
                                          index=propViewModel.index(propViewModel.root.child_count(),
                                                                    1,
                                                                    parent=propViewModel.root.index()))
        
        propViewModel.propertyRootItems.append(propRootItem) 
        #TODO: Flatten the proplists via [item for sublist in l for item in sublist]
        #Add the value for the new item 
        self.addPropValueToTreeView(propRootItem, property, ownerId, propViewModel)

    ##
    #   
    #TODO: Flatten the proplists via [item for sublist in l for item in sublist]    
    def addPropValueToTreeView(self, propRootItem, property, ownerId, propViewModel):
        if not propViewModel.propValueExists(property[0],property[1]):
            propViewModel.addToKVStore(property[0], value=property[1], itemId=ownerId)
            #Add the widget for colorchoosing to all items
            colorButtonDelegate = buttonDelegate.ButtonDelegate(self.temanejo)
            treeItemData = treemodel.ListItemData([property[1],
                                                   propViewModel.kvStore[property[0]][property[1]],
                                                   colorButtonDelegate])
            treeItemData.addFlag(QtCore.Qt.ItemIsEditable)
            #Add the item
            propValueItem = treemodel.TreeItem(treeItemData, parent=propRootItem,
                                               index=propViewModel.index(propRootItem.child_count(), 
                                                                         2,
                                                                         parent=propRootItem.index()))
        else:
            propViewModel.addToKVStore(property[0],value=property[1],itemId=ownerId)
    #------------ END: Helper for adding the stuff to the models -----------

    ##
    #   Slot for the signal addedTask of the RemoteCommunicator 
    def taskAdded(self,task):
        logger.debug("Added TASK %s from client %s"%(task.get_task_label() ,task.get_client_id()))
        self.parseTask(task)
    
    ##
    #   Slot for the signal addedDependency of the RemoteCommunicator    
    def depAdded(self,dep):
        self.parseDependency(dep)
    
    ##
    #   Slot for the signal addedProperty of the RemoteCommunicator
    def propAdded(self,prop):
        logger.debug("Added Property ")
        self.parseProperty(prop)
        
    def userdataAdded(self,userdata):
        logger.error("Unhandled Userdata: %s"%(userdata.get_data()))
