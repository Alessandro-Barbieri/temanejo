'''!
@brief All Appearance changes of the Graph are handled in here 

This Module provides a Graph Library class for every appearance options like zooming
etc. It is not the layout routine. Here just all change events are handled

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from temanejo2.temanejo2.core.items import miscDialogs
from temanejo2.temanejo2.lib import configuredValues, qhelperStuff

logger = logging.getLogger(__name__)



##
# All concrete graph events are handled here. So all modifications like zooming or scrolling 
# should be handled in here. This is done that way in order to deduplicate code.
# This class is only called by other controller and not set to ANY view or anything else.
# It is more a library for all controller classes
class GraphAppearanceEvents(object):
    def __init__(self, widget):
        self.widget = widget
        self.scaleFactor = configuredValues.DEFAULT_ZOOM_FACTOR
    ##
    # Here all the graph zoom is done.
    # @param factor The Zoomfactor >1 for zooming in and <1 for zooming out
    def zoom_ctrl(self,factor):
        self.widget.scale(factor,factor) 
        self.scaleFactor *=factor
    ##
    # By calling this method you set the zoom back to the default value
    def zoom_default(self):
        self.zoom_ctrl(1/self.scaleFactor)
        self.scaleFactor = configuredValues.DEFAULT_ZOOM_FACTOR
    
    ##
    # Zoom to Fit
    def zoom_fit(self,temanejo):
        def computeFitZoomFactor():
            zoomFactor  = 1
            sceneRect   = temanejo.ui.temanejoGraph.sceneRect()
            visibleRect = qhelperStuff.getVisibleRect(temanejo.ui.temanejoGraph)
            width = sceneRect.width()
            height = sceneRect.height()
            if width > height:
                if not width == 0:
                    zoomFactor = visibleRect.width() / width 
            else:
                if not height == 0: 
                    zoomFactor = visibleRect.height() / height 
            return zoomFactor
        
        self.zoom_ctrl(computeFitZoomFactor())
    
    
    ##
    # This will move the scene by x,y
    def translate_scene(self,x,y):
        current_center = self.widget.mapToScene(self.widget.viewport().size().width()/2,
                                                self.widget.viewport().size().height()/2)
        self.widget.translate(x/self.scaleFactor,y/self.scaleFactor)
        self.widget.centerOn(current_center.x()-x/self.scaleFactor,
                             current_center.y()-y/self.scaleFactor)
    
    ##
    # This will change the color of all nodes in the scene
    def changeAllNodeColors(self):
        overallColor = miscDialogs.colordialog()
        if overallColor is None:
            return
        for node in self.widget.scene().getNodes():
            node.setNodeColor(overallColor)
                
    def changeNodeColorOnPropDep(self, nodeItem, viewLabel):
        if nodeItem is not None:
            nodeIds = nodeItem[1]
        else:
            nodeIds = []
        color = miscDialogs.colordialog()
        if color is None:
            return
        #setColor in propertyView
        nodeItem[-1] = color
        logger.debug("Nodeids to be colored: %s"%nodeIds)
        for node in self.widget.scene().getNodes():
            logger.debug("node from scene: %s"%node.getNodeId())
            if str(node.getNodeId()) in nodeIds:
                if viewLabel == "Node color means":
                    node.setNodeColor(color = color)
                elif viewLabel == "Margin color means":
                    node.setMarginColor(color = color)

    ##
    # This will change the color of all nodemagrins in the scene
    def changeAllNodeMarginColors(self):
        overallMarginColor = miscDialogs.colordialog()
        if overallMarginColor is None:
            return
        for node in self.widget.scene().getNodes():
            node.setMarginColor(overallMarginColor)

    def changeEdgeColorOnPropDep(self, edgeItem, viewLabel):
        if edgeItem is not None:
            edgeIds = edgeItem[1]
        else:
            edgeIds = []
        color = miscDialogs.colordialog()
        if color is None:
            return
        #setColor in propertyView
        edgeItem[-1] = color
        logger.debug("Edgeids to be colored: %s"%edgeIds)
        for edge in self.widget.scene().getEdges():
            logger.debug("edge from scene to be collored: %s"%edge.getEdgeId())
            if str(edge.getEdgeId()) in edgeIds:
                if viewLabel == "Edge color means":
                    edge.setEdgeColor(color = color)
                #elif viewLabel == "Margin color means":
                #    node.setMarginColor(color = color) 

    ##
    # This will change the color of all edges in the scene
    def changeAllEdgeColors(self):
        overallColor = miscDialogs.colordialog()
        if overallColor is None:
            return
        for edge in self.widget.scene().getEdges():
            edge.setEdgeColor(overallColor)
    
    ##
    #   Interactive Changing of edgewidth         
    def changeAllEdgeWidths(self):
        def changeEdgeWidthConstant(value):
            configuredValues.DEFAULT_EDGE_WIDTH = (value + 1.0) / 10
            self.widget.viewport().update()
        dialog = miscDialogs.SliderDialog("Choose Edgewidth",changeEdgeWidthConstant,parent=self.widget)
        dialog.show()     
        
    ##
    #   The clearance of the graph.
    #   It will first delete all graphic items from the scene and then clear the propertymodels,
    #   so the initial startup state will be restored.\\
    #   This leads to the opportunity to connect to a new application without closing temanejo.
    def clearAllModels(self, temanejo):
        # Remove all data from the pgvgraph
        self.__clearPGVGraph(temanejo)
        # Remove Graph Data
        temanejo.remoteControl.dataedges.clear()
        temanejo.remoteControl.datanodes.clear()
        # Remove all qgraphicsitems from the scene
        temanejo.graphModel.clearGraph()
        # Clear Prop Model
        # node Model 
        temanejo.propSelectionModel.clearModel()
        temanejo.propDisplayModel.clearModel()
        # node Model
        temanejo.depPropSelectionModel.clearModel()
        temanejo.depPropDisplayModel.clearModel()

        # Remove Prop Data
        temanejo.remoteControl.dataproperties.clear()
        temanejo.remoteControl.dep_dataproperties.clear()
        temanejo.statusBar().showMessage("All gathered (Graph and Prop) data was deleted")

    
    ##
    #   Since the standard clear operation from the AGraph class does also deletes the 
    #   graphattributes like the prefered layoutdirection we do have our own delete graph option
    #   in order to have only node and edge attributes deleted from the graph but not the graph 
    #   itself
    def __clearPGVGraph(self,temanejo):
        edges = temanejo.remoteControl.graph.edges()
        nodes = temanejo.remoteControl.graph.nodes()
        # temanejo.remoteControl.graph.delete_edges_from(edges)
        # temanejo.remoteControl.graph.delete_nodes_from(nodes)
        temanejo.remoteControl.layout_routine.reset_graph()
