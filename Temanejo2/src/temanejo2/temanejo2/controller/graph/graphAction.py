'''!
@brief All Graph action events are defined in this module

This module provides a class which handles all the graph action events like stepping forward etc.

@author: Stefan Reinhardt
@contact temanejo@hlrs.de
'''
import logging

from ayudame import ayu_socket

logger = logging.getLogger(__name__)


class GraphActionEvents(object):

    def __init__(self, temanejo):
        self.temanejo = temanejo
        #self.socket = ayu_socket.Ayu_Socket()
        self.currentlypaused = True

    def blockTask(self, client_id, task_id):
        try:
            request = ayu_socket.Intern_event_create_intern_request(client_id,
                                                                    ayu_socket.AYU_REQUEST,
                                                                    0,
                                                                    ayu_socket.AYU_BLOCK_TASK,
                                                                    "block_task",
                                                                    str(task_id))
        except:
            logger.warning("No remote connection is open. So the task could not be blocked")
            return False
        try:
            self.temanejo.remoteControl.rmc.socket_write(request)
            logger.info("Blocked task %i:%i", client_id, task_id)
        except:
            logger.error("No remote client was available at port %s"%self.temanejo.remoteControl.getActivePort())
            return False
        return True

    def unblockTask(self, client_id, task_id):
        try:
            request = ayu_socket.Intern_event_create_intern_request(client_id,
                                                                    ayu_socket.AYU_REQUEST,
                                                                    0,
                                                                    ayu_socket.AYU_UNBLOCK_TASK,
                                                                    "unblock_task",
                                                                    str(task_id))
        except:
            logger.warning("No remote connection is open. So the task could not be unblocked")
            return False

        try:
            self.temanejo.remoteControl.rmc.socket_write(request)
            logger.info("Unblocked task %i:%i", client_id, task_id)
        except:
            logger.error("No remote client was available at port %s"%self.temanejo.remoteControl.getActivePort())
            return False
        return True

    def steppingForward(self):

        #clients + tasks
        #client_Ids = self.temanejo.propDisplayModel.kvStore["clientId"]
        try:
            client_Ids = self.temanejo.propDisplayModel.kvStore["clientId"].keys()
        except:
            logger.error("No Clients were found in the KV Store")
            return

        for client in client_Ids:
            request = ayu_socket.Intern_event_create_intern_request(client,
                                                                    ayu_socket.AYU_REQUEST,
                                                                    0,
                                                                    ayu_socket.AYU_STEP,
                                                                    "step_client",
                                                                    "1")
            try:
                self.temanejo.remoteControl.rmc.socket_write(request)
            except:
                logger.error("No remote client was available at port %s"%self.temanejo.remoteControl.getActivePort())

        logger.info("Stepped forward by 1 step")



    def steppingFastForward(self,numOfSteps):
        for i in range(numOfSteps):
            self.steppingForward()

    def pauseRun(self, button_pointer):
        if(self.currentlypaused):
            #Set icon
            button_pointer.setIcon(':images/tem_pause.png')
            #unpause session
            try:
                client_Ids = self.temanejo.propDisplayModel.kvStore["clientId"].keys()
                for client in client_Ids:
                    request = ayu_socket.Intern_event_create_intern_request(client, ayu_socket.AYU_REQUEST, 0,
                                                                            ayu_socket.AYU_CONTINUE, "continue", "")
                    try:
                        self.temanejo.remoteControl.rmc.socket_write(request)
                    except:
                        logger.error(
                            "No remote client was available at port %s" % self.temanejo.remoteControl.getActivePort())

            except:
                logger.error("No Clients were found in the KV Store")


            logger.info("Unpaused")
        else:
            #set icon
            button_pointer.setIcon(':images/tem_unpause.png')
            # pause session
            try:
                client_Ids = self.temanejo.propDisplayModel.kvStore["clientId"].keys()
                for client in client_Ids:
                    request = ayu_socket.Intern_event_create_intern_request(client, ayu_socket.AYU_REQUEST, 0,
                                                                            ayu_socket.AYU_BREAK, "break", "")
                    try:
                        self.temanejo.remoteControl.rmc.socket_write(request)
                    except:
                        logger.error(
                            "No remote client was available at port %s" % self.temanejo.remoteControl.getActivePort())
            except:
                logger.error("No Clients were found in the KV Store")

            logger.info("Paused")

        self.currentlypaused = not self.currentlypaused

    def _copy_graph_and_add_attributes_to_graph(self, graph):
        datanodes = self.temanejo.remoteControl.datanodes
        new_graph = graph.copy()
        dataedges = self.temanejo.remoteControl.dataedges
        for node in new_graph.nodes():
            attributes = datanodes[int(node)].getProperties()
            for k, v in attributes.iteritems():
                node.attr[k] = v
        for edge in new_graph.edges(keys=True):
            new_graph.delete_edge(edge[0], v=edge[1], key=edge[2])

            attributes = dataedges[int(edge[-1])].getProperties()
            # for k, v in attributes.iteritems():
            #     edge = edge + ((k,v))
            new_graph.add_edge(edge[0], edge[1], edge[2])
        return new_graph
