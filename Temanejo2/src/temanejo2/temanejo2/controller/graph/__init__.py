'''!
@brief In this package all connections from the graph to the outer world are controlled

There are basicly two types of connections to the outside:
- The Userconnection  \n
All the modifications caused by events provoked from the user
- The Remoteconnection \n
All the modifications caused by events provoked from the remote Application.\n
In this part of the package also the communication to the remote Application will be defined
as well as the data parsing from the remote Application




@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
