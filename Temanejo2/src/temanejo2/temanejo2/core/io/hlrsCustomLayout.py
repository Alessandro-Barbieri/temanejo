'''!
@brief The layout Module for Temanejo2

@author: Steffen Brinkmann, brinkmann@hlrs.de
@author: Maik Figura
@contact temanejo@hlrs.de
@copyright  (C) 2015, HLRS, University of Stuttgart
            This software  is published under the terms of the BSD license.
            See the LICENSE file for details.import logging
'''
import logging

import networkx as nx

from operator import xor, attrgetter
import temanejo2.temanejo2.lib as lib
import temanejo_graph

logger = logging.getLogger(__name__)
line_separator = '----------------------------------------' \
                 '---------------------------------------'


class HlrsCustomLayout(temanejo_graph.Graph):
    def __init__(self):
        temanejo_graph.Graph.__init__(self)

        # separate nodes by a constant amount
        self.node_separation = lib.configuredValues.DEFAULT_NODESIZE * \
                               lib.configuredValues.DEFAULT_SEP_FACTOR
        # qt's origin is top left, gtks isnt
        self.coord_direction = 1 if lib.configuredValues.DEFAULT_COORD_DIRECTION == 1 else -1
        self.global_tasks_in_first_row = False

    # #
    #   seperates a graph with weakply connected subgraphs
    #   into seperate graphs
    #       @return independent_graphs a list with independent graphs
    def __separate_independent_graphs(self):
        self.dict_of_indep_graphs = dict()
        for index, graph in enumerate(nx.weakly_connected_component_subgraphs(self)):

            # get scope of the graph here
            #print graph.nodes()[0]  # This is the node which is queried in order to optain the nesinglevel

            scope_of_graph = self.temanejo.remoteControl.datanodes[graph.nodes()[0]].getProperties()['scope']
            # Check if there is already an independendant graph in the scoping level and if append the new one
            if scope_of_graph in self.dict_of_indep_graphs.keys():
                self.dict_of_indep_graphs[scope_of_graph].append(graph)
            # Else create a new entry in the dict.
            else:
                self.dict_of_indep_graphs[scope_of_graph] = [graph]

            #TODO  Afterwards the caller need to be rebuilded so the call the items of the 2 dimensional list then and not anymore the values of the dict as currently done...



    ##
    #   Layout trivial graphs, with only one or two nodes.
    def _layout_one_or_two_nodes_graph(self, g, offsets=(0,0)):
        logger.debug("layouting a simple (1 or 2 nodes) graph.")
        
        for i_n, n in enumerate(sorted(g.nodes())):
            x = offsets[0] * self.node_separation
            y = (-i_n + offsets[1]) * self.node_separation

            # multiply because gtk and qt have different
            # coordinate-systems
            self.add_node(n, pos=[x, y * -1])

    ##
    #   This method adds nodes with a calculated x and y position to
    #   the graph. See this as a final step when layouting
    def build_coordinates_for_layout(self, independent_graph, offsets=(0, 0)):
        # build layout
        offset = 1 if only_none_in_lev_zero(independent_graph) else 0

        for l_i in xrange(independent_graph.gld.number_of_levels):
            # Why are there None's here in .nodes_in_level[l_i]? -> shifts graph horizontally
            for i,n in enumerate(independent_graph.gld.nodes_in_level[l_i]):
                if n is None:  # or isinstance(n, str):
                    continue
                x = (i + offsets[0]) * self.node_separation
                y = (-l_i + offset + offsets[1]) * self.node_separation
                #layout_tmp[n] = (x, y)
                #logger.debug("updating layout")
                self.add_node(n, pos=[x, y * -1])

    ##  When minimizing edge crossings, gaps can appear. To avoid this
    #   before the final calculation of node positions, it is adviced
    #   to minimize edge crossings and look out for any gaps and eventually
    #   fill them to create a compact graph.
    def minimize_crossings_and_swap_nones(self, independent_graph, times,
                                          runs):
        for k in xrange(times):
            # swap nodes
            minimize_edge_crossing(independent_graph, self, runs)

            # swap nodes with None neighbours
            swap_nodes_with_none_neighbours(independent_graph, runs)

            # if global tasks is true, pull them to first row
            if self.global_tasks_in_first_row is True:
                move_global_tasks_up(independent_graph)

    ##
    #   You can call do_layout whenever you want a layout to be built.
    #   Layouts idea is to do all neccessary steps to produce a viewable
    #   layout using nodes previously added to the graph object.
    def do_layout(self):
        import time

        t0 = time.time()
        # is the graph empty, trivial case
        # if a graph is empty, early out is an option, thus
        #   check graphs if graphs are empty
        if is_graph_empty(self):
            logging.info("graph is empty")
            return None

        # graph is not empty, start layouting
        #logging.info("graph has %s nodes", self.number_of_nodes())

        # determine number of independent graphs
        #logger.info("graph has %s independent graphs",
        #            get_number_of_independent_graphs(self))

        #logging.info("start layouting graph")
        # for each independent graph do layouting
        # TODO: maybe building an independent_graph object makes sense
        self.__separate_independent_graphs()

        for independent_graph_list in self.dict_of_indep_graphs.values():
            for independent_graph in independent_graph_list:
                #logger.debug(line_separator)
                #logging.debug('current graph: %s', independent_graph.nodes())

                # case for one or two nodes
                if independent_graph.number_of_nodes() <= 2:
                    # layout/levelling for this graph is trivial, thus
                    # continue with next independent_graph iteration
                    continue

                # from this point on we are sure subgraph > 2 nodes
                # layout is complex, thus make sure graph data is empty
                independent_graph.gld.clear_data()

                # fill predecessors_of_node, successors_of_node etc.
                # for each node get predecessors and successors
                # for each node determine if single or multi dependency node
                # determine for each nodes neighbours if single or multi dep node
                #logging.debug("filling predecessors & successors of node.")
                for n in independent_graph:
                    set_node_dependencies(independent_graph, n)

                    #logger.debug('%s are single dependency neighbours of node %s',
                    #             independent_graph.gld.single_dep_neighbours[n], n)
                    #logger.debug('%s are multi dependency neighbours of node %s',
                    #             independent_graph.gld.multi_dep_neighbours[n], n)

                #logger.debug('graph has %s single dependency nodes',
                #             independent_graph.gld.single_dep_nodes)
                #logger.debug('graph has %s single dependency nodes',
                #             independent_graph.gld.multi_dep_nodes)

                #logger.debug(line_separator)

                # create subgraph with multi dependency nodes only
                subgraph = independent_graph.subgraph(independent_graph.gld.multi_dep_nodes)
                #logging.debug('subgraph with multi dependency nodes only: %s',subgraph.nodes())

                # for each multidependency subgraph do arrange its nodes in levels
                # TODO: WHY!
                #logger.debug("finding maximum level of predecessors of node in multi-dependency subgraph")
                arrange_subgraph_nodes_in_levels(independent_graph, subgraph)

                # center levels
                #logger.debug(line_separator)
                #logger.debug("centering nodes in each level")
                center_nodes(independent_graph)
                #logger.debug("centered nodes to %s",independent_graph.gld.nodes_in_level)
                #logger.debug(line_separator)

                # fill index_of_node
                # for each node determine the index it has in each sublist
                # example: [[None, None, 1, None], [None, 2, 3, 4]]
                # would output {1: 2, 2: 1, 3: 2, 4:3}
                # for each level (which could contain multiple nodes)
                for level in independent_graph.gld.nodes_in_level:
                    independent_graph.gld.index_of_node.update(
                        get_index_of_nodes_in_level(level))
                #logger.debug("updated index of nodes to %s",
                #             independent_graph.gld.index_of_node)

                #logger.debug(line_separator)

                # minimize edge crossings from level to suc_level
                #logger.debug("minimizing edge crossings")
                minimize_edge_crossing(independent_graph, subgraph, 1)

                #logger.debug(line_separator)

                # swap nodes with none neighbours
                #logger.debug('swapping nodes with their None neighbours if mean '
                #             'index of predecessors')
                swap_nodes_with_none_neighbours(independent_graph, 2)

                #logger.debug(line_separator)

                # sort in single dependency nodes
                fill_in_single_dep_nodes(independent_graph)

                #logger.debug(line_separator)

                # place single dependencey nodes
                place_single_dep_nodes(independent_graph)

                #logger.debug(line_separator)

                # center levels
                center_nodes(independent_graph)

                #logger.debug(line_separator)

                # fill g.gld.index_of_node
                # copy values from nodes_in_level
                fill_index_of_node(independent_graph)

                #logger.debug(line_separator)

                self.minimize_crossings_and_swap_nones(independent_graph, 10, 2)

                #logger.debug(line_separator)


        for k, independent_graph_list in self.dict_of_indep_graphs.iteritems():
            for independent_graph in independent_graph_list:

                for i in range(get_max_nodes_in_level(independent_graph)):

                    colIsNone=True
                    for l_i in xrange(independent_graph.gld.number_of_levels):
                        if independent_graph.gld.nodes_in_level[l_i][0] is not None:
                            colIsNone=False


                    if colIsNone is True:
                        for l_i in xrange(independent_graph.gld.number_of_levels):
                            del independent_graph.gld.nodes_in_level[l_i][0]


        # need to arrange subgraph relative to each other;
        # for now, sort them by number of tasks

        # independent_graphs = sorted(independent_graphs, key=attrgetter("number_of_nodes"))
        # assign coordinates to nodes in subgraphs
        # until be get barriers we arrange all graphs vertically
        # (we do not have info on width yet anyway ;)
        offset_x = 0
        offset_y = 0
        min_separation = 1

        #TODO == HERE THE SCOPING ID IS NEEDED in ORDER TO Check if graph is in uppermost scope or not ==
        #TODO == Only for the uppermost scope shifts of the independent graphs are nee==
        # TODO == Begin ==
        # for independent_graph_list in self.dict_of_indep_graphs.values():
        for k, independent_graph_list in self.dict_of_indep_graphs.iteritems():
            for independent_graph in independent_graph_list:
                if independent_graph.number_of_nodes() <= 2:
                    # if int(k) > 0:
                    #     self._layout_one_or_two_nodes_graph(independent_graph, offsets=(0, 0))
                    #     continue

                    # do short-cut for small graphs
                    self._layout_one_or_two_nodes_graph(independent_graph, offsets=(offset_x, offset_y))
                    # determine new offsets
                    offset_x += min_separation
                    offset_y -= 0
                else:
                    # if int(k) > 0:
                    #     self.build_coordinates_for_layout(independent_graph, offsets=(0, 0))
                    #     continue

                    self.build_coordinates_for_layout(independent_graph, offsets=(offset_x, offset_y))
                    # determine new offsets
                    offset_x += min_separation + get_max_nodes_in_level(independent_graph) - 1
                    offset_y -= 0
        # TODO == END ==

##
#   Decide whether there is no node in the very first level of our graph
#   @param g the graph
#   @returns True if there are only 'Nones' in the first level
def only_none_in_lev_zero(g):
    return g.gld.nodes_in_level[0].count(None) == len(g.gld.nodes_in_level[0])


##
#   Move all tasks, marked as global up in the graph. We want them
#   aligned in the first row.
#   param g the graph
def move_global_tasks_up(g):
    for n in g.nodes():
        # node isnt already in level 0
        if g.gld.level_of_node[n] != 0:
            # node has predecessors
            if len(g.predecessors(n)) == 0:
                g.gld.nodes_in_level[g.gld.level_of_node[n]].remove(n)
                g.gld.nodes_in_level[0].append(n)
                g.gld.level_of_node[n] = 0
    # for all nodes in level zero
    for n in g.gld.nodes_in_level[0]:
        if n is not None:
            # set the nodes index right
            g.gld.index_of_node[n] = g.gld.nodes_in_level[0].index(n)


# #
#   Add a level to a graph. If needed, also append the new level to graph.
#       @param g the graph to which the level and node are added
#       @param node the node to be added
#       @param next_level the level to be added
def add_node_to_level_in_graph(g, node, next_level):
    # next level is currently empty, so add another level to graph
    if next_level >= g.gld.number_of_levels:
        g.gld.number_of_levels += 1
        # append empty next level list
        g.gld.nodes_in_level.append([])

    # add level to graph
    g.gld.nodes_in_level[next_level].append(node)
    g.gld.level_of_node[node] = next_level


##
#   Remove a node from level.
#       @param g graph of which the node is removed
#       @param node the node to be removed
def remove_node_from_level(g, node):
    g.gld.nodes_in_level[
        g.gld.level_of_node[node]].remove(node)


##
#   Alignment helper, to vertically align subgraphs in graph.
#   Each multi-dependency subgraph node is aligned. If a node has no
#   successors or predecessors it will get skipped
#       @param g the graph
#       @param subgraph the multi-dependency subgraph
#       @param up if True upwards alignment is used, else downwards
def align_vertically(g, subgraph, up=False):
    if up:
        for level in reversed(g.gld.nodes_in_level):
            # for each node in level
            # move node above its successors
            for node in level:
                # successors exist
                if len(subgraph.successors(node)) is not 0:
                    # find minimum level of successors
                    # which one is nearest
                    min_succ_level = find_min_level_of_succ_of_node(
                        subgraph.successors(node), g)
                    # if node is not above minimal successor
                    if g.gld.level_of_node[node] is not min_succ_level - 1:
                        # put node one level above successor
                        next_level = min_succ_level - 1
                        # remove node from current level and add it to other lvl
                        remove_node_from_level(g, node)
                        add_node_to_level_in_graph(g, node, next_level)
    else:
        for level in g.gld.nodes_in_level:
            # move node below its predecessors
            for node in level:
                if len(subgraph.predecessors(node)) is not 0:
                    # find maximum level of predecessors
                    max_pred_level = find_max_level_of_pred_of_node(
                        subgraph.predecessors(node), g)

                    if g.gld.level_of_node[node] is not max_pred_level + 1:
                        # put node one level below
                        next_level = max_pred_level + 1

                        # next level is currently empty, so add another level to graph
                        remove_node_from_level(g, node)
                        add_node_to_level_in_graph(g, node, next_level)


##
#   Initializes various fields needed for graph layouts related to
#   dependencies of a node n. See GraphData Class for descriptions.
#   List of fields: single_dep_neighbours
#                   multi_dep_neighbours
#                   single_dep_nodes
#                   multi_dep_nodes
#       @param g a graph
#       @param n a node of graph g
def set_node_dependencies(g, n):
    # initialize empty neighbours for next node n
    g.gld.single_dep_neighbours[n] = []
    g.gld.multi_dep_neighbours[n] = []

    # TODO see whats faster, using pre suc directly
    #g.graph_data.predecessors_of_node[n] = g.predecessors(n)
    #g.graph_data.successors_of_node[n] = g.successors(n)

    # save dependency status for further usage
    if is_node_single_dependency(g, n):
        g.gld.single_dep_nodes.append(n)
    else:
        g.gld.multi_dep_nodes.append(n)

    # is node a single or multi dependency node
    if g.gld.number_of_dependencies_of_node == 1:
        g.gld.single_dep_nodes.append(n)
    elif g.gld.number_of_dependencies_of_node > 1:
        g.gld.multi_dep_nodes.append(n)

    # is a neighbour node x a single or multi dependency node
    for x in g.predecessors(n) + g.successors(n):
        if is_node_single_dependency(g, x):
            g.gld.single_dep_neighbours[n].append(x)
        else:
            g.gld.multi_dep_neighbours[n].append(x)


##
#   What is the furthest away predecessor level of a node
#   @param predecessors_of_node a list of all predecessors of a node
#   @param the graph
#   @returns the maximal predecessor level of a node
def find_max_level_of_pred_of_node(predecessors_of_node, g):
    # find maximum level of predecessors
    max_pred_level = 0
    for node in predecessors_of_node:
        max_pred_level = max(max_pred_level,
                             g.gld.level_of_node[node])
    return max_pred_level


##
#   What is the furthest awaya successor level of a node
#   @param successors_of_node a list of all successors of a node
#   @param g the graph
#   @returns the maximal successor level of a node
def find_min_level_of_succ_of_node(successors_of_node, g):
    # mind minimum level of successors
    min_succ_level = g.gld.number_of_levels
    for node in successors_of_node:
        min_succ_level = min(min_succ_level,
                             g.gld.level_of_node[node])
    return min_succ_level


##  Arrange all nodes of subgraph in levels. Sort of flattening them out
#   in their subgraph context.
#   @param g the graph
#   @param subgraph the designated subgraph of g
def arrange_subgraph_nodes_in_levels(g, subgraph):
    # arrange all nodes of subgraph in levels
    # pre step before arranging them vertically
    #logger.debug('arranging subgraph nodes vertically')
    for node in nx.topological_sort(subgraph):
        # find the maximum level of all predecessors of a node in
        # multi-dependency subgraph
        #logger.debug('finding maximal level of predecessor of node')
        max_pred_level = find_max_level_of_pred_of_node(
            subgraph.predecessors(node), g)
        #logger.debug("maximal predecessor level of node %s is %s", node, max_pred_level)
        # put node one level below
        next_level = max_pred_level + 1
        # add new node to graph
        add_node_to_level_in_graph(g, node, next_level)

    # arrange vertically:
    # on each level move nodes up as far as possible
    #logger.info('aligning vertically upwards')
    align_vertically(g, subgraph, up=True)
    # on each level move nodes down as far as possible, depending
    # on their predecessors
    #logger.info('aligning vertically downwards')
    align_vertically(g, subgraph, up=False)


##
#   Get all successors of a given node. Using l_i and max_depth one can specify
#   a range of the returned successors. The range is build by a upper limit
#   which works as start index and an lower limit which works as end index.
#   @param g the graph
#   @param subgraph the subgraph of the graph
#   @param node the node of which to get successors
#   @param l_i upper limit
#   @param max_depth lower limit
def get_successors(g, subgraph, node, l_i, max_depth):
    return [x for x in subgraph.successors(node)
            if g.gld.level_of_node[x] - l_i < max_depth]


##
#   Determine whether swapping two nodes would lower the cross_count.
#   @param cross_count_swap the cross count respecting a swap
#   @param cross_count the current cross count
def swap_minimizes_crossing(cross_count_swap, cross_count):
    return True if cross_count_swap < cross_count else False


##  Minimize edge crossings between to levels
#   for a node and its left neighbour, check whether they should get
#   switched to minimize edge crossings with connections to next level
#   If a left node has a successor that has a greater index than the
#   current node, then edge crossing is happening. Switch left and node
#   to test whether this would result in less crossings, and if so,
#   switch them permanently
#   @param g The graph where to minimize crossings
#   @param subgraph multi-dependency subgraph
def minimize_edge_crossing(g, subgraph, runs):
    # run this several times to acchive good results
    for i in xrange(runs):  # 6
        l_i = 0  # list index
        for nodes_in_level in g.gld.nodes_in_level:
            # skip index 0, as it is always 'None'
            for node in nodes_in_level[1:]:
                if node is None:
                    continue
                # from current node in level, get left neighbour
                left = nodes_in_level[g.gld.index_of_node[node] - 1]
                if left is None:
                    continue
                # get successors in next level of current node
                suc = get_successors(g, subgraph, node, l_i, 2)

                # get next level successors of left neighbour
                left_suc = get_successors(g, subgraph, left, l_i, 2)

                cross_count, cross_count_swap = swap_counts(g, suc,
                                                            left_suc)

                # swap nodes if it results in less crossings
                if swap_minimizes_crossing(cross_count_swap, cross_count):
                    #logger.debug("swapping node %s with %s", left, node)
                    nodes_in_level[g.gld.index_of_node[node]] = left
                    nodes_in_level[g.gld.index_of_node[left]] = node

                    # use classic xor swapping
                    swap_index_of_nodes(g, left, node)
            l_i += 1


##
#   What is the index of a node in its specific level?
#   @param g the graph
#   @param n the node
#   @returns the index of node in level list
def get_index_of_node_in_level(g, n):
    return g.gld.nodes_in_level[
        g.gld.level_of_node[n]].index(n)


##
#   Swap nodes with their neighbour either on the left or on the right.
#   @param g the graph
#   @param i_n the level of the nodes
#   @param the level array
#   @param node the node to swap
#   @param to_right swap with the right or else with the left node
def swap_node_with_neighbour(g, i_n, level, node, to_right=True):
    #TODO: use reference on level list
    level[i_n] = None
    if to_right:
        level[i_n - 1] = node
        g.gld.index_of_node[node] = i_n - 1
    else:
        if i_n + 1 >= len(level):
            level.append(node)
        else:
            level[i_n + 1] = node
        g.gld.index_of_node[node] = i_n + 1


##
#   Swaps a node with none neighbours, to relax their relative position in a
#   level. Depending on runs this is done several times. See it as a sort of
#   relaxation.
#   @param g the graph
#   @param runs how many times should we relax
def swap_nodes_with_none_neighbours(g, runs):
    # swap nodes with None neighbours
    for i in xrange(runs):  # 6
        break_flag = True
        # traverse all nodes in level
        for level in g.gld.nodes_in_level:
            # for half of the nodes
            for kk in xrange(len(level) / 2):
                break_flag = True

                for node in level:
                    if node is None:
                        continue

                    # node is not none, get index of node
                    # get left neighbour of node and right neighbour of node
                    i_n = level.index(node)
                    if i_n == 0:  # first element
                        left = None
                    else:
                        left = level[i_n - 1]

                    if i_n == len(level) - 1:  # last element
                        right = None
                    else:
                        right = level[i_n + 1]

                    # if none of them is None, continue with next iteration
                    if left is not None and right is not None:
                        continue

                    mean_neighbour_index = 0  # nodes_tmp.index(n)
                    count = 0.0
                    # build mean based on neighbour indices
                    for x in g.gld.multi_dep_neighbours[node]:
                        if abs(g.gld.level_of_node[node] -
                                g.gld.level_of_node[x]) < 2:
                            mean_neighbour_index += get_index_of_node_in_level(
                                g, x)
                            count += 1
                    if count == 0:  # we did never find candidates
                        continue

                    mean_neighbour_index /= count

                    # swap nodes for being closer to
                    # mean_neighbour_index swap to right
                    if mean_neighbour_index < i_n - .5 and left is None:
                        break_flag = False
                        swap_node_with_neighbour(g, i_n, level, node,
                                                 to_right=True)
                    # swap to left
                    elif mean_neighbour_index > i_n + .5 and right is None:
                        break_flag = False
                        swap_node_with_neighbour(g, i_n, level, node,
                                                 to_right=False)

                if break_flag is True:
                    break

        if break_flag is True:
            break


##
#   How many nodes are in a specific level.
#   @param g the graph
#   @param n the node
#   @param shift relative level to node
#   @returns amount of nodes
def get_len_of_nodes_in_level(g, n, shift=0):
    return len(get_nodes_in_level(g, n, shift))


##  Get all nodes of a specific level
#   @param g the current graph
#   @param n the node of which one wants all level neighbours
#   @param shift=0 shift option to specify the level relative to the node
def get_nodes_in_level(g, n, shift=0):
    return g.gld.nodes_in_level[g.gld.level_of_node[n] + shift]


##
#   Single dependency nodes should be in standalone levels. So, find all
#   single dependeny nodes and move them up, as far as possible and then
#   move them down as far as possible.
#   @param g the graph
def fill_in_single_dep_nodes(g):
    import copy

    for l in g.gld.nodes_in_level:
        # loop over a shallow copy, as we modify the original list
        for n in copy.copy(l):
            if n is None or n in g.gld.single_dep_nodes:
                continue
            move_single_dep_nodes_up(g, n)

    for l in reversed(g.gld.nodes_in_level):
        for n in copy.copy(l):
            if n is None:
                continue
            move_single_dep_nodes_down(g, n)


##
#   Move single dependency nodes up if there is a free spot.
#   @param g the graph
#   @param n the node
def move_single_dep_nodes_up(g, n):
    for p in g.predecessors(n):
        if p in g.gld.single_dep_nodes:
            i_n = get_nodes_in_level(g, n).index(n)
            if is_spot_in_level_free(g, n, i_n, shift=-1):
                get_nodes_in_level(g, n, shift=-1)[i_n] = p
            else:
                get_nodes_in_level(g, n, shift=-1).insert(i_n, p)
                get_nodes_in_level(g, n, shift=0).insert(i_n, None)

            g.gld.level_of_node[p] = g.gld.level_of_node[n] - 1


##
#   Move single dependency nodes down, if there is a free spot in the level
#   below.
#   @param g the graph
#   @param n the node
def move_single_dep_nodes_down(g, n):
    for s in g.successors(n):
        if s in g.gld.single_dep_nodes:
            i_n = get_nodes_in_level(g, n).index(n)
            if g.gld.level_of_node[n] >= g.gld.number_of_levels - 1:
                g.gld.nodes_in_level.append([])
                g.gld.number_of_levels += 1
            if is_spot_in_level_free(g, n, i_n, shift=1):
                get_nodes_in_level(g, n, shift=1)[i_n] = s
            else:
                get_nodes_in_level(g, n, shift=1).insert(i_n, s)
                get_nodes_in_level(g, n).insert(i_n, None)

            g.gld.level_of_node[s] = g.gld.level_of_node[n] + 1

##
#   Relax vertically. Whenever a single dependency node is found, and a spot
#   below or above is free, we can move this node.
#   @param g the graph
def place_single_dep_nodes(g):
    for n in reversed(g.gld.single_dep_nodes):
        # get predecessors or successors of node
        if len(g.successors(n)) is not 0:
            x = g.successors(n)[0]
        else:
            x = g.predecessors(n)[0]

        # get index of node
        i_n = get_nodes_in_level(g, n).index(n)
        # get index of suc or pred
        i_x = get_nodes_in_level(g, x).index(x)
        get_nodes_in_level(g, n)[i_n] = None

        # put sd node directly above or below next / previous node
        if is_spot_in_level_free(g, n, i_x):
            # move n, because there is space
            get_nodes_in_level(g, n)[i_x] = n

        # put sd node left above or below next / previous node
        elif is_spot_in_level_free(g, n, i_x - 1):
            get_nodes_in_level(g, n)[i_x - 1] = n

        # put sd node right abolve or below next / previous node
        elif is_spot_in_level_free(g, n, i_x + 1):
            get_nodes_in_level(g, n)[i_x + 1] = n

        else:
            get_nodes_in_level(g, n).insert(i_x, n)


##
#   Is a spot free in a specific level in the same index as the node is. Free
#   spot means "None" spots.
#   @param g the graph
#   @param n the node
#   @param index the index of the node
#   @returns True of there is a free spot
def is_spot_in_level_free(g, n, index, shift=0):
    return True \
        if (get_len_of_nodes_in_level(g, n, shift=shift) > index and
            get_nodes_in_level(g, n, shift=shift)[index] is None) else False


##  Calculate spacing  before and after nodes for each level. Centering
#   of nodes in level is done inplace.
#   Thius will create a a list in the form of [ [], [], [] ] which contains
#   items in the schema of:
#   ([None] * spacing front) + nodes_in_level + ([None] * spacing back)
#       @param independent_graph a independent graph, could be a subgraph
def center_nodes(independent_graph):
    max_level_len = get_max_nodes_in_level(independent_graph)
    independent_graph.gld.width = max_level_len
    for l in independent_graph.gld.nodes_in_level:
        curr_level_len = len(l)
        l[0:] = [None] * ((max_level_len - curr_level_len) / 2 + 1) + l + \
                [None] * ((max_level_len - curr_level_len) / 2)

##
#   Fill index of node list.
#   @param g the graph
def fill_index_of_node(g):
    for l in g.gld.nodes_in_level:
        i_n = 0
        for n in l:
            if n is not None:
                g.gld.index_of_node[n] = i_n
            i_n += 1


##  Determines whether a given graph is empty
#       @param g a graph, possibly empty
#       @return True if graph is empty
def is_graph_empty(g):
    return False if g.number_of_nodes() != 0 else True


##
# This determines if a specific node of a graph g is a single dependency node
# or a multi dependency node.
#       @param g a graph with nodes
#       @param n a specific node of the graph
#       @return True if node has only a single dependency
def is_node_single_dependency(g, n):
    # is a given node n a single dependency node
    if len(g.predecessors(n)) + len(g.successors(n)) == 1:
        return True
    elif len(g.predecessors(n)) + len(g.successors(n)) > 1:
        return False


##
#   Get maximum level (level containing the most nodes).
#   @param g the graph
#   @returns level with the maximum amount of nodes
def get_max_nodes_in_level(g):
    return max([len(nodes) for nodes in g.gld.nodes_in_level])

##
#   This method determines the number of independent graphs. Essentially
#   this is only counting the number of items in an iterable (like [[],[],...])
#       @param independent_graphs a list of independent graphs
#       @return the amount/number of independent graphs
def get_number_of_independent_graphs(independent_graphs):
    return len(independent_graphs)


##  Build a list of tuples, where each tuple is a node id and its index in
#   given level.
#   build list of tuples [(1,2), (2,3)]
#   meaning node 1 is on index 2 node 2 is on index 3
#       @param level containing nodes
#       @return list of tuples
def get_index_of_nodes_in_level(level):
    # for each node in a level
    tlist = []
    # build list of tuples [(1,2), (2,3)]
    # meaning node 1 is on index 2 node 2 is on index 3
    [tlist.append((node, level.index(node))) for node in level
     if node is not None]

    return tlist


##
#   When swapping nodes, also the index of those nodes has be swapped.
#   This method implements a xor (no temp var) based swap of these indices.
#   @param g the graph
#   @param a node a
#   @param b node b
def swap_index_of_nodes(g, a, b):
    g.gld.index_of_node[a] = xor(
        g.gld.index_of_node[a], g.gld.index_of_node[b])
    g.gld.index_of_node[b] = xor(
        g.gld.index_of_node[a], g.gld.index_of_node[b])
    g.gld.index_of_node[a] = xor(
        g.gld.index_of_node[a], g.gld.index_of_node[b])


##
#   Count all swaps. One time with a possible swap one time without.
#   @param g the graph
#   @param suc successor of node
#   @param left_suc the left successor of node
#   @returns cross_count, cross_count_swap where cross_count only considers
#   the crosses and cross_count_swap considers the crosses with swapped nodes
def swap_counts(g, suc, left_suc):
    cross_count = 0
    cross_count_swap = 0
    for s in suc:
        for x in left_suc:
            # if indices of successors overlap we have a cross
            # could possibly be swapped
            if g.gld.index_of_node[x] > g.gld.index_of_node[s]:
                cross_count += 1
            elif g.gld.index_of_node[x] < \
                    g.gld.index_of_node[s]:
                cross_count_swap += 1
    return cross_count, cross_count_swap
