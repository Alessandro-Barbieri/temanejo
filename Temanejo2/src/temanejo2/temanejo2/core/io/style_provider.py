import logging

import base_style
from hlrs_style import HLRSStyle

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


##
#     StyleProvider is a handler for given layout styles. It splits and
#     imports a given module and class string
class StyleProvider(object):

    ##
    #   Register registers a given module / class String by importing the
    #   module and instantiating the class object.
    #   @param layout_style module and class path to designated layout style
    #   @returns a class object of the given class
    @staticmethod
    def register(layout_style='temanejo2.temanejo2.core.io.hlrs_style.HLRSStyle',temanejo=None):
        
        #This code can be used to dynamicaly load different styles
        #mod, klass = split_module_and_class(layout_style)
        #import importlib
        #my_modu = importlib.import_module(mod)
        #class_obj = getattr(my_modu, klass)
        #ls = class_obj()
        #if not issubclass(class_obj, base_style.BaseStyle):
        #    raise RuntimeError("%s is not a subclass of BaseStyle", klass)
        #return ls

        return HLRSStyle(temanejo)


##
#   Split a given module and class string into a string that contains
#   the module path and the class itself
#   @param layout_style a string to split
#   @returns module path and class as strings
def split_module_and_class(layout_style):
    splits = layout_style.split('.')
    klass = splits[len(splits) - 1]
    mod = '.'.join(splits[:-1])
    return mod, klass