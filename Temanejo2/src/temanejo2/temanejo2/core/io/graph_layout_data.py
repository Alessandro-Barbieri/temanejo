##
#   GraphData is used in conjunction with a graph class. It defines
#   a specific set of data objects, related to a graph, thus it usually
#   doesnt make sense to use this independently. In our case,
#   this is an attribute of SteffenCustomLayout.
class GraphData(object):

    ##
    #   Constructor, which calls a helper method to instantiate an
    #   GraphData object.
    def __init__(self):
        self.__init_data()

    ##
    #   A clearstructor, to remove any data from a previously used object.
    def clear_data(self):
        self.__init_data()

    ##
    #   This is a private helper method, called whenever a GraphData object
    #   is instantiated. We use this to keep the constructor clean.
    def __init_data(self):
        # level for each node
        self.level_of_node = {}

        # index for each node
        self.index_of_node = {}

        # predecessors of each node
        #self.predecessors_of_node = {}

        # successors of each node
        #self.successors_of_node = {}

        # nodes in each level
        self.nodes_in_level = [[]]

        # total number of levels
        self.number_of_levels = 1

        # total width
        self.width = 1

        # list of neighbours with only one
        self.single_dep_neighbours = {}

        # list of neighbours with more than one
        self.multi_dep_neighbours = {}

        # dependency for each node
        self.single_dep_nodes = []

        # list of nodes with exactly one dependency
        self.multi_dep_nodes = []

        # amount of how many dependencies a node has
        self.number_of_dependencies_of_node = int()
