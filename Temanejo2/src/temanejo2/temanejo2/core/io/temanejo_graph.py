#****h* Temanejo/temanejo_graph
# NAME
#   temanejo_graph
#   - the graph module for Temanejo's task graph
# DESCRIPTION
#   temanejo_graph.py contains Graph, a graph data structure derived from the
#   MultiDiGraph (directed graph with multiple edges) structure from
#   NetworkX.
# PORTABILITY
#   The following modules are imported:
#   * networkx as nx
#   * temanejo_client_config as client_conf
#   * logging
# AUTHOR
#   Steffen Brinkmann, HLRS <brinkmann@hlrs.de>
# COPYRIGHT
#   (C) 2013-2014 HLRS, University of Stuttgart
#   temanejo_graph.py is published under the terms of the BSD license.
#******

VERBOSE_GRAPH = False

import logging

import networkx as nx

import graph_layout_data

#import temanejo_client_config as client_conf # for client dependent
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#****c* Temanejo_graph/Graph
# NAME
#   Graph - class containing the dependency graph of a task parallel
#   application
# DESCRIPTION
#   Graph implements a data structure representing a task dependency graph as
#   a directed acyclic graph. It is intented to be imported from Temanejo.
# DERIVED FROM
#   NetworkX.MultiDiGraph
# METHODS
#   __init__
#   add_dep
#   add_task
#   clear
#   distance
#   get_funcId
#   get_funcname
#   get_nodeId
#   get_taskId
#   get_threadId
#   set_funcname
#   set_status
#   set_threadId
#   __get_barrier_count # commented out, as it is not used
#   __get_highlight # commented out, as it is not used
# SOURCE



class Graph(nx.MultiDiGraph):

    def __init__(self):
        nx.MultiDiGraph.__init__(self)

        self.temanejo = None          # A Handler to the main_application in order to query the scope of the subgraph

        self.gld = graph_layout_data.GraphData()
        self.last_nodeId = 0          # last nodeId used. New tasks/nodes have
                                      # to use last_nodeId + 1
        self._taskId = {}             # task Id, used as label for nodes
        self._nodeId_of_taskId = {}   # node Id for every task. For reverse
                                      # lookup of self.taskId
        self._funcId = {}             # function Id
        self._threadId = {}           # thread Id
        self._status = {}             # 0: not queued, 1: queued, 2: running,
                                      # 3: finished, 4: failed
        #self.task_states = ["NQ", "Q", "R", "F", "X"] # must correspond to list above
        #self._funcName = properties.propFunc           # list of function
        # names
        self.addresses = {}           # list of memory addresses of
                                      # dependencies
        self.orig_addresses = {}      # list of original memory addresses of
                                      # dependencies (before renaming)
        self.blockedTasks = []        # tasks to be marked as blocked
        self.dependentTasks = {}      # tasks that depend on index task due to
                                      # user input
        self.criticalTasks = []       # tasks to be marked as critical
                                      # (prioritised)
        self._barrier_count = {}      # count of barriers before each task
        self.total_barrier_count = 0  # number of barriers in graph
        self._highlight = {}          # highlighted 1: neighbour, 2: clicked
        self.is_invalid = True        # True when graph has changed, decides
                                      # whether to generate new layout
        self.dict_of_indep_graphs = dict()

    def register_temanejo_to_graph(self, temanejo):
        self.temanejo = temanejo

    def clear(self):
        nx.MultiDiGraph.clear(self)

        self.last_nodeId = 0
        self._taskId = {}
        self._nodeId_of_taskId = {}
        self._funcId = {}
        self._threadId = {}
        self._status = {}
        self.addresses = {}
        self.orig_addresses = {}
        self.blockedTasks = []
        self.criticalTasks = []
        self.dependentTasks = {}
        self._barrier_count = {}
        self._highlight = {}
        self.is_invalid = True  # True when graph has changed

    def add_task(self, taskId, funcId, critical, barrier_count, timestamp,
                 filename='', linenumber=0):
#        logger.info("add_task %s %s %s %s %s %s %s" %
#                (str(taskId), str(funcId), str(critical), str(barrier_count),
#                str(timestamp), str(filename), str(linenumber)))

        self.last_nodeId += 1
        nodeId = self.last_nodeId

        self.add_node(nodeId, ts_add=timestamp)
        self._taskId[nodeId] = taskId
        self._nodeId_of_taskId[taskId] = nodeId
        self._funcId[nodeId] = funcId
        self._threadId[nodeId] = client_conf.no_thread_id
        self._status[nodeId] = 0  # not queued
        self._highlight[nodeId] = 0

        if critical > 0:
            self.criticalTasks.append(nodeId)

        self._barrier_count[nodeId] = barrier_count
        self.is_invalid = True

    def add_dep(self, taskId, parentId, address, original_address):
#        logger.info('add_edge(%s,%s)' %(str(parentId), str(taskId)))
        if ( taskId not in self._nodeId_of_taskId ):
            logger.warning("edge(%s,%s) cannot be created. "
                            "Node %s does not exist!" %
                            (str(parentId), str(taskId), str(taskId)))
            return
        if ( parentId not in self._nodeId_of_taskId ):
            logger.warning("edge(%s,%s) cannot be created. "
                            "Node %s does not exist!" %
                            (str(parentId), str(taskId), str(parentId)))
            return
        if ( (self._nodeId_of_taskId[taskId],self._nodeId_of_taskId[parentId])
                in self.edges()):
            logger.warning('edge (%s,%s) would create a trivial cycle.'
                            'It will be ignored.'
                            % (str(parentId), str(taskId)))
            return
        if self._nodeId_of_taskId[taskId] < self._nodeId_of_taskId[parentId]:
            logger.warning('edge (%s,%s) would possibly create a cycle.'
                            'It will be ignored.'
                            % (str(parentId), str(taskId)))
            return

        self.add_edge(self._nodeId_of_taskId[parentId],
                      self._nodeId_of_taskId[taskId],
                      addr=address,
                      orig_addr=original_address)

        if address not in self.addresses:
            self.addresses[address] = 0

        self.addresses[address] += 1

        if original_address not in self.orig_addresses:
            self.orig_addresses[original_address] = 0

        self.orig_addresses[original_address] += 1

        self.is_invalid = True

    def get_taskId(self, nodeId):
        return self._taskId.get(nodeId, None)

    def get_nodeId(self, taskId):
        return self._nodeId_of_taskId.get(taskId, None)

    def get_status(self, taskId):
        nodeId = self._nodeId_of_taskId[taskId]
        return self._status[nodeId]

    def set_status(self, taskId, status, timestamp):
        nodeId = self._nodeId_of_taskId[taskId]
        if status == 1:
            self.add_node(nodeId, ts_q=timestamp)
        elif status == 2:
            self.add_node(nodeId, ts_r=timestamp)
        elif status == 3:
            self.add_node(nodeId, ts_f=timestamp)
        elif status == 4:
            self.add_node(nodeId, ts_fail=timestamp)
        self._status[nodeId] = status

    def get_threadId(self, taskId):
        nodeId = self._nodeId_of_taskId[taskId]
        return self._threadId[nodeId]

    def set_threadId(self, taskId, threadId):
        nodeId = self._nodeId_of_taskId[taskId]
        self._threadId[nodeId] = threadId

    def get_funcId(self, taskId):
        nodeId = self._nodeId_of_taskId[taskId]
        return self._funcId[nodeId]

    def get_funcname(self, funcId):
        if funcId == client_conf.waiton_func_id:
            return "wait on task"
        return self._funcName[funcId]

    def set_funcname(self, funcId, buf):
        self._funcName[funcId] = buf

# not used, but kept for future use
#    def get_highlight(self, taskId):
#        nodeId = self._nodeId_of_taskId[taskId]
#        return self._highlight[nodeId]

# not used, but kept for future use
#    def __get_barrier_count(self, taskId):
#        nodeId = self._nodeId_of_taskId[taskId]
#        return self._barrier_count[nodeId]

    def distance(self, source, target):
        if target is None or source is None:
            return None
        try:
            return nx.shortest_path_length(self, source, target)
        except nx.NetworkXNoPath, msg:
            try:
                return -nx.shortest_path_length(self, target, source)
            except nx.NetworkXNoPath, msg:
                logger.warning(msg)
                return None
            except nx.NetworkXError, msg:
                logger.warning(msg)
                return None
        except nx.NetworkXError, msg:
            logger.warning(msg)
            return None

#******
