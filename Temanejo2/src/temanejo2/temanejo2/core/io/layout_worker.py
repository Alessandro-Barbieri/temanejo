'''!
@brief The Layout will be spawned in here

This class is responsible for layouting the displayed graph
This is done in a seperat thread and will only be done if the graph is marked as dirty

@author: Stefan Reinhardt
@contact info@temanejo.com
'''

__author__ = 'Stefan Reinhardt'

import logging

from PySide import QtCore
from PySide.QtCore import QThread as Thread
from threading import Lock
import time

logger = logging.getLogger(__name__)

class LayoutWorker(Thread, QtCore.QObject):
    def __init__(self, temanejo):
        super(LayoutWorker, self).__init__()
        ##@member self.isActive The flag for the readloop in the runmethod of the thread
        self.temanejo = temanejo
        self.isActive = False
        logger.debug("Initialized Layoutworker")

        self.update_lock = Lock()

        with self.temanejo.graph_is_dirty_lock:
            self.graphIsDirty = False

        with self.update_lock:
            self.updateIsDirty =time.time()

        self.force_relayout = False

        self.updatedTime=time.time()

        self.signalizer_to_update = _graph_updater_signalizer(self)
        self.signalizer_to_update.do_updates_on_graph_signal.connect(self.do_updates_on_graph)

    @QtCore.Slot()
    def do_updates_on_graph(self, currentGraph):
        with self.update_lock:
            ulock = self.updateIsDirty

        if ulock > self.updatedTime:
            self.updatedTime = time.time()
            t0 = time.time()
            
            self.temanejo.remoteControl.layoutCtrl.updateGraphModel(currentGraph)
            # Update QGraphicsView
            bounds = self.temanejo.remoteControl.layoutCtrl.bounds
            self.temanejo.remoteControl.layoutCtrl.graphModel.setSceneRect(QtCore.QRectF(bounds[0], bounds[1],
                                           2 * bounds[2], 2 * bounds[3]))
            self.temanejo.ui.temanejoGraph.setSceneRect(QtCore.QRectF(bounds[0], bounds[1],
                                                    2 * bounds[2], 2 * bounds[3]))
            self.temanejo.ui.temanejoGraph.viewport().update()
            # See disabling the button some lines above
            try:
                self.temanejo.zoomButtons[4].setEnabled()
            except:
                pass

            t1 = time.time()
            t=t1-t0
            logger.debug("All data on the graph was updated in %s"%t)


    @QtCore.Slot()
    def dirtyGraph(self, force_relayout = False):
        if (not self.temanejo.block_relayout) or force_relayout:
            with self.temanejo.graph_is_dirty_lock:
                self.graphIsDirty = True
            self.force_relayout = force_relayout
            logger.debug("Recieved new Items from remote")
        else:
            logger.debug("Recieved new Items from remote but relayout is currently blocked")
    def run(self):
        self.isActive = True
        inCount = 0
        while self.isActive:
            #Do layout
            with self.temanejo.graph_is_dirty_lock:
                grisd = self.graphIsDirty

            if grisd or self.force_relayout:
                inCount = 0
                routine = self.temanejo.createOnFlyLayoutCodeForLayoutAction()
                with self.temanejo.graph_is_dirty_lock:
                    self.graphIsDirty = False


                self.force_relayout = False
                try:
                    self.temanejo.zoomButtons[4].setDisabled()
                except:
                    pass

                currentGraph = self.temanejo.graphlayoutIO.getGraph()

                self.temanejo.graphlayoutIO.layoutGraph(currentGraph, routine=routine)


                with self.update_lock:
                    self.updateIsDirty = time.time()

                with self.temanejo.graph_is_dirty_lock:
                    if self.graphIsDirty == False:
                        self.signalizer_to_update.do_updates_on_graph_signal.emit(currentGraph)

                Thread.msleep(200)
                Thread.yieldCurrentThread()

            else:
                inCount += 1
                Thread.yieldCurrentThread()
                if(inCount > 50):
                    Thread.msleep(100)
                elif(inCount > 200):
                    Thread.msleep(200)

    def terminate(self):
        self.isActive = False

class _graph_updater_signalizer(QtCore.QObject):
    do_updates_on_graph_signal = QtCore.Signal(object)

    # def __init__(self, layout_worker):
    #     QtCore.QObject.__init__(self)
