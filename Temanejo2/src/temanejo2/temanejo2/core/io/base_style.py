import abc


class BaseStyle(object):
    """This metaclass is a blueprint for custom layout styles. Using this
    one can register (using :class:`StyleProfider`) a custom layout style.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        """Initializes the given style."""

    def getNodes(self):
        """Get all nodes."""

    def getNodePos(self, node_id):
        """Get position of a specific node
        :param node_id: An unique ID of the node
        """

    def getEdges(self):
        """Get all edges."""

    def addEdge(self, from_node, to_node):
        """Get an edge between two given nodes.
        :param from_node: Node above the edge
        :param to_node: Node below the edge
        """
