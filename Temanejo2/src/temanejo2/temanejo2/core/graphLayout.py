'''!
@brief This module provides a graph layout routine done via pygraphviz
       
A Graph layout Object is defined in here. \n
All graphlayouting will be done in here. It queries the information of the Graphitems
from the remoteController and fills the models for the TemanejoGraph. 
For layouting the pyGraphViz lib will be used  


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtCore

from temanejo2.temanejo2.core.items import EdgeItem, NodeItem

logger = logging.getLogger(__name__)


##
#  This Class defines a object with which the graph will be layouted.
#  It queries information from the Datanodes handles all the layouting,
#  maps the positions to the scene and finally fills the models for
#  the representation of the Graph in the TemanejoGraph view.
class GraphLayout(object):
    ##
    # Intitializes the Graphlayout object
    # @param temanejo: A handle to the main application
    # @param pgvgraph: A handle to a AGraph from the remote controller. 
    #                  This object has to be defined in the remotecontroller.
    #                  There all the connections have to be setted up, so that this 
    #                  object only has to layout the given graph
    # @param dataNodes: A handle to all dataNodes which hold all the real information and
    #                   properties of the tasks. Only needed to make a one on one connection
    #                   for all nodes which should be painted on the view and the tasks produced
    #                   from Ayudame or any given Backend 
    def __init__(self, temanejo, pgvgraph, dataNodes):
        ##@member A handle on the given PygraphViz Graph
        self.graph = pgvgraph
        ##@member A handle to the main Application
        self.temanejo = temanejo
        ##@member A handle to all dataNodes
        self.dataNodes = dataNodes
        ##@member A handle to the graphModel which will be filled
        self.graphModel = temanejo.graphModel
        
        # The maximum and minimum scene boundaries
        self.maxPos = [0, 0]
        self.minPos = [0, 0]
        
        ##@member This flag ensures that there is only one layouting Procedure active at once\n
        #         It needs to be set for threadsafety... 
        #         (This is just to be prepared for threading the layout not really in use now!)
        self.currentlyLayouting = False
        
        self.bounds = [0, 0, 0, 0]


    ##
    #   Setups all NodeItems for the Graphmodel
    #   Creates a NodeItem for all nodes in the graph and maps the position to the scene
    def setupNodes(self):
        for node in self.graph.nodes():
            self.setUpNode(node)
        self.mapPositionsToScene()
    ##
    #   SetUp a single node for the graph
    def setUpNode(self, node, currentGraph):
        #Get NodePosition and size
        nodePos = self.temanejo.graphlayoutIO.getNodePos(node, currentGraph)
        # Convert nodepositions
        nodePos = [float(nodePos[0]),float(nodePos[1])]
        #Get max and min of positions for mapping them later
        self.maxPos[0] = nodePos[0] if self.maxPos[0] < nodePos[0] else self.maxPos[0]
        self.maxPos[1] = nodePos[1] if self.maxPos[1] < nodePos[1] else self.maxPos[1]
        self.minPos[0] = nodePos[0] if self.minPos[0] > nodePos[0] else self.minPos[0]
        self.minPos[1] = nodePos[1] if self.minPos[1] > nodePos[1] else self.minPos[1]
        
        # ==== Setup Item ====
        # Check for datanode
        try:
            dataNode = self.dataNodes[int(node)]
        except:
            dataNode = None
            logger.warning("No data was found for node %s"%node)
        # Create Node Item
        node_item = NodeItem.NodeItem(self.temanejo, dataNode , int(node), 
                                      location=nodePos)

        try:
            node_item.setNodeLabel(text=str(dataNode.getProperty("taskId")))
        except:
            logger.debug("No according datanode was found the label will be set to nodeID")
            node_item.setNodeLabel(text=node)

        self.graphModel.setNode(node_item.getId(), node_item)
        return node_item
    
    ##
    #   Setups all EdgeItems for the Graphmodel
    #   Creates a EdgeItem for all edges in the graph and maps the position to the scene
    def setupEdges(self):
        for edge in self.graph.edges():
            self.setupEdge(edge)
            

    ##
    #  Creates a single edgeitem
    def setupEdge(self, edge, edgeId, origID):
        #Register dependencies to Node as Child and Parent
        par_node = self.graphModel.getNode(int(edge[0]))
        child_node = self.graphModel.getNode(int(edge[1]))
        if par_node is None:
            return None
        if child_node is None:
            return None
        par_node.getInOutDependencies()[1].append(int(edge[1]))
        child_node.getInOutDependencies()[0].append(int(edge[0]))

        #Setup Edge
        edge_item = EdgeItem.EdgeItem(self.graphModel.temanejo, edgeId, origID, parent=None)
        #Modify start/endLocation in order to paint it to the center of the node
        start_loc = par_node.getNodeLocation()
        end_loc   = child_node.getNodeLocation()
        start_loc = [start_loc[0] + par_node.getNodeSize()/2, 
                     start_loc[1] + par_node.getNodeSize()/2]
        end_loc   = [end_loc[0] + child_node.getNodeSize()/2,
                     end_loc[1] + child_node.getNodeSize()/2]
         
        edge_item.setStartLocation(start_loc)
        edge_item.setEndLocation(end_loc)
        self.graphModel.setEdge(edge_item.getId(), edge_item)  
        return edge_item

    ##
    #   Updates the Graphmodel after any new layouting...
    def updateGraphModel(self, currentGraph):
        self.updateGraphItems(currentGraph)
        #logger.debug("New Items added")
        self.updateNodePositions(currentGraph)
        #logger.debug("Node positions updated")
        self.updateEdgePositions()
        #logger.debug("Edge positions updated")
        
    ##
    #  Updates the Sceneview items if any new Items appeared while runtime, by doing the following:
    #  - Checking if the nodeCount of the GraphModel is the same as the nodeCount of the PGV-Graph
    #  - If not it runs through all nodes of the PGV-Graph and checks if there is a according one
    #    in the GraphModel
    #  - If not it calls a setUpNode for the according node and stores it into a list of newNodes
    #  - Same for Edges/Dependencies
    #  - After doing that it adds the Edges to the Model and then the Nodes for correct Painting order\n
    #    For doing that it captures all graphItems removes them from the scenE and adds them in 
    #    correct order 
    #    @returns True if there were new items added to the graph or false if only some Properties
    #             were changed so no new layouting is required
    def updateGraphItems(self, currentGraph):
        itemCountChanged = False
        #Update NodeItems
        #if not self.graphModel.getNodeCount() == len(self.temanejo.remoteControl.datanodes):
        logger.debug("Trying to add nodes")
        existingKeys = self.graphModel._nodes.keys()
        itemCountChanged = True
        for node in self.temanejo.graphlayoutIO.getNodes(currentGraph):
            if not int(node) in existingKeys:
                #logger.debug("setting up node")
                self.temanejo.graphModel.addItemToModel(self.setUpNode(node, currentGraph))
                #logger.debug("Added node to Scene")

        #Update EdgeItems
        #if not self.graphModel.getEdgeCount() == len(self.temanejo.remoteControl.dataedges):
        existingKeys = self.graphModel._edges.keys()
        itemCountChanged = True

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # !!!!!!!!!!!! We need to remove duplicated items in the get edgelist of the current graph, since in !!!!!!!!!!!
        # !!!!!!!!!!!!   the pygraphviz routine duplicated items are overridden and in our routine they are  !!!!!!!!!!!
        # !!!!!!!!!!!!                         handled correctly as multidependencies                        !!!!!!!!!!!
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        hackEdgeIdCounterForImportDot = 1
        for edge in list(set(self.temanejo.graphlayoutIO.getEdges(currentGraph))):
            #TODO Fix this String cast hack
            n_edge = (str(edge[0]), str(edge[1]))
            multiDepCounter = 0
            origID = []
            for dataedge in self.temanejo.remoteControl.dataedges.values():
                if dataedge.getProperty('fromToId') == n_edge:
                    multiDepCounter += 1
                    origID.append(dataedge.getProperty('depId'))
                #TODO HACK: This is done for importing from dot where the dataedge is totally empty
                elif dataedge.getProperties() == {}:
                    multiDepCounter = 1
                    origID.append(hackEdgeIdCounterForImportDot)
                    hackEdgeIdCounterForImportDot+=1

            for depCount in range(0, multiDepCounter):
                #Setup Edge
                edgeId = ("%s_%s"%(n_edge[0], depCount),
                          "%s_%s"%(n_edge[1], depCount))
                if not edgeId in existingKeys:
                    self.temanejo.graphModel.addItemToModel(self.setupEdge(n_edge, edgeId, origID[depCount]))

        return itemCountChanged
        
    ##
    #   Updates the nodePositions after any new layouting...                
    def updateNodePositions(self, currentGraph):
        for node in self.graphModel.getNodes():
            # Get node pos from the layouted graph
            nodePos = self.temanejo.graphlayoutIO.getNodePos(node.getNodeId(), currentGraph)
            #Query Boundaries
            self.maxPos[0] = nodePos[0] if self.maxPos[0] < nodePos[0] else self.maxPos[0]
            self.maxPos[1] = nodePos[1] if self.maxPos[1] < nodePos[1] else self.maxPos[1]
            self.minPos[0] = nodePos[0] if self.minPos[0] > nodePos[0] else self.minPos[0]
            self.minPos[1] = nodePos[1] if self.minPos[1] > nodePos[1] else self.minPos[1]
            
            node.setNodeLocation(nodePos)
        self.mapPositionsToScene()
    
    ##
    #   Updates the edgePositions after any new layouting...      
    def updateEdgePositions(self):
        for edge in self.graphModel.getEdges():

            fromToNodeIds = (edge.getEdgeId()[0].split("_"), edge.getEdgeId()[1].split("_"))
            # Update Startlocation
            curr_item = self.graphModel.getNode(int(fromToNodeIds[0][0]))
            startpos = curr_item.getNodeLocation()
            startpos = [startpos[0]+curr_item.getNodeSize()/2, 
                        startpos[1]+curr_item.getNodeSize()/2]
            edge.setStartLocation(startpos)
            # Update Endlocation
            curr_item = self.graphModel.getNode(int(fromToNodeIds[1][0]))
            endpos = curr_item.getNodeLocation()
            endpos = [endpos[0]+curr_item.getNodeSize()/2, 
                      endpos[1]+curr_item.getNodeSize()/2]
            edge.setEndLocation(endpos)

    ##
    #   Maps the node and edgePositions to the scene.\n
    #   This is done in order to map the nodepositions around the scenecenter (0,0)\n
    #   Only needed for zooming correctly  
    def mapPositionsToScene(self):
        mapPos = [0,0]
        mapPos[0] = self.maxPos[0] if abs(self.maxPos[0]) > abs(self.minPos[0]) else self.minPos[0]
        mapPos[1] = self.maxPos[1] if abs(self.maxPos[1]) > abs(self.minPos[1]) else self.minPos[1]
        for node in self.graphModel.getNodes():
            newNodeLoc = [node.getNodeLocation()[0] - mapPos[0] / 2,
                          node.getNodeLocation()[1] - mapPos[1] / 2]
            node.setNodeLocation(newNodeLoc)
            
        self.bounds = [-mapPos[0] - 40, -mapPos[1] - 40, mapPos[0] + 40, mapPos[1] + 40]
    
    #
    #   Layouts the Graph via calling build in methods from the PyGraphViz lib
    #   It layouts the Graph by doing the following things:
    #   - It checks if the currentlyLayouting flag is false and if it is it starts the layouting
    #   - Sets the currentlyLayouting flag to true
    #   - Checks if a new layouting is needed by counting input nodes and edges and compare them to
    #     the viewed edges
    #   - Disables the refreshButton
    #   - Calls the PGV-Layoutroutine
    #   - Updates the Graphmodel and the sceneRect
    #   - Updates the Viewport
    #   - Finally enables the Refresh button

    # def layoutPgvGraph(self,routine = None):
    #     logger.debug("Number of edges in the Graph: %s"%self.temanejo.graphModel.getEdgeCount())
    #     previousLayoutRoutine = self.temanejo.currentActiveLayout
    #     routine = self.temanejo.createOnFlyLayoutCodeForLayoutAction()
    #     if not self.currentlyLayouting:
    #         self.currentlyLayouting = True
    #         # The 5. Button in the zoomButtonCollection Is the Refreshbutton
    #         # It will be disabled it while layouting the Graph so there cannot be any collisions.\n
    #         # This is done in order to be prepared for layouting the Graph in a separate Thread\n
    #         # Needed to be encapsuled in a try except because of the creation order by starting the
    #         # application
    #         if (not (self.graphModel.getNodeCount() == len(self.temanejo.remoteControl.datanodes)
    #                 and self.graphModel.getEdgeCount() == len(self.temanejo.remoteControl.dataedges))
    #             ) or previousLayoutRoutine is not routine:
    #
    #             try:
    #                 self.temanejo.zoomButtons[4].setDisabled()
    #             except:
    #                 pass
    #
    #
    #             self.temanejo.statusBar().showMessage("Layouting Graph ...")
    #             logger.debug("Layouting Graph ...")
    #             self.temanejo.graphlayoutIO.layoutGraph(routine=routine)
    #             self.temanejo.statusBar().showMessage("Graph layouted via %s"%routine)
    #             logger.debug("Graph layouted via %s"%routine)
    #             self.updateGraphModel()
    #             # Update QGraphicsView
    #             self.graphModel.setSceneRect(QtCore.QRectF(self.bounds[0], self.bounds[1],
    #                                                        2 * self.bounds[2], 2 * self.bounds[3]))
    #             self.temanejo.ui.temanejoGraph.viewport().update()
    #             # See disabling the button some lines above
    #             try:
    #                 self.temanejo.zoomButtons[4].setEnabled()
    #             except:
    #                 pass
    #
    #             #self.temanejo.statusBar().showMessage("Temanejo Viewport is updated")
    #         else:
    #             self.temanejo.statusBar().showMessage("")
    #             logger.debug("No layouting was necessary... Only some PropertiesUpdate")
    #         self.currentlyLayouting = False
