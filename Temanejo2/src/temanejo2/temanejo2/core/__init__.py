"""!
@brief The core package provides all necessary objects and modifications from standard pysideQT Objects in order
to set up temanejo

@author Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com

In this Module a Basic set of objects with no interconnections, except of inheritance, will be provided.
These Objects following the structure which has to be implemented in order to get the Project implemented
according to a MVC-Pattern.\n
Especially all abstract graph layout and paint data will be implemented here. \n
As well as all Modifications to the standard Qt-Models and -Views\n
Last but not least the Commuicator to ayudame should be defined here
"""

__all__=["items"]