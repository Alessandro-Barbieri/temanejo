'''!
@brief All Item to be painted onto the graph will be provided here. 

This Module provides a set of drawable objects. These Object will all be based on a QGraphicsItem, 
so in order to get them painted, they have to be held in a QGraphicsScene, which is a Model for 
the View QGraphicsView. This is handled that way just to get the data away from the appearance.\n
A Item can be everything which has to be painted at one point into the GUI 
So this can be everything from a GraphItem or a Button or anything else...

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

__all__=["NodeItem","GraphItem"]