'''!
@brief All nodes of the graph will be provided here.

The NodeItem Module provides a Class NodeItem which defines the appearance of a node in the graph.
A node can be just a node but also a set of node merged together.
This NodeItem should be Painted later on a QGraphicsView therefore it will be held in a QGraphicsScene
in order to get all the Items into a Model which then will be independent from the View. So it could
also be painted everythere.
@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtCore, QtGui

from GraphItem import BaseItem
from temanejo2.temanejo2.core.items import miscDialogs
from temanejo2.temanejo2.lib import configuredValues, primitives

logger = logging.getLogger(__name__)



##
# This class defines the Node which has to be painted in the QGraphicsView. It will be set in
# the Scene defined in the model package.\n
# It further delivers all necessary properties which are needed to paint the node to the correct
# place in the correct appearance.
# In order to Paint the Node to the correct Place, it helds a reference onto an id of a DataNode
# where the absolute and relative position will be found

class NodeItem(BaseItem):

    # ===============================================================================================
    #  Initialization Section
    # ===============================================================================================

    ##
    #     Initialize the Node to be painted
    #     @param temanejo An instance of the main application in order to be able to query
    #                      for controller etc.
    #     @param datanode A reference to the corresponding datanode generated from the backend.
    #     @param nodeId This is the unique id of a node in the graph and should be wrapped in a list.
    #                 For a single node it is just a list with length 1. But since the node can
    #                 also be a Set of nodes (so it can be a subgraph) this parameter should ever
    #                 be a list.\n Example: [1,4345,654,7678,7] for a set of nodes or [12376543] for
    #                 a single node.
    #     @param inOutId This should be a 2 dimensional list in order to get the inputs and outputs
    #                       of a node in the graph. All inputs are saved in the first entry and all
    #                       outputs are saved in the second one.\n Example: [[11,44,33,22],[12,13,14]]
    #                       Here the list [11,44,33,22] stands for all input Items and [12,13,14]
    #                       represents the output items
    #     @param location This should be a list with a length of 2. the first entry is the nomralized
    #                      x-position and the second is the y-position\n
    #                      Example: location = [0,0]\n
    #                      If the NodeItem is a group of nodes the location needs to be averaged.
    #                      Therefore you can find a averageLocation method in the test_lib package
    def __init__(self, temanejo, datanode, nodeId, nodeSize = None, inOutId = None, location = None,
                 parent = None, tool_tip = "Nothing set yet"):
        #Call baseclass constructor
        BaseItem.__init__(self, nodeId, parent)
        ## @member The id of the node. !SHOULD BE UNIQUE!
        self.__nodeId = nodeId
        ## @member See param inOutId
        self.__inOutId = [[],[]] if inOutId is None else inOutId
        ## @member
        # The node_size defines the size of a node which shall be painted
        # It has a default value but can also be changed in order to get a proper appearance for the
        #  node in every zoom of the graph
        self.__nodeSize = configuredValues.DEFAULT_NODESIZE if nodeSize is None else nodeSize
        ##@var __location
        # This is the center of the node and defines where the node should be painted
        self.__location = [0,0] if location is None else location
        self.temanejo = temanejo
        ##@var nodeColor
        # This variable helds the color of the node which should be painted
        self.nodeColor = QtCore.Qt.red
        ##@var marginColor
        # This variable helds the color of the margin of the node which should be painted
        self.marginColor = QtCore.Qt.black

        ##@var _shape
        #  The shape of the node which shall be painted.
        #  Default is circle
        self._shape = "circle"

        # If datanode is none grey out the nodeitem since the task has not arrived yet
        if datanode is None:
            self.nodeColor = QtCore.Qt.gray
            self.marginColor = QtCore.Qt.gray

        ##@var label
        # This is the text a node has. By default it is the ID of the node
        self.label = "%s"%self.getId()

        ##@var tool_tip
        # Sets the tooltip for the node
        self.setTooltip(tool_tip)

        ##@var datanode
        # This is the reference to the datanode created by the remotecontroller in order
        # to hold all properties given by the backend. \n
        # Will also be the way how to communikate with the backend
        self.datanode = datanode

        ##@var _status
        # This is the processing state of the node\n
        # It reflects if the corresponding task of the node has been processed yet or not.
        try:
            self._status = datanode.getProperty("state") in [True,1]
        except:
            logger.warning("Couldn't find processed Attribute")
            self._status = False
        ##@var _paused
        # This var shows if the node is blocked by the User or not.\n
        # It is just held in here for convenience for giving the user a visual feedback.
        # The overlay of the node will be painted or not by queriing this state...

        try:
            self._paused = datanode.getProperty("paused") in [True,1]
        except:
            logger.warning("Couldn't find paused Attribute node state is set to nonpaused ")
            self._paused = False

        # Set Nodes in front of edges
        self.setZValue(0.0)
    #===============================================================================================
    # Draw Item Section
    #===============================================================================================


    ##
    # Overwrites the QGraphicsItem Method in order to paint the Node into the view.\n
    # It just checks if the node is processed and calls the according method\n
    # It also checks if text should be painted or not. If scaling factor
    # is greater than 2 text will be seen otherwise not
    def paint(self, painter=None, option=None, widget=None):

        if ((self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE
           or self.temanejo.ui.actionForce_Show_Nodes.isChecked())
           and not self.temanejo.ui.actionForce_Hide_Nodes.isChecked()):
            self.drawShape(painter)
            #if (self._status):
            #    self.drawProcessedNode(painter)
            #else:
            #    self.drawUnprocessedNode(painter)

        # Draw label and overlay
        if (self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE
            and not self.temanejo.ui.actionForce_Hide_Nodes.isChecked()):

            self.drawOverlay(painter)
            self.drawText(painter)

    ##
    #   Draws the shape according to the prop shape which is set
    def drawShape(self,painter=None):
        # Draw Margin
        if self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE:
            painter.setRenderHints(QtGui.QPainter.Antialiasing, on=True)
            primitives.drawMargin(self._shape, painter, self.marginColor,
                                  [self.__location[0] - configuredValues.DEFAULT_MAGRIN_SEP_FACTOR
                                                        * configuredValues.DEFAULT_MARGINSIZE,
                                   self.__location[1] - configuredValues.DEFAULT_MAGRIN_SEP_FACTOR
                                                        * configuredValues.DEFAULT_MARGINSIZE],
                                   self.__nodeSize + 2 * configuredValues.DEFAULT_MAGRIN_SEP_FACTOR
                                                        * configuredValues.DEFAULT_MARGINSIZE)

        # Draw node itself
        primitives.drawShape(self._shape, painter, self.nodeColor, self.__location, self.__nodeSize)

    ##
    # TODO deprecated
    #     Draws a circle for the unprocessed state of the node
    def drawUnprocessedNode(self, painter=None):
        # Draw Margin
        if self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE:
            primitives.drawCircle(painter, self.marginColor,
                                  [self.__location[0] - configuredValues.DEFAULT_MARGINSIZE,
                                   self.__location[1] - configuredValues.DEFAULT_MARGINSIZE],
                                   self.__nodeSize + 2 * configuredValues.DEFAULT_MARGINSIZE)

        # Draw node itself
        primitives.drawCircle(painter,self.nodeColor,self.__location,self.__nodeSize)


    ##
    # TODO deprecated
    # Draws a triangle for the processed state of the node
    def drawProcessedNode(self, painter=None):
        # Margin
        if self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE:
            primitives.drawTriangle(painter, self.marginColor,
                                  [self.__location[0] - 2 * configuredValues.DEFAULT_MARGINSIZE,
                                   self.__location[1] - 1 * configuredValues.DEFAULT_MARGINSIZE],
                                   self.__nodeSize + 4 * configuredValues.DEFAULT_MARGINSIZE)
        # Draw node itself
        primitives.drawTriangle(painter,self.nodeColor,self.__location,self.__nodeSize)

    ##
    #   Draws a text in the center of the node.\n
    #   The text which should be drawn can be set via manipulating the instance variable self.label
    def drawText(self, painter=None):
        # Draw text on node
        f = painter.font()
        fontSize = self.__nodeSize * 0.25
        f.setPointSizeF(fontSize)
        painter.setFont(f)
        painter.setRenderHints(QtGui.QPainter.TextAntialiasing, on=True)
        painter.setPen(QtGui.QPen(QtGui.QColor(0.0,0.0,0.0), 1, QtCore.Qt.SolidLine,
                    QtCore.Qt.FlatCap, QtCore.Qt.MiterJoin))

        bounding_rect = QtCore.QRect(self.__location[0], self.__location[1], self.__nodeSize, self.__nodeSize)
        painter.drawText(bounding_rect, QtCore.Qt.AlignCenter|QtCore.Qt.TextDontClip, str(self.label))


    ##
    #   Draws all overlays onto the node...
    def drawOverlay(self,painter=None):
        if self._paused and not self._status:
            painter.setRenderHints(QtGui.QPainter.Antialiasing, on=True)
            painter.setPen(QtGui.QPen(QtGui.QColor(0.0,0.0,0.0), 2, QtCore.Qt.SolidLine,
                    QtCore.Qt.RoundCap, QtCore.Qt.MiterJoin))
            primitives.drawPause(painter, QtCore.Qt.black, self.__location, self.__nodeSize)

    ##
    #   Sets the PauseState
    def setPauseState(self,pause):
        self._paused = pause

    ##
    #   Returns the PauseState
    def getPauseState(self):
        return self._paused
    #===============================================================================================
    # Getter and Setter Section
    #===============================================================================================
    ##
    #   Sets the shape
    def setShape(self, shape = None):
        self._shape = shape

    ##
    #   Returns the name of the shape which is set to the node
    def getShape(self):
        return self._shape

    ##
    #   Returns the id of the node
    def getNodeId(self):
        return self.__nodeId

    ##
    #   Just sets the nodes Size
    def setNodeSize(self,size):
        self.__nodeSize = size

    ##
    #   Queries the nodes Size of the instance
    def getNodeSize(self):
        return self.__nodeSize
    ##
    #   Sets the nodes location in the graph
    def setNodeLocation(self,location):
        self.__location = location

    ##
    #   Queries the location of the node in the graph
    def getNodeLocation(self):
        return self.__location

    ##
    #   Update the nodedependencies
    #   Please see init how inOutId has to be formatted
    def setInOutDependencies(self,inOutId):
        self.__inOutId = inOutId

    ##
    #   Returns the node dependencies
    def getInOutDependencies(self):
        return self.__inOutId

    def setTooltip(self, tool_tip):
        super(NodeItem, self).setToolTip(tool_tip)

    ##
    # Sets the Margin Color of the node given by argument
    # if no arg is given then a colordialog is created and a color can be chosen
    def setMarginColor(self,color = None):
        if color is None:
            tempCol = miscDialogs.colordialog()
            self.marginColor = tempCol if tempCol is not None else self.marginColor
        else:
            self.marginColor = color
    ##
    # Sets the Color of the node given by argument
    # if no arg is given then a colordialog is created and a color can be chosen
    def setNodeColor(self, color = None):
        if color is None:
            tempCol = miscDialogs.colordialog()
            self.nodeColor = tempCol if tempCol is not None else self.nodeColor
        else:
            self.nodeColor = color

    ##
    # Sets the label of the node by the given text
    # If no argument is given it pops up a textInputDialog so the user can specify the Label on his own
    def setNodeLabel(self, text=None):
        if text is None:
            tempLabel = miscDialogs.textInputDialog()
            self.label = tempLabel if len(tempLabel) > 0 else self.label
        else:
            self.label = text
    ##
    #   Defines the bounding rect of the NodeItem
    def boundingRect(self):
        return QtCore.QRectF(self.__location[0] - 6 * configuredValues.DEFAULT_MARGINSIZE,
                             self.__location[1] - 6 * configuredValues.DEFAULT_MARGINSIZE,
                             self.__nodeSize + 12 * configuredValues.DEFAULT_MARGINSIZE,
                             self.__nodeSize + 12 * configuredValues.DEFAULT_MARGINSIZE)

    ##
    #   Set a datanode to the according node. This is needed in the case a dependency was send before
    #   the according task and therefore a dummiNode was needed to be set
    def setDataNode(self, datanode):
        self.datanode = datanode
        self.nodeColor = QtCore.Qt.red
        self.marginColor = QtCore.Qt.black

    #===============================================================================================
    #===============================================================================================
    # EVENT HANDLER
    #===============================================================================================
    #===============================================================================================

    ## Connects all necessary controllers to the Item
    def connectController(self, controller):
        ## @member A handle to the controller object where all actions will handled.\n
        #          This var is used to indirect all Events to the controller
        self.inputEventCtrl = controller

    #===============================================================================================
    # KEY EVENTS
    #===============================================================================================
    ##
    #   Definition of the keypress event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def keyPressEvent(self, evt):
        self.inputEventCtrl.keyPressEvent(evt)
    ##
    #   Definition of the keyrelease event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def keyReleaseEvent(self,evt):
        self.inputEventCtrl.keyReleaseEvent(evt)

    #===============================================================================================
    # MOUSE EVENTS
    #===============================================================================================
    ##
    #   Definition of the mousepress event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mousePressEvent(self,evt):
        self.inputEventCtrl.mousePressEvent(evt)

    ##
    #   Definition of the mouserelease event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mouseReleaseEvent(self,evt):
        self.inputEventCtrl.mouseReleaseEvent(evt)

    ##
    #   Definition of the mousepress event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mouseDoubleClickEvent(self,evt):
        self.inputEventCtrl.mouseDoubleClickEvent(evt)

    ##
    #   Definition of the mouseMove event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mouseMoveEvent(self,evt):
        self.inputEventCtrl.mouseMoveEvent(evt)

    #===============================================================================================
    # Context Menu Events
    #===============================================================================================
    ##
    #   Changes the PauseState of the node\n
    #   It just flips the value so if it was true it is false afterwards etc...
    def __changePauseState(self):
        successState = False
        if self.datanode.getProperty("paused"):
            successState = self.temanejo.graphActionEventHandler.unblockTask(self.datanode.getProperty("clientId"),
                                                              self.datanode.getProperty("taskId"))
        else:
            successState = self.temanejo.graphActionEventHandler.blockTask(self.datanode.getProperty("clientId"),
                                                            self.datanode.getProperty("taskId"))

        if(successState):
            self._paused = not self._paused
            self.datanode.setProperty("paused", not self.datanode.getProperty("paused"))
            self.update()

    def __addDDTBreakpoint(self):
        if self.temanejo.remoteControl.ddt_parser is not None:
            self.temanejo.remoteControl.ddt_parser.request_add_breakpoint(self.datanode.getProperty("function_name"))
        else:
            logger.error("There is no open DTT connection")

    def __showInfo(self):
        self.infowidget = NodeInfowidget(self.datanode.getProperties(),parent=self.temanejo)
        self.infowidget.show_infowidget()
    ##
    #   Creates a context menu for the node.\n
    #   By default it will be created via left mouse click onto the node.
    #   There all the nodedependent attrs can be set
    #   @TODO Move it to the controller object
    def contextMenuEvent(self, event):
        #Create ContextMenu
        menu = QtGui.QMenu()
        title = menu.addAction("Menu for Node %s"%self.label)
        title.setEnabled(False)
        menu.addSeparator()
        colormenu  = QtGui.QMenu("Node Colors")
        menu.addMenu(colormenu)
        labelAction = menu.addAction("Label")
        labelAction.triggered.connect(self.setNodeLabel)
        statemenu  = QtGui.QMenu("Change Nodestates")
        menu.addMenu(statemenu)
        #Create ColorMenu
        colorAction = colormenu.addAction("Choose Color")
        colorAction.triggered.connect(self.setNodeColor)
        magcolAction = colormenu.addAction("Choose Margin Color")
        magcolAction.triggered.connect(self.setMarginColor)
        #Create NodeStateMenu
        pauseAction = statemenu.addAction("Block/Unblock")
        pauseAction.triggered.connect(self.__changePauseState)

        #Create Addbreakpoint
        breakpointAction = menu.addAction("Add DDT Breakpoint")
        breakpointAction.triggered.connect(self.__addDDTBreakpoint)

        # Create ShowNodeInfo
        showInfoAction = menu.addAction("Show Nodeinfo")
        showInfoAction.triggered.connect(self.__showInfo)

        #Execute ContextMenu
        menu.exec_(event.screenPos())


class NodeInfowidget(QtGui.QWidget):
    def __init__(self, props, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.props = props
        self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.FramelessWindowHint)
        self.b_layout = QtGui.QGridLayout()
        self.left_mouse_button_pressed = False
        self.old_mouse_pos = None

    def show_infowidget(self):
        self.set_items_to_layout()
        self.setLayout(self.b_layout)
        self.show()

    def set_items_to_layout(self):
        row_counter = 0
        for key, val in self.props.items():
            key_label = QtGui.QLabel(str(key))
            val_label = QtGui.QLabel(str(val))
            self.b_layout.addWidget(key_label, row_counter, 0)
            self.b_layout.addWidget(val_label, row_counter, 1)
            row_counter += 1

    def mousePressEvent(self, evt):
        pressed_button = evt.button()
        self.old_mouse_pos = evt.globalPos()
        if pressed_button == QtCore.Qt.RightButton: #or pressed_button == QtCore.Qt.MidButton
            self.hide()
            self.destroy()
        else:
            self.left_mouse_button_pressed = True

    def mouseReleaseEvent(self, evt):
        self.left_mouse_button_pressed = False

    def mouseMoveEvent(self, evt):
        widget_pos = self.pos()
        if self.left_mouse_button_pressed:
            diff = widget_pos - (self.old_mouse_pos-evt.globalPos())
            self.move(diff.x(), diff.y())
            self.old_mouse_pos = evt.globalPos()
