'''!
@brief All edges of the graph will be provided here.

The EdgeItem Module provides a GraphItem in which the the standard Edge is provided.
This EdgeItem should be Painted later on a QGraphicsView therefore it will be held in a QGraphicsScene
in order to get all the Items into a Model which then will be independent from the View. So it could
also be painted everywhere.

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import logging

from PySide import QtCore, QtGui

from GraphItem import BaseItem
from temanejo2.temanejo2.core.items import miscDialogs
from temanejo2.temanejo2.lib import configuredValues, primitives

logger = logging.getLogger(__name__)


##
# This class defines the Edge which has to be painted in the QGraphicsView. It will be set in
# the Scene defined in the model package.\n
# It further delivers all necessary properties which are needed to paint the node to the correct
# place in the correct appearance.\n
# In order to Paint the Edge to the correct Place, it gets a start and end location.
# For now it is just a simple line but there can be implemented everything which is wanted to strore
# Data etc
class EdgeItem(BaseItem):

    # ===============================================================================================
    # Initialization Section
    # ===============================================================================================
    ##
    #     Initialize the Edge to be painted
    #     @param temanejo An instance of the main application in order to be able to query
    #                      for controller etc.
    #     @param edgeId This is the unique id of a edge in the graph. By default it should be a
    #                   tuple (a,b) where a is the id of the parent Node and b is the id of the
    #                   childnode
    #     @param startlocation The location where the edge begins. (Should be the center of the
    #                          parent node)
    #     @param endLocation The location where the edge ends. (Should be the center of the
    #                        child node)
    def __init__(self, temanejo, edgeId, origID, startLocation=None, endLocation=None, parent=None):
        BaseItem.__init__(self, edgeId, parent)
        ##
        # @member Handle to the main application
        self.temanejo = temanejo

        self.origID = origID
        ##
        # @member The (unique) edge id. At least it should be a unique value
        self.__edgeId = edgeId
        ##
        # @member The location where the edge starts
        self.__startLocation = startLocation if startLocation else [0,0]
        ##
        # @member The location where the edge ends
        self.__endLocation = endLocation if endLocation else [0,0]
        ##
        # @member The Label which should be painted onto the edge
        self.label = "(%s, %s)"%self.__edgeId
        ##
        # @member The color of the edge
        self.color = QtCore.Qt.blue
        # Set Edge behind nodes
        self.setZValue(-1.0)
        #ComputeDrawing Angle
        self.edgeDuplicateNumber = int((str(self.__edgeId[0]).split("_"))[1])
        self.total_amount_of_duplicated_edges = 0
        self.numberAngle = 0.0
        self.vertical_label_shift = 0.0

    ##
    #   Paints the edge.\n
    #   Creates a pen sets the edge width to 2 and draws a line from __startLocation
    #   to __endLocation
    #   Finally it calls drawText in order to paint the Label if zoom is big enough
    def paint(self, painter=None, option=None, widget=None):
        # Get Pen and set all edge appearance stuff
        pen = QtGui.QPen()
        pen.setColor(self.color)
        pen.setWidth(configuredValues.DEFAULT_EDGE_WIDTH)
        pen.setCapStyle(QtCore.Qt.RoundCap)
        painter.setPen(pen)
        painter.setRenderHints(QtGui.QPainter.Antialiasing, on=True)
        # Compute variables for multidependency painting
        self.numberAngle = 0.0
        horiz_edge_shifter = (2 * (self.edgeDuplicateNumber % 2) - 1)
        # set numberangle in order to paint alternately left and right
        if self.edgeDuplicateNumber % 2 == 0:
            self.numberAngle = - self.edgeDuplicateNumber/2.0
            self.vertical_label_shift = - self.edgeDuplicateNumber
            horiz_edge_shifter *= self.edgeDuplicateNumber
        else:
            self.numberAngle = (self.edgeDuplicateNumber+1)/2.0
            self.vertical_label_shift = self.edgeDuplicateNumber + 1
            horiz_edge_shifter *= self.edgeDuplicateNumber + 1

        if (not configuredValues.SHOW_MULTI_EDGES and self.edgeDuplicateNumber < 1) or configuredValues.SHOW_MULTI_EDGES:
            # Always paint a curved line for dependencies which go through more than one level
            if self.temanejo.graphlayoutIO.getRoutine() =="hlrs_custom":
                if self.__endLocation[1] - self.__startLocation[1] > configuredValues.DEFAULT_SEP_FACTOR*configuredValues.DEFAULT_NODESIZE:
                    # if self.numberAngle < 0.0:
                    #     self.numberAngle -= 1.5
                    # else:
                    #     self.numberAngle += 1.5

                    primitives.drawCurvedLine(painter,
                                                  self.__startLocation[0],
                                                  self.__startLocation[1] + configuredValues.DEFAULT_NODESIZE*0.6 + configuredValues.DEFAULT_MAGRIN_SEP_FACTOR*configuredValues.DEFAULT_MARGINSIZE,
                                                  self.__endLocation[0],
                                                  self.__endLocation[1] - configuredValues.DEFAULT_NODESIZE*0.6 - configuredValues.DEFAULT_MAGRIN_SEP_FACTOR*configuredValues.DEFAULT_MARGINSIZE,
                                                  configuredValues.DISTANCE_CURVED_EDGE_MULT * self.numberAngle)
                    if self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE:
                        pen.setColor(QtCore.Qt.black)
                        painter.setPen(pen)
                        self.drawText(painter, shifted=True)
                    return
            horiz_edge_shifter *= configuredValues.DISTANCE_STRAIGHT_EDGE_MULT

            # paint straight or round edges
            if self.edgeDuplicateNumber < 6:
                if configuredValues.STRAIGHT_EDGES:
                    painter.drawLine(self.__startLocation[0] + horiz_edge_shifter,
                                     self.__startLocation[1] + configuredValues.DEFAULT_NODESIZE*0.6 + configuredValues.DEFAULT_MAGRIN_SEP_FACTOR*configuredValues.DEFAULT_MARGINSIZE,
                                     self.__endLocation[0] + horiz_edge_shifter,
                                     self.__endLocation[1] - configuredValues.DEFAULT_NODESIZE*0.6 - configuredValues.DEFAULT_MAGRIN_SEP_FACTOR*configuredValues.DEFAULT_MARGINSIZE)
                else:
                    primitives.drawCurvedLine(painter,
                                              self.__startLocation[0],
                                              self.__startLocation[1] + configuredValues.DEFAULT_NODESIZE*0.6 + configuredValues.DEFAULT_MAGRIN_SEP_FACTOR*configuredValues.DEFAULT_MARGINSIZE,
                                              self.__endLocation[0],
                                              self.__endLocation[1] - configuredValues.DEFAULT_NODESIZE*0.6- configuredValues.DEFAULT_MAGRIN_SEP_FACTOR*configuredValues.DEFAULT_MARGINSIZE,
                                              configuredValues.DISTANCE_CURVED_EDGE_MULT * self.numberAngle)

        if self.temanejo.graphAppearanceEventHandler.scaleFactor > configuredValues.FACTOR_FOR_DETAIL_APPEARANCE:
            pen.setColor(QtCore.Qt.black)
            painter.setPen(pen)
            self.drawText(painter)

    ##
    #   Paints the label onto the edge
    def drawText(self,painter=None, shifted=False):
        if configuredValues.SHOW_EDGE_LABELS:
            f = painter.font()
            fontSize = 6
            f.setPointSizeF(fontSize)
            painter.setFont(f)
            painter.setRenderHints(QtGui.QPainter.TextAntialiasing, on=True)

            if shifted:
                bounding_rect = QtCore.QRect(self.__startLocation[0] - self.numberAngle * (self.__endLocation[1] - self.__startLocation[1]) / 30.0 - self.vertical_label_shift,
                                             self.__startLocation[1] - self.numberAngle * (self.__endLocation[0] - self.__startLocation[0]) / (0.5*(self.__endLocation[1] - self.__startLocation[1])+0.0001),
                                             self.__endLocation[0] - self.__startLocation[0],
                                             self.__endLocation[1] - self.__startLocation[1] -
                                             self.vertical_label_shift * configuredValues.EDGE_LABEL_SHIFT_MULT)
            else:
                bounding_rect = QtCore.QRect(self.__startLocation[0] - self.vertical_label_shift,
                                             self.__startLocation[1],
                                             self.__endLocation[0] - self.__startLocation[0],
                                             self.__endLocation[1] - self.__startLocation[1] -
                                             self.vertical_label_shift * configuredValues.EDGE_LABEL_SHIFT_MULT)

            if configuredValues.SHOW_MULTI_EDGES:
                painter.drawText(bounding_rect, QtCore.Qt.AlignCenter | QtCore.Qt.TextDontClip, self.label)

            else:
                painter.drawText(bounding_rect, QtCore.Qt.AlignCenter | QtCore.Qt.TextDontClip, str(self.edgeDuplicateNumber))


    # ===============================================================================================
    # Getter and Setter Section
    # ===============================================================================================

    ##
    # Sets the startlocation of the edge
    def setStartLocation(self,location):
        self.__startLocation = location

    ##
    # Returns the startlocation of the edge
    def getStartLocation(self):
        return self.__startLocation

    ##
    # Sets the endlocation of the edge
    def setEndLocation(self,location):
        self.__endLocation = location

    ##
    # Returns the endlocation of the edge
    def getEndLocation(self):
        return self.__endLocation

    ##
    # Returns the id of the egde
    def getEdgeId(self):
        return self.__edgeId

    ##
    # Returns the id of the egde
    def getOrigId(self):
        return self.origID

    ##
    #   Defines the bounding rect of the EdgeItem
    def boundingRect(self):
        x = self.__startLocation[0] if self.__startLocation[0] < self.__endLocation[0] else self.__endLocation[0]
        y = self.__startLocation[1] if self.__startLocation[1] < self.__endLocation[1] else self.__endLocation[1]
        w = self.__endLocation[0] - self.__startLocation[0] if self.__startLocation[0] < self.__endLocation[0] else self.__startLocation[0] - self.__endLocation[0]
        h = self.__endLocation[1] - self.__startLocation[1] if self.__startLocation[1] < self.__endLocation[1] else self.__startLocation[1] - self.__endLocation[1]
        return QtCore.QRect(x, y ,w, h)

    ##
    #  Sets the label by the given text.\n
    #  It no text is given it calls a textInputDialog and lets the user choose a Text for the label
    def setLabel(self, text = None):
        if text is None:
            tempLabel = miscDialogs.textInputDialog()
            self.label = tempLabel if len(tempLabel) > 0 else self.label
        else:
            self.label = text

    ##
    #  Sets the color of the edge.\n
    #  If no color is given a colordialog is called and the user can chose the color from the dialog
    def setEdgeColor(self, color = None):
        if color is None:
            tempCol = miscDialogs.colordialog()
            self.color = tempCol if tempCol is not None else self.color
        else:
            self.color = color

    # ===============================================================================================
    # ===============================================================================================
    # EVENT HANDLER
    # ===============================================================================================
    # ===============================================================================================

    ## Connects all necessary controllers to the Item
    def connectController(self, controller):
        ## @member A handle to the controller object where all actions will handled.\n
        #          This var is used to indirect all Events to the controller
        self.inputEventCtrl = controller

    # ===============================================================================================
    # KEY EVENTS
    # ===============================================================================================
    ##
    #   Definition of the keypress event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def keyPressEvent(self, evt):
        self.inputEventCtrl.keyPressEvent(evt)
    ##
    #   Definition of the keyrelease event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def keyReleaseEvent(self,evt):
        self.inputEventCtrl.keyReleaseEvent(evt)

    # ===============================================================================================
    # MOUSE EVENTS
    # ===============================================================================================
    ##
    #   Definition of the mousepress event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mousePressEvent(self,evt):
        self.inputEventCtrl.mousePressEvent(evt)

    ##
    #   Definition of the mouserelease event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mouseReleaseEvent(self,evt):
        self.inputEventCtrl.mouseReleaseEvent(evt)

    ##
    #   Definition of the mousepress event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mouseDoubleClickEvent(self,evt):
        self.inputEventCtrl.mouseDoubleClickEvent(evt)

    ##
    #   Definition of the mouseMove event. All events are redirected to the NodeUserInputEvents
    #   class found in the NodeController Module
    def mouseMoveEvent(self,evt):
        self.inputEventCtrl.mouseMoveEvent(evt)

    ##
    #   Creates a context menu for the edge.\n
    #   By default it will be created via left mouse click onto the node.
    #   There all the egdedependent attrs can be set
    #   @TODO Move it to the controller object
    def contextMenuEvent(self, event):
        #Create ContextMenu
        menu = QtGui.QMenu()
        title = menu.addAction("Menu for Edge %s"%self.label)
        title.setEnabled(False)
        menu.addSeparator()
        labelAction = menu.addAction("Label")
        labelAction.triggered.connect(self.setLabel)
        #Create ColorMenu
        colorAction = menu.addAction("Choose Color")
        colorAction.triggered.connect(self.setEdgeColor)
        #Execute ContextMenu
        menu.exec_(event.screenPos())
