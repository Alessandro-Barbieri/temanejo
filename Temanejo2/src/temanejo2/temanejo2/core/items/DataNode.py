'''!
@brief This module Provides a Datanode object

The Datanode will be created by the remote controller which communicates with Ayudame
It provides all data which is parsed from the remoteApplication 

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

logger = logging.getLogger(__name__)

##
#   Defines the Datanode\n 
#   This node will be created from the backend and helds all the data which could be displayed
#   Every Node of the scene has a reference on one of these instances and queries the display
#   information from this node
class DataNode(object):
    ## Creates a datanode
    def __init__(self,id, inOutIds = None, properties = None):
        ## @var __id:
        #                 The id given from the backend
        self.__id = id
        ## @var __inOutIds:
        #                These are the dependencies for all tasks which depend on this.
        #                It is a 2 dimensional list where the first dimension is the one for all
        #                inputs and the second is the one for all outputs
        self.__inOutIds = [[],[]] if inOutIds is None else inOutIds
        ## @var __properties: 
        #                  This is a dictionary in which all the properties should be held
        #                  except id and dependencies, only task related properties like taskduration etc
        #                  should be held there...\n
        #                  The key should ever be a utf8-string and the value should be a list of 
        #                  utf8-strings
        self.__properties = self._setStandardProperites() if properties is None else properties
        
        
    #===============================================================================================
    # Getter and Setter Section
    #===============================================================================================
    def getId(self):
        return self.__id
    
    def getDependencies(self):
        return self.__inOutIds
    
    def getProperties(self):
        return self.__properties
    
    def setProperties(self, dic):
        self.__properties.update(dic)
    
    def getProperty(self, name):
        try:
            return self.__properties[name]
        except KeyError, e:
            logger.warning("Property %s not known for the dataNode with id %s"%(name,self.__id))
            return None
    
    def setProperty(self,name,values):
        self.__properties[name] = values
        
    def _setStandardProperites(self):
        standardDict = dict()
        # The Status flag should be False if the corresponding task is not yet processed
        # otherwise true
        standardDict["state"] = True
        logger.info("StandartProps were set to the Datanode since no attributes were given to it!")
        return standardDict
