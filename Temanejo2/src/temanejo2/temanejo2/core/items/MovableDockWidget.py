'''!
@brief In this module a container Widget is defined where any collection can be added.

This module provides a custom widgetcontainer for adding any collection of widgets in order
to get a free movable window.\n
This shall provide the possibility for the user to move any collection of e.g. buttons free
over the Gui to customize the layout in a way the user find it the best to use for himself. 

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtCore, QtGui

logger = logging.getLogger(__name__)




##
# Definition of a Containerwidget which gets e.g a layout as childwidget.\n
# This object shall be a custom container in order to get a Custom Dock widget.\n
# Every collection of buttons etc. should be held in such a object in order to give the
# user the possibility to move the collection free on the screen. So the user can customize 
# the view himself as he thinks it should be... 
class FreeDockWidget(QtGui.QDockWidget):
    ##
    #   Initialize custom container
    #   @var temanejo: Handle to the main Application
    #   @var title: The title for the Dockwidget
    #   @var pos: The initial Position of the Dockwidget has to be a constant
    #             from the class QtCore.Qt.DockWidgetArea
    #   @var parent: The parentWidget in which this widget should be parented
    #   @var children:  The childwidgets which should be added onto this container.
    #   @var resizeable: Set this flag to True if you want your dockwidget to be resizeable.\n
    #                    Per default it is set to false for convenience. Every dockwidged will be
    #                    relayouted after docking to new Position. If this flag is not False it won't
    #                    shrink to minimum size...
    def __init__(self,temanejo, title = "", pos = None, parent=None, children=[], resizeable=True):
        QtGui.QDockWidget.__init__(self,parent)
        self.temanejo = temanejo
        self.children = children
        self.__resizeable = resizeable
        self.setStyleSheet(temanejo.styleSheet())
        # Set Tooltip for the widget
        self.setToolTip("Click Titleline to move the widget around")
        #Set features for Dockwindow (Everything but not deletable) 
        self.setFeatures(QtGui.QDockWidget.DockWidgetFloatable|QtGui.QDockWidget.DockWidgetMovable)
        #Set Sizepolicy for the widget
        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum,
                                             QtGui.QSizePolicy.Minimum))
        #Set a Title for the Dockwidget if needed
        self.setTitleBar(title)
        # Connect dockLocationChanged in order to make sure the children are layouted correctly
        self.dockLocationChanged.connect(self.dockLocationChange)
        #Add dockWidget to the main application at the correct Position 
        pos = QtCore.Qt.DockWidgetArea.RightDockWidgetArea if pos is None else pos
        temanejo.addDockWidget(pos, self)
        
    ##
    # This method layouts the assigned Widgets in a "correct" way.\n 
    # If the widget is docked on top or floor of the application
    # the widgets will be layouted horizontally in all other cases
    # they will be layouted vertically.\n
    # This method is called every time when the docklocation was changed.
    def _layoutChildren(self, location):
        # Create QWidget in order to place all widgets onto it
        wid = QtGui.QWidget()
        # Check where the window is docked and create according layout
        if(location == QtCore.Qt.LeftDockWidgetArea or
           location == QtCore.Qt.RightDockWidgetArea):
            layout = QtGui.QVBoxLayout()
        else:
            layout = QtGui.QHBoxLayout()
        # Add all childwidgets to the layout
        for child in self.children:
            layout.addWidget(child)
        # Set stretch for the layout so children are not streched dynamicly over the whole widget
        layout.setSpacing(1)
        # Set layout to the widget
        wid.setLayout(layout)
        # Check if dockwidget is resizable and set max size if not.
        # This is done because the widget wasn't resized after docking it to new location so
        # the Widget was far too large, should just be done for e.g. buttoncollections etc.
        if not self.__resizeable:
            self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
            wid.setMinimumSize(layout.sizeHint())
            wid.setMaximumSize(layout.sizeHint())
        # Add widget to the Custom Dockwidget
        self.setWidget(wid)


    ##
    # Sets a title to the dockwidget.\n
    # For now it only sets a label onto a horizontal layout. But there could be set everything
    # to it like a pixmap etc. as long as it is wrapped into any kind of a QWidget...
    def setTitleBar(self, title):
        layout = QtGui.QHBoxLayout()
        label = FreeDockWidgetHeader(title,self.temanejo)
        layout.addWidget(label)
        widget = QtGui.QWidget()
        widget.setLayout(layout)
        self.setTitleBarWidget(widget)
    
    ##
    #   This method performs everything what is needed to relayout the widget when the 
    #   appearance of it has changed (When it was undocked, dockLoc was changed etc.) 
    def dockLocationChange(self):
        dockLocation = self.temanejo.dockWidgetArea(self)
        self._layoutChildren(dockLocation)

        
    ##
    #   Override of the paintEvent in order to be able to apply a stylesheet
    def paintEvent(self, evt):
        super(FreeDockWidget,self).paintEvent(evt)
        opt = QtGui.QStyleOption()
        opt.initFrom(self)
        p = QtGui.QPainter(self)
        s = self.style()
        s.drawPrimitive(QtGui.QStyle.PE_Widget, opt, p, self)
        
        
class FreeDockWidgetHeader(QtGui.QLabel):
    def __init__(self,text,temanejo,parent=None):
        QtGui.QLabel.__init__(self,parent)
        self.setStyleSheet(temanejo.styleSheet())
        self.setText(text)
        self.setToolTip("Drag widget here to move it freely ")

