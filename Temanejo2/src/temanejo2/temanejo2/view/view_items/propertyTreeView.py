'''!
@brief 


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
''''''
Created on Nov 13, 2014

@author: temanejo
'''
from PySide import QtGui

from temanejo2.temanejo2.lib.easymodel.buttonDelegate import ButtonDelegate
from temanejo2.temanejo2.lib.easymodel.widgetdelegate import WD_TreeView, WidgetDelegate


class PropertyTreeView(WD_TreeView):
    """A tree view that that when clicked, tries to issue
    a left click to the widget delegate.
    """
    def __init__(self, temanejo, parent = None):
        """Initialize a new tree view
        :raises: None
        """
        super(PropertyTreeView, self).__init__(parent)
        self.wd = ButtonDelegate(temanejo, parent)
        self.setItemDelegateForColumn(2,self.wd)
        
        
        
    def mouseDoubleClickEvent(self, event):
        """If a widgetdelegate is double clicked,
        enter edit mode and propagate the event to the editor widget.
        :param event: the mouse event
        :type event: :class:`QtGui.QMouseEvent`
        :returns: None
        :rtype: None
        :raises: None
        """
        # find index at mouse position
        globalpos = event.globalPos()
        viewport = self.viewport()
        pos = viewport.mapFromGlobal(globalpos)
        i = self.indexAt(pos)
        # if the index is not valid, we don't care
        if not i.isValid():
            return super(PropertyTreeView, self).mouseDoubleClickEvent(event)
        # get the widget delegate. if there is None, return
        delegate = self.itemDelegate(i)
        if not isinstance(delegate, WidgetDelegate):
            return super(PropertyTreeView, self).mouseDoubleClickEvent(event)
        # if we are not editing, start editing now
        if self.state() != self.EditingState:
            self.edit(i)
        # check if we are in edit state now. if not, return
        if self.state() != self.EditingState:
            return
        # get the editor widget. if there is None, there is nothing to do so return
        widget = delegate.edit_widget()
        if not widget:
            return super(PropertyTreeView, self).mouseDoubleClickEvent(event)
        # try to find the relative position to the widget
        rect = self.visualRect(i) # rect of the index
        p = viewport.mapToGlobal(rect.topLeft())
        clickpos = globalpos - p
        # create a new event for the editor widget.
        e = QtGui.QMouseEvent(event.type(),
        clickpos,
        event.button(),
        event.buttons(),
        event.modifiers())
        widget.mouseDoubleClickEvent(e)
        # make sure to accept the event. If the widget does not accept the event
        # it would be propagated to the view, and we would end in a recursion.
        e.accept()
    
    def mousePressEvent(self, event):
        """If the mouse is presses on a widgetdelegate,
        enter edit mode and propagate the event to the editor widget.
        :param event: the mouse event
        :type event: :class:`QtGui.QMouseEvent`
        :returns: None
        :rtype: None
        :raises: None
        """
        # find index at mouse position
        globalpos = event.globalPos()
        viewport = self.viewport()
        pos = viewport.mapFromGlobal(globalpos)
        i = self.indexAt(pos)
        # if the index is not valid, we don't care
        if not i.isValid():
            return super(PropertyTreeView, self).mousePressEvent(event)
        # get the widget delegate. if there is None, return
        delegate = self.itemDelegate(i)
        if not isinstance(delegate, WidgetDelegate):
            return super(PropertyTreeView, self).mousePressEvent(event)
        # if we are not editing, start editing now
        if self.state() != self.EditingState:
            self.edit(i)
            # check if we are in edit state now. if not, return
            if self.state() != self.EditingState:
                return
        # get the editor widget. if there is None, there is nothing to do so return
        widget = delegate.edit_widget()
        if not widget:
            return super(PropertyTreeView, self).mousePressEvent(event)
        # try to find the relative position to the widget
        rect = self.visualRect(i) # rect of the index
        p = viewport.mapToGlobal(rect.topLeft())
        clickpos = globalpos - p
        # create a new event for the editor widget.
        e = QtGui.QMouseEvent(event.type(),
                              clickpos,
                              event.button(),
                              event.buttons(),
                              event.modifiers())
        widget.mousePressEvent(e)
        # make sure to accept the event. If the widget does not accept the event
        # it would be propagated to the view, and we would end in a recursion.
        e.accept()
        
    def mouseReleaseEvent(self, event):
        """If the mouse is released on a widgetdelegate,
        enter edit mode and propagate the event to the editor widget.
        :param event: the mouse event
        :type event: :class:`QtGui.QMouseEvent`
        :returns: None
        :rtype: None
        :raises: None
        """
        # find index at mouse position
        globalpos = event.globalPos()
        viewport = self.viewport()
        pos = viewport.mapFromGlobal(globalpos)
        i = self.indexAt(pos)
        # if the index is not valid, we don't care
        if not i.isValid():
            return super(PropertyTreeView, self).mouseReleaseEvent(event)
        # get the widget delegate. if there is None, return
        delegate = self.itemDelegate(i)
        if not isinstance(delegate, WidgetDelegate):
            return super(PropertyTreeView, self).mouseReleaseEvent(event)
        # if we are not editing, start editing now
        if self.state() != self.EditingState:
            self.edit(i)
            # check if we are in edit state now. if not, return
            if self.state() != self.EditingState:
                return
        # get the editor widget. if there is None, there is nothing to do so return
        widget = delegate.edit_widget()
        if not widget:
            return super(PropertyTreeView, self).mouseReleaseEvent(event)
        # try to find the relative position to the widget
        rect = self.visualRect(i) # rect of the index
        p = viewport.mapToGlobal(rect.topLeft())
        clickpos = globalpos - p
        # create a new event for the editor widget.
        e = QtGui.QMouseEvent(event.type(),
                              clickpos,
                              event.button(),
                              event.buttons(),
                              event.modifiers())
        widget.mouseReleaseEvent(e)
        # make sure to accept the event. If the widget does not accept the event
        # it would be propagated to the view, and we would end in a recursion.
        e.accept()
