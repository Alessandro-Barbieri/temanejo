"""!
@brief In this Package all view customizations will be held. Custom views will be created etc.

@author Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com

@todo: Make nice description
"""

__all__=["view_items"]