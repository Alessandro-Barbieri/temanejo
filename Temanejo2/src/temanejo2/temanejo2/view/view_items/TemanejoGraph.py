'''!
@brief The Base Graph View will be found in this Module

This Module provides the View class for painting the Graph onto the screen
it will expand the standard QtGraphicsView Class and add all customizations 
needed. All signals have to be redirected to the according Controll class 
found in the controll module. 

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtGui

logger = logging.getLogger(__name__)




## 
# This class is the base View Class for viewing the graph in the GUI
#All necessary signals and slots will be defined in here but not handled.
#They are only redirected to the according control object. You will
#find them in the controller module. 
class TemanejoGraph(QtGui.QGraphicsView):


    ##
    # Not much to do here except of calling the baseclass constructor\n
    # Furthermore different Optimsationflags are set here in order to 
    # Optimize the Rendering of the scene shown in the temanejo view..
    def __init__(self,parent=None):
        QtGui.QGraphicsView.__init__(self,parent)
        self.inputEventCtrl = None
        self.setDragMode(QtGui.QGraphicsView.DragMode.RubberBandDrag)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)
        self.setRenderHints(QtGui.QPainter.HighQualityAntialiasing|QtGui.QPainter.TextAntialiasing|
                            QtGui.QPainter.SmoothPixmapTransform)
        self.setOptimizationFlags(QtGui.QGraphicsView.DontClipPainter|
                                  QtGui.QGraphicsView.DontAdjustForAntialiasing|
                                  QtGui.QGraphicsView.DontSavePainterState)
        
      
        self.setViewportUpdateMode(QtGui.QGraphicsView.BoundingRectViewportUpdate)
        self.setCacheMode(QtGui.QGraphicsView.CacheBackground)
        
    ## Connects all necessary controllers to the View 
    def connectController(self, controller):
        self.inputEventCtrl = controller
        
    #===============================================================================================
    #===============================================================================================
    # EVENT HANDLER
    #===============================================================================================
    #===============================================================================================
    
    
    #===============================================================================================
    # KEY EVENTS
    #===============================================================================================
    ##
    #   Definition of the keypress event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module    
    def keyPressEvent(self, evt):
        self.inputEventCtrl.keyPressEvent(evt)
    ##
    #   Definition of the keyrelease event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module        
    def keyReleaseEvent(self,evt):
        self.inputEventCtrl.keyReleaseEvent(evt)
    
    #===============================================================================================
    # MOUSE EVENTS
    #===============================================================================================
    ##  
    #   Definition of the mousepress event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module    
    def mousePressEvent(self,evt):
        self.inputEventCtrl.mousePressEvent(evt)
    
    ##  
    #   Definition of the mouserelease event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module    
    def mouseReleaseEvent(self,evt):
        self.inputEventCtrl.mouseReleaseEvent(evt)
    
    ##  
    #   Definition of the mouseclick event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module    
    def mouseDoubleClickEvent(self,evt):
        self.inputEventCtrl.mouseDoubleClickEvent(evt)

    ##  
    #   Definition of the mouseMove event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module    
    def mouseMoveEvent(self,evt):
        self.inputEventCtrl.mouseMoveEvent(evt)

    ##  
    #   Definition of the mouseWheel event. All events are redirected to the GraphUserInputEvents
    #   class found in the GraphController Module
    def wheelEvent(self, evt):
        self.inputEventCtrl.wheelEvent(evt)
