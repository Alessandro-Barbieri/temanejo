'''!
@brief Several Collections for special needs are provided

In this Module several collections are provided in order to add
them into different models if needed. For example a collection of zoomButtons
will contain a zoom_in and a zoom_out Button but here can be added many more buttons
and all the views will be updated correctly since the models take their
content out of these collections.\n\n
<b>!!! Also all signals of the widgets will be connected in here to their according actions !!!</b>

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging
from functools import partial

from PySide import QtCore

from temanejo2.temanejo2.core.items.MovableDockWidget import FreeDockWidget
from temanejo2.temanejo2.core.items.PropertyWidgets import PropertyDisplayWidget
from temanejo2.temanejo2.core.items.TemanejoButton import TemanejoButton
from temanejo2.temanejo2.lib import configuredValues, temPaths, resources

logger = logging.getLogger(__name__)





##
#   This collection creates a ButtonCollection for all zoomactions as zoomIn or zoomOut etc...
#   in the end it adds them to a FreeDockWidget and adds this to the right side of the
#   Application
def zoomButtonCollection(temanejo):
    buttons = []
    # Create ZoomOut Button
    zoomOutButton = TemanejoButton(':images/tem_zoom_out_01.png',
                                  tool_tip = "Zoom Out")
    zoomOutButton.clicked.connect(partial(temanejo.graphAppearanceEventHandler.zoom_ctrl,1/configuredValues.ZOOM_FACTOR))
    buttons.append(zoomOutButton)

    # Create ZoomIn Button
    zoomInButton = TemanejoButton(':images/tem_zoom_in_01.png',
                                  tool_tip = "Zoom In")
    zoomInButton.clicked.connect(partial(temanejo.graphAppearanceEventHandler.zoom_ctrl,configuredValues.ZOOM_FACTOR))
    buttons.append(zoomInButton)

    # Create ZoomToDefault Button
    zoomDefButton = TemanejoButton(':images/tem_zoom_default_01.png',
                                  tool_tip = "Zoom To Default")
    zoomDefButton.clicked.connect(temanejo.graphAppearanceEventHandler.zoom_default)
    buttons.append(zoomDefButton)


    # Create ZoomFit Button
    zoomFitButton = TemanejoButton(':images/tem_zoom_fit_01.png',
                                  tool_tip = "Zoom To Fit")
    zoomFitButton.clicked.connect(partial(temanejo.graphAppearanceEventHandler.zoom_fit,temanejo))
    buttons.append(zoomFitButton)

    # Create Refresh Button
    refreshButton = TemanejoButton(':images/tem_refresh_01.png',
                                  tool_tip = "Refresh scene")
    refreshButton.clicked.connect(partial(temanejo.graphlayoutIO.makeGraphDirty.emit, True))
    buttons.append(refreshButton)

    # Create Debug Button
    debugButton = TemanejoButton(':images/tem_debug_01.png',
                                  tool_tip = "Debug")
    debugButton.clicked.connect(partial(temanejo.dddEventHandler.startDebugger))
    buttons.append(debugButton)

    # Create Debug Refresh Button
    debug_rf_button = TemanejoButton(':images/tem_debug_refr.png',
                                 tool_tip="Update Debugger")
    debug_rf_button.clicked.connect(partial(temanejo.remoteControl.connector_for_update_ddt_button))
    buttons.append(debug_rf_button)

    #Create the widget
    FreeDockWidget(temanejo,pos = QtCore.Qt.DockWidgetArea.RightDockWidgetArea,
                                   children=buttons, resizeable=False)
    return buttons

##
#   This collection will create all Buttons for stepping through the graph like stepping forward
#   fast forward etc...
def graphManipButtonCollection(temanejo):
    buttons = []
    # Create StepForwardButton
    stepForwardButton = TemanejoButton(':images/tem_run_01.png',
                                  tool_tip = "Step forward")
    stepForwardButton.clicked.connect(partial(temanejo.graphActionEventHandler.steppingForward))
    buttons.append(stepForwardButton)

    #Create FastForwardButton
    fastForwardButton = TemanejoButton(':images/tem_runfast_01.png',
                                  tool_tip = "Fast forward")
    fastForwardButton.clicked.connect(partial(temanejo.graphActionEventHandler.steppingFastForward,
                                              configuredValues.NUM_OF_STEPS_FOR_FAST_FORWARD))
    buttons.append(fastForwardButton)

    #Create PauseButton
    pauseButton = TemanejoButton(':images/tem_unpause.png',
                                  tool_tip = "Pause/Unpause")
    pauseButton.clicked.connect(partial(temanejo.graphActionEventHandler.pauseRun, pauseButton))

    buttons.append(pauseButton)

    #Create the widget
    FreeDockWidget(temanejo,pos = QtCore.Qt.DockWidgetArea.BottomDockWidgetArea,
                                   children=buttons, resizeable=False)
    return buttons


##
#   Set a property widget collection as a doc window at the left side of the mainWindow, which is
#   not more than the graph itself...
def propertyWidgetCollection(temanejo):
    widgets = []
    dep_widgets = []
    displayWidgets = []
    ncolor = PropertyDisplayWidget(temanejo, "n_col", labelText="Node color means", tool_tip="Color Nodes by Property Values")
    widgets.append(ncolor)
    displayWidgets.append(ncolor)

    mcolor = PropertyDisplayWidget(temanejo, "s_col", labelText="Margin color means", tool_tip="Color Margins by Property Values")
    widgets.append(mcolor)
    displayWidgets.append(mcolor)

    nshape = PropertyDisplayWidget(temanejo, "shape", labelText="Node shape means", tool_tip="Shape Nodes by Property Values")
    widgets.append(nshape)
    displayWidgets.append(nshape)

    dcolor = PropertyDisplayWidget(temanejo, "d_col", labelText="Dep. color means", tool_tip="Color Edges by Property Values")
    dep_widgets.append(dcolor)
    displayWidgets.append(dcolor)

    FreeDockWidget(temanejo, pos=QtCore.Qt.DockWidgetArea.LeftDockWidgetArea, children=displayWidgets)
    return widgets, dep_widgets
