import os 

from distutils.core import setup, Extension
#from setuptools import setup, Extension

__package_dir = "src"
__script_dir = "scripts"
__resources_dir = "resources"

setup(
    # Application name:
    name="Temanejo",

    # Version number (initial):
    version="2.0a",

    # Application author details:
    author="Stefan Reinhardt et al",
    author_email="temanejo@hlrs.de",

    # Details
    url="http://www.hlrs.de/temanejo/",

    #
    license="BSD",
    description="A visual debugger for task-based parallel programming models.",
    # long_description=open("README.txt").read(),

    # Packages
    package_dir={'': __package_dir},
    packages=["temanejo2",
               "temanejo2.temanejo2",
               "temanejo2.temanejo2.controller",
               "temanejo2.temanejo2.controller.graph",
               "temanejo2.temanejo2.controller.graph.remoteControls",
               "temanejo2.temanejo2.core",
               "temanejo2.temanejo2.core.items",
               "temanejo2.temanejo2.core.io",
               "temanejo2.temanejo2.lib",
               "temanejo2.temanejo2.lib.easymodel",
               "temanejo2.temanejo2.model",
               "temanejo2.temanejo2.view",
               "temanejo2.temanejo2.view.view_items"],

    # Include additional files into the package
    package_data={"temanejo2" : [os.path.join(__resources_dir,"*")]},
    
    #
    scripts=[os.path.join(__script_dir,'Temanejo2')],
    
    # extension modules
    #ext_modules=[Extension('temanejo2.ayu_socket', ["ayu_socket.cc"])],
    #ext_modules=[Extension('temanejo2.ayu_socket', ["ayu_socket.i"],
    #                         swig_opts=['-I../include'],  # this 
    #                         include_dirs=["../include/"],  # or this 
    #                         libraries=["ayu_XXX"], library_dirs=["../lib"],
    #                         )],
)
